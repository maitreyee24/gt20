package com.destek.gt20.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.destek.gt20.Activities.Activity_Bookticket;
import com.destek.gt20.Model.Ticket_Booking.Datum;
import com.destek.gt20.R;
import java.util.List;

public class Ticket_Adapter  extends RecyclerView.Adapter<Ticket_Adapter.MyViewHolder>
{
    private List<Datum> tickets_list;
    Context context;

    public Ticket_Adapter(List<Datum> tickets_list, Context applicationContext)
    {
        this.tickets_list=tickets_list;
        this.context=applicationContext;
    }

    @NonNull
    @Override
    public Ticket_Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.tickets_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position)
    {
        final Datum tickets = tickets_list.get(position);
        holder.Tex_place.setText(tickets.getStadium());
        holder.Txt_date.setText(tickets.getMatchDate());
        holder.Tex_time.setText(tickets.getMatchTime());
        holder.Tex_cost.setText(tickets.getCost());
        holder.txt_team_name.setText(tickets.getTeam1SN());
        holder.txt_team_name2.setText(tickets.getTeam2SN());


        Glide.with(context).load(tickets.getTeam1Logo()).error(R.mipmap.ic_launcher_round).into(holder.Img_team1);
        Glide.with(context).load(tickets.getTeam2Logo()).error(R.mipmap.ic_launcher_round).into(holder.Img_team2);


        holder.Btn_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, Activity_Bookticket.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("Tickets", tickets_list.get(position));
                context.startActivity(i);
            }
        });
    }


    @Override
    public int getItemCount() {
        return tickets_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView Tex_place,Txt_date,Tex_time,Tex_cost,txt_team_name,txt_team_name2;
        private Context context;
        public Button Btn_book;
        private ImageView Img_team1,Img_team2;

        public MyViewHolder(View view)
        {
            super(view);
            Tex_place = view.findViewById(R.id.Tex_place);
            Txt_date = view.findViewById(R.id.Txt_date);
            Tex_time = view.findViewById(R.id.Tex_time);
            Tex_cost = view.findViewById(R.id.Tex_cost);
            Btn_book = view.findViewById(R.id.Btn_book);
            txt_team_name = view.findViewById(R.id.txt_team_name);
            txt_team_name2 = view.findViewById(R.id.txt_team_name2);

            Img_team1 = view.findViewById(R.id.Img_team1);
            Img_team2 = view.findViewById(R.id.Img_team2);


        }
    }

}
