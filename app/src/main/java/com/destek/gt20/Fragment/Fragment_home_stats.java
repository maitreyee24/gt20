package com.destek.gt20.Fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.Adapter.CustomSpinnerAdapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Seasons.Datum;
import com.destek.gt20.Model.Seasons.Seasons;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_home_stats extends Fragment {
    private List<Datum> seasonList = new ArrayList<>();
    private String Season, StartDate, EndDate;
    private int SeasonId;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public Spinner spinner;
    private PreferenceSettings preferenceSettings;
    private CustomSpinnerAdapter customSpinnerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_home_stats, container, false);


        viewPager = view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        spinner = view.findViewById(R.id.spinner);

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();

        tabLayout = view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        if (ProjectConfig.isNetworkConnected(getActivity())) {
            GetSeasons();
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }
        return view;
    }

    private void GetSeasons()
    {

        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity());
        progressDialog.show();
        progressDialog.setCancelable(false);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Seasons> call = apiService.GetAllSeason("Bearer " + preferenceSettings.getTokenId());
        call.enqueue(new Callback<Seasons>() {
            @Override
            public void onResponse(Call<Seasons> call, Response<Seasons> response)
            {
                progressDialog.dismiss();

                String Message = response.body().getMessage();
                String Description = response.body().getDescription();
                String Error = response.body().getError();

                if (Message.equals("Success")) {
                    seasonList = response.body().getData();
                    for (int i = 0; i < seasonList.size(); i++) {
                        SeasonId = seasonList.get(i).getSeasonId();
                        Season = seasonList.get(i).getSeason();
                        StartDate = seasonList.get(i).getStartDate();
                        EndDate = seasonList.get(i).getEndDate();

                        System.out.println("##Season" + Season);
                        System.out.println("##StartDate" + StartDate);
                        System.out.println("##EndDate" + EndDate);

                        customSpinnerAdapter = new CustomSpinnerAdapter(getContext(), seasonList);
                        spinner.setAdapter(customSpinnerAdapter);
                        preferenceSettings.setSeasonid("" + SeasonId);
                    }
                }
            }

            @Override
            public void onFailure(Call<Seasons> call, Throwable t) {
                // Log error here since request failed
                progressDialog.dismiss();
                System.out.println("##onFailure" + t.toString());
            }
        });
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new Fragment_highest_run(), "HIGHEST RUN");
        adapter.addFragment(new Fragment_highest_wickets(), "HIGHEST WICKETS");
        adapter.addFragment(new Fragment_player_of_series(), "PLAYER OF SERIES");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
