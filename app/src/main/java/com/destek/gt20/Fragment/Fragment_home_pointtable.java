package com.destek.gt20.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;
import com.destek.gt20.Activities.ExpandableListAdapter;
import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.Adapter.PointTable_Adapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.PointTable.Datum;
import com.destek.gt20.Model.PointTable.PointTable;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_home_pointtable extends Fragment
{
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;



    RecyclerView vertical_recycler_view_home;
    private PointTable_Adapter mAdapter;
    private List<Datum> Point_List = new ArrayList<>();
    private PreferenceSettings preferenceSettings;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_home_pointtable, container, false);
        expListView = view.findViewById(R.id.lvExp);

        context = view.getContext();

        vertical_recycler_view_home = view.findViewById(R.id.vertical_recycler_view_home);
        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();


        if(ProjectConfig.isNetworkConnected(getActivity()))
        {
            prepareListData();
        }
        else
        {
            Toast.makeText(getActivity(),getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }


        /*listAdapter =new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
        // setting list adapter
        expListView.setAdapter(listAdapter);
*/


        return view;
    }

    private void prepareListData()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(context);
        progressDialog.show();
        progressDialog.setCancelable(false);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        System.out.println("##preferenceSettings.getTokenId()"+preferenceSettings.getTokenId());
        Call<PointTable> call = apiService.GetPointTableDetail("Bearer "+preferenceSettings.getTokenId());
        call.enqueue(new Callback<PointTable>()
        {
            @Override
            public void onResponse(Call<PointTable>call, Response<PointTable> response)
            {
                progressDialog.dismiss();
                String Message = response.body().getMessage();
                String Description = response.body().getDescription();
                String Error = response.body().getError();


                if(Message.equals("Success"))
                {
                    Point_List = response.body().getData();

                    mAdapter = new PointTable_Adapter(Point_List,getActivity());
                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
                    vertical_recycler_view_home.setLayoutManager(mLayoutManager1);
                    vertical_recycler_view_home.setItemAnimator(new DefaultItemAnimator());
                    vertical_recycler_view_home.setAdapter(mAdapter);
                }
            }
            @Override
            public void onFailure(Call<PointTable> call, Throwable t) {
                // Log error here since request failed
                progressDialog.dismiss();
                System.out.println("##onFailure"+ t.toString());
            }
        });

    }
}
