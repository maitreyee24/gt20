package com.destek.gt20.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Destek on 4/27/2017.
 */

public class PreferenceSettings
{
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private Context mContext;
    private int PRIVATE_MODE = 0;

    private static final String PREFERENCE_NAME = "gt20";
    private static final String KEY_NEW_INSTALL = "firstinstall";
    private static final String KEY_DEVICE_ID = "deviceId";
    private static final String KEY_LOGIN = "login";
    private static final String KEY_USER_ID = "userid";
    private static final String KEY_LOGIN_TYPE = "logintype";
    private static final String KEY_USER_NAME = "user_name";
    private static final String KEY_USER_EMAIL = "user_email";
    private static final String KEY_IS_LOGIN = "islogin";
    private static final String KEY_TEAM_LOGO = "team_logo";
    private static final String KEY_TEAM_NAME = "team_name";
    private static final String KEY_TEAM_ID = "team_id";
    private static final String KEY_COACH_NAME = "coach_name";
    private static final String KEY_SEASON_ID = "season_id";
    private static final String KEY_FIXTURE_ID = "fixture_id";
    private static final String KEY_TOKEN_ID= "token_id";
    private static final String KEY_TEAM1_ID = "team1_id";
    private static final String KEY_TEAM2_ID = "team2_id";

    public PreferenceSettings(Context context)
    {
        mContext = context;
        mSharedPreferences = mContext.getSharedPreferences(PREFERENCE_NAME,PRIVATE_MODE);
        mEditor = mSharedPreferences.edit();
    }

    public void setPermission(String KEY_PERMISSION, boolean login)
    {
        mEditor.putBoolean(KEY_PERMISSION,login).commit();
    }

    public boolean getPermission(String KEY_PERMISSION)
    {
        return mSharedPreferences.getBoolean(KEY_PERMISSION,false);
    }



    public void setNewInstall(boolean first)
    {
        mEditor.putBoolean(KEY_NEW_INSTALL,first).commit();
    }

    public boolean getNewInstall()
    {
        return mSharedPreferences.getBoolean(KEY_NEW_INSTALL,false);
    }

    public void setDeviceToken(String s) {
        mEditor.putString(KEY_DEVICE_ID,s).commit();
    }

    public String getDeviceToken()
    {
        return mSharedPreferences.getString(KEY_DEVICE_ID,null);
    }

    public void setLogin(boolean isLogin)
    {
        mEditor.putBoolean(KEY_LOGIN,isLogin).commit();
    }

    public boolean getLogin()
    {
        return mSharedPreferences.getBoolean(KEY_LOGIN,false);
    }

    public void setUserName(String userName)
    {
        mEditor.putString(KEY_USER_NAME,userName).commit();
    }

    public String getUserName()
    {
        return mSharedPreferences.getString(KEY_USER_NAME,null);
    }

    public void setLoginType(String s) {
        mEditor.putString(KEY_LOGIN_TYPE,s).commit();
    }

    public String getLoginType()
    {
        return mSharedPreferences.getString(KEY_LOGIN_TYPE,null);
    }


    public void setTokenId(String userId)
    {
        mEditor.putString(KEY_TOKEN_ID,userId).commit();
    }

    public String getTokenId()
    {
        return mSharedPreferences.getString(KEY_TOKEN_ID,null);
    }

    public void setUserId(String userId)
    {
        mEditor.putString(KEY_USER_ID,userId).commit();
    }

    public String getUserId()
    {
        return mSharedPreferences.getString(KEY_USER_ID,null);
    }

    public void setLoggedIn(Boolean status) {
        mEditor.putBoolean(KEY_IS_LOGIN,status).commit();
    }

    public boolean isLoggedIn()
    {
        return mSharedPreferences.getBoolean(KEY_IS_LOGIN,false);
    }

    public void setTeamName(String s) {
        mEditor.putString(KEY_TEAM_NAME,s).commit();
    }

    public String getTeamName()
    {
        return mSharedPreferences.getString(KEY_TEAM_NAME,null);
    }

    public void setFixtureid(String s) {
        mEditor.putString(KEY_FIXTURE_ID,s).commit();
    }

    public String getFixtureid()
    {
        return mSharedPreferences.getString(KEY_FIXTURE_ID,null);
    }

    public void setSeasonid(String s) {
        mEditor.putString(KEY_SEASON_ID,s).commit();
    }

    public String getSeasonid()
    {
        return mSharedPreferences.getString(KEY_SEASON_ID,null);
    }

    public void setTeam1id(String s) {
        mEditor.putString(KEY_TEAM1_ID,s).commit();
    }

    public String getTeam1id()
    {
        return mSharedPreferences.getString(KEY_TEAM1_ID,null);
    }

    public void setTeam2id(String s) {
        mEditor.putString(KEY_TEAM2_ID,s).commit();
    }

    public String getTeam2id()
    {
        return mSharedPreferences.getString(KEY_TEAM2_ID,null);
    }


    public void setTeamLogo(String s) {
        mEditor.putString(KEY_TEAM_LOGO,s).commit();
    }

    public String getTeamLogo()
    {
        return mSharedPreferences.getString(KEY_TEAM_LOGO,null);
    }

    public void setEmail(String s) {
        mEditor.putString(KEY_USER_EMAIL,s).commit();
    }

    public String getEmail()
    {
        return mSharedPreferences.getString(KEY_USER_EMAIL,null);
    }

    public void setCoachName(String s) {
        mEditor.putString(KEY_COACH_NAME,s).commit();
    }

    public String getCoachName()
    {
        return mSharedPreferences.getString(KEY_COACH_NAME,null);
    }

    public void setTeamId(String s) {
        mEditor.putString(KEY_TEAM_ID,s).commit();
    }

    public String getTeamId()
    {
        return mSharedPreferences.getString(KEY_TEAM_ID,null);
    }

    public void clearPreferences()
    {
        mSharedPreferences = mContext.getSharedPreferences(PREFERENCE_NAME, PRIVATE_MODE);
        mEditor.clear().commit();
    }


}
