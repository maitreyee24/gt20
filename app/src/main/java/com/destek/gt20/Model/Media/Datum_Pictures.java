package com.destek.gt20.Model.Media;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum_Pictures {

    @SerializedName("Thumnails")
    @Expose
    private String thumnails;
    @SerializedName("ImageName")
    @Expose
    private String imageName;

    public String getThumnails() {
        return thumnails;
    }

    public void setThumnails(String thumnails) {
        this.thumnails = thumnails;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

}