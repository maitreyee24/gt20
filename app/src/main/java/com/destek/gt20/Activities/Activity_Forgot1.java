package com.destek.gt20.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.OTPVerification;
import com.destek.gt20.Model.OTPVerified;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;

import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_Forgot1 extends AppCompatActivity
{
    Button btn_confirm,btn_resend,back_btn;
    EditText et_otp,et_mobile_num;
    String Otp, mobile;
    TextView txt_time;
    BroadcastReceiver receiver;
    private PreferenceSettings preferenceSettings;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password2);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();

        et_otp = findViewById(R.id.et_otp);
        et_mobile_num = findViewById(R.id.et_mobile_num);
        txt_time = findViewById(R.id.txt_time);
        mobile = getIntent().getStringExtra("Mobile");
        et_mobile_num.setText(mobile);

        mContext = Activity_Forgot1.this;


        et_otp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 4) {
                    btn_confirm.setBackgroundResource(R.drawable.card_border_fill);
                    btn_confirm.setEnabled(true);
                    btn_confirm.setTextColor(getResources().getColor(R.color.white));


                    btn_resend.setBackgroundResource(R.drawable.card_border_fill);
                    btn_resend.setEnabled(true);
                    btn_resend.setTextColor(getResources().getColor(R.color.white));


                } else {
                    btn_confirm.setBackgroundResource(R.drawable.card_border);
                    btn_confirm.setEnabled(false);
                    btn_confirm.setTextColor(getResources().getColor(R.color.grey));
                }

            }
        });



        if(ProjectConfig.isNetworkConnected(Activity_Forgot1.this))
        {
            GenerationAndSaveOTP();
        }
        else
        {
            Toast.makeText(Activity_Forgot1.this,getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }


        btn_resend = findViewById(R.id.btn_resend);
        btn_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(ProjectConfig.isNetworkConnected(Activity_Forgot1.this))
                {
                    GenerationAndSaveOTP();
                }
                else
                {
                    Toast.makeText(Activity_Forgot1.this,getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
                }

            }
        });


        btn_confirm = findViewById(R.id.btn_confirm);
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Otp = et_otp.getText().toString();
                if (!Otp.equals(""))
                {
                    VerifyOTP(Otp);
                } else {
                    et_otp.setError("Please Enter OTP");
                }
            }
        });


        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
        startTimer();


        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase("otp")) {
                    final String message = intent.getStringExtra("message");

                    String[] s1 = message.split(" ");
                    String []otp = s1[1].split(",");
                    et_otp.setText(otp[0]);
                   //Do whatever you want with the cod e here
                }
            }
        };

    }


    private void startTimer()
    {
        new CountDownTimer(90000, 1000) {

            public void onTick(long millisUntilFinished) {
                txt_time.setText("Waiting for an OTP " + String.format("%d : %d ",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                txt_time.setText( "00:00");
                //  btn_resend.setEnabled(true);

                btn_resend.setBackgroundResource(R.drawable.card_border_fill);
                btn_resend.setEnabled(true);
                btn_resend.setTextColor(getResources().getColor(R.color.white));
            }

        }.start();
    }
    private void GenerationAndSaveOTP()
    {

        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(mContext);
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("CountryCode","91");
        jsonObject.addProperty("Mobile",mobile);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<OTPVerification> call = apiService.OTPVerification(jsonObject);
        call.enqueue(new Callback<OTPVerification>()
        {
            @Override
            public void onResponse(Call<OTPVerification>call, Response<OTPVerification> response)
            {
               progressDialog.dismiss();

                System.out.println("##response"+response);
                String Message = response.body().getMessage();
                String Description = response.body().getDescription();
                String Token = response.body().getToken();

                if(Message.equals("Success"))
                {
                    preferenceSettings.setTokenId(Token);

                    Toast.makeText(getApplicationContext(),Description,Toast.LENGTH_SHORT).show();
                }
                else
                {
                  //  Toast.makeText(getApplicationContext(),Description,Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<OTPVerification>call, Throwable t) {

                System.out.println("##onFailure"+ t.toString());
              progressDialog.dismiss();
            }
        });
    }

    private void VerifyOTP(String otp)
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(mContext);
        progressDialog.show();
        progressDialog.setCancelable(false);

        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("CountryCode","91");
        jsonObject.addProperty("Mobile",mobile);
        jsonObject.addProperty("OTP",otp);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<OTPVerified> call = apiService.OTPVerified("Bearer "+preferenceSettings.getTokenId(),jsonObject);
        call.enqueue(new Callback<OTPVerified>()
        {
            @Override
            public void onResponse(Call<OTPVerified>call, Response<OTPVerified> response)
            {
                progressDialog.dismiss();

                    String Message = response.body().getMessage();
                    String Description = response.body().getDescription();

                System.out.println("##Message"+Message);

                    if(Message.equals("Success"))
                    {
                        Toast.makeText(getApplicationContext(),Description,Toast.LENGTH_SHORT).show();
                        Intent mainIntent = new Intent(getApplicationContext(),Activity_Forgot2.class);
                        mainIntent.putExtra("Mobile",mobile);
                        startActivity(mainIntent);
                        finish();
                    }
                    else {
                        Toast.makeText(getApplicationContext(),Description,Toast.LENGTH_SHORT).show();
                    }
            }
            @Override
            public void onFailure(Call<OTPVerified>call, Throwable t) {
                // Log error here since request failed

                System.out.println("##onFailure"+ t.toString());
                progressDialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent mainIntent = new Intent(getApplicationContext(), Activity_Forgot.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mainIntent);
        finish();
    }
}
