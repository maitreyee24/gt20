package com.destek.gt20.Activities;


import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.CommonUtils;
import com.destek.gt20.Model.ProfilePicture.ProfilePic;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Activity_ProfilePic extends AppCompatActivity implements View.OnClickListener
{

    private String TAG = Activity_ProfilePic.class.getSimpleName();
    private PreferenceSettings mPreferenceSettings;
    private boolean sentToSettings = false;
    private String[] permissionsRequired = new String[]{Manifest.permission.CAMERA};
    private static final int REQUEST_PERMISSION_SETTING = 2001;
    private static final int PERMISSION_CALLBACK_CONSTANT = 2000, IMG_RESULT = 255;
    private LinearLayout llCamera, llGallery;
    private ImageView ivImage, ivUpload, ivBack;
    private static String imageStoragePath;
    private ProgressDialog progressDialog;
    private boolean IS_IMAGE_UPLOAD = false;
    private byte[] imageInByte;
    private FrameLayout flMain;

    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    // key to store image path in savedInstance state
    public static final String KEY_IMAGE_STORAGE_PATH = "image_path";
    public static final int MEDIA_TYPE_IMAGE = 1;

    // Bitmap sampling size
    public static final int BITMAP_SAMPLE_SIZE = 4;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_image);

        mPreferenceSettings = AppSingleton.getInstance().getPreferenceSettings();

        if (Build.VERSION.SDK_INT >= 23) {
            takePermissions();
        } else {
            initialize();
        }
    }

    private void initialize()
    {

        flMain = findViewById(R.id.fl_main_cp);

        llCamera = findViewById(R.id.ll_camera);
        llGallery = findViewById(R.id.ll_gallery);
        ivBack = findViewById(R.id.img_back_capture_image);
        ivImage = findViewById(R.id.img_image);
        ivUpload = (ImageView) findViewById(R.id.img_upload_profile);

       /* Picasso.with(ProfilePicActivity.this).load(mPreferenceSettings.getUserImage())
                .placeholder(R.drawable.user5)
                .error(R.drawable.user5)
                .into(ivImage);*/

//        ivUpload.setVisibility(View.GONE);
        llCamera.setOnClickListener(this);
        llGallery.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        ivUpload.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_camera:
                takeCameraImage();
                break;
            case R.id.ll_gallery:
                takeGalleryImage();
                break;
            case R.id.img_back_capture_image:
                onBackPressed();
                break;
            case R.id.img_upload_profile:
                if (ProjectConfig.isNetworkConnected(Activity_ProfilePic.this)) {
                    uploadImage(ivImage);

                } else {
                    Toast.makeText(Activity_ProfilePic.this, "" + getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void takeGalleryImage()
    {
        Intent intent = new Intent(Activity_ProfilePic.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, IMG_RESULT);
    }

    private void takeCameraImage()
    {
        Intent intent = new Intent(Activity_ProfilePic.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent,CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CommonUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }

        Uri fileUri = CommonUtils.getOutputMediaFileUri(getApplicationContext(), file);


    }

    private void galleryIntent() {
        System.out.println("##galleryIntent");
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, IMG_RESULT);
    }


    /**
     * Saving stored image path to saved instance state
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putString(KEY_IMAGE_STORAGE_PATH, imageStoragePath);
    }

    /**
     * Restoring image path from saved instance state
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        imageStoragePath = savedInstanceState.getString(KEY_IMAGE_STORAGE_PATH);
    }


    public void takePermissions() {


        if (ActivityCompat.checkSelfPermission(Activity_ProfilePic.this, permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(Activity_ProfilePic.this, permissionsRequired[0])) {
                final Dialog dialog = new Dialog(Activity_ProfilePic.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                dialog.setContentView(R.layout.dialog_permission);

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.gravity = Gravity.CENTER;

                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                dialog.getWindow().setAttributes(lp);

                TextView tvTitle, tvMessage;
                tvTitle = dialog.findViewById(R.id.txt_permission_title);
                tvMessage = dialog.findViewById(R.id.txt_permission_message);

                tvTitle.setText("" + getResources().getString(R.string.permission_required));
                tvMessage.setText("" + getResources().getString(R.string.permission_message_camera));

                LinearLayout btnSend = dialog.findViewById(R.id.btn_allow);
                btnSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ActivityCompat.requestPermissions(Activity_ProfilePic.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                        dialog.dismiss();
                    }
                });

                dialog.show();

            } else if (mPreferenceSettings.getPermission(permissionsRequired[0])) {
                final Dialog dialog = new Dialog(Activity_ProfilePic.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                dialog.setContentView(R.layout.dialog_permission);

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.gravity = Gravity.CENTER;

                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                dialog.getWindow().setAttributes(lp);

                TextView tvTitle, tvMessage;
                tvTitle = dialog.findViewById(R.id.txt_permission_title);
                tvMessage = dialog.findViewById(R.id.txt_permission_message);

                tvTitle.setText("" + getResources().getString(R.string.permission_required));
                tvMessage.setText("" + getResources().getString(R.string.permission_message_camera));

                LinearLayout btnSend = dialog.findViewById(R.id.btn_allow);
                btnSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", Activity_ProfilePic.this.getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(Activity_ProfilePic.this, "" + getResources().getString(R.string.go_to_settings), Toast.LENGTH_LONG).show();
                    }
                });

                dialog.show();
            } else {
                ActivityCompat.requestPermissions(Activity_ProfilePic.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
            }

            mPreferenceSettings.setPermission(permissionsRequired[0], true);

        } else {
            initialize();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            boolean allgranted = false;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allgranted = true;
                } else {
                    allgranted = false;
                    break;
                }
            }

            if (allgranted) {
                initialize();
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(Activity_ProfilePic.this, permissionsRequired[0])) {
                final Dialog dialog = new Dialog(Activity_ProfilePic.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                dialog.setContentView(R.layout.dialog_permission);

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.gravity = Gravity.CENTER;

                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                dialog.getWindow().setAttributes(lp);

                TextView tvTitle, tvMessage;
                tvTitle = dialog.findViewById(R.id.txt_permission_title);
                tvMessage = dialog.findViewById(R.id.txt_permission_message);

                tvTitle.setText("" + getResources().getString(R.string.permission_required));
                tvMessage.setText("" + getResources().getString(R.string.permission_message_camera));

                LinearLayout btnSend = dialog.findViewById(R.id.btn_allow);
                btnSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ActivityCompat.requestPermissions(Activity_ProfilePic.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                        dialog.dismiss();
                    }
                });

                dialog.show();
            } else {
                final Dialog dialog = new Dialog(Activity_ProfilePic.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                dialog.setContentView(R.layout.dialog_permission);

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.gravity = Gravity.CENTER;

                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                dialog.getWindow().setAttributes(lp);

                TextView tvTitle, tvMessage;
                tvTitle = dialog.findViewById(R.id.txt_permission_title);
                tvMessage = dialog.findViewById(R.id.txt_permission_message);

                tvTitle.setText("" + getResources().getString(R.string.permission_required));
                tvMessage.setText("" + getResources().getString(R.string.permission_message_camera));

                LinearLayout btnSend = dialog.findViewById(R.id.btn_allow);
                btnSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", Activity_ProfilePic.this.getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(Activity_ProfilePic.this, "" + getResources().getString(R.string.go_to_settings), Toast.LENGTH_LONG).show();
                    }
                });

                dialog.show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_PERMISSION_SETTING:
                takePermissions();
                break;

            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:

                if (resultCode == RESULT_OK) {

                    Uri uri = data.getParcelableExtra("path");
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                        checkOrientation(uri.getPath(),bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(getApplicationContext(),
                            "User cancelled image capture", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                            .show();
                }
                break;

            case IMG_RESULT:
                if (resultCode == RESULT_OK)
                    onSelectFromGalleryResult(data);
                break;
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        System.out.println("##onSelectFromGalleryResult");

        if (data != null) {
            try {
                Uri URI = data.getParcelableExtra("path");
                //Uri URI = data.getData();
                String[] FILE = {MediaStore.Images.Media.DATA};

                Bitmap bitmap = null;
                imageStoragePath = URI.getPath();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), URI);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                checkOrientation(imageStoragePath,bitmap);


            } catch (Exception e) {
                System.out.println("## Exeption :" + e);
                e.printStackTrace();
            }
        }
    }


    public void checkOrientation(String photoPath,Bitmap bitmap) {
        Bitmap bitmap1 = CommonUtils.optimizeBitmap(BITMAP_SAMPLE_SIZE, imageStoragePath);
        ExifInterface ei = null;
        Bitmap rotatedBitmap = null;
        try {
            ei = new ExifInterface(photoPath);

            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(bitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(bitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(bitmap, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        previewCapturedImage(rotatedBitmap);
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private void previewCapturedImage(Bitmap bitmap) {
        try {
            Log.e(TAG, "previewCapturedImage: " + imageStoragePath);
            ivImage.setImageBitmap(bitmap);
            ivUpload.setVisibility(View.VISIBLE);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    class convertToByterray extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... strings) {
            byte[] bytes = CommonUtils.readBytesFromFile(strings[0]);
            String encoded = Base64.encodeToString(bytes, 0);
            Log.e("~~~~~~~~ Encoded: ", encoded);

            byte[] decoded = Base64.decode(encoded, 0);
            Log.e("~~~~~~~~ Decoded: ", Arrays.toString(decoded));
            return encoded;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("onPostExecute", ": " + s);

        }
    }

    private JSONObject getInputJson() {
        JSONObject inputJson = new JSONObject();
        try {
            inputJson.put("UserId", mPreferenceSettings.getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return inputJson;
    }


    // Upload image on server
    public void uploadImage(final ImageView imageView)

    {

        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(Activity_ProfilePic.this);
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("UserId","2");
        jsonObject.addProperty("File", String.valueOf(imageView));



        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ProfilePic> call = apiService.ProfileImageUpload("Bearer "+mPreferenceSettings.getTokenId(),jsonObject);
        call.enqueue(new Callback<ProfilePic>()
        {
            @Override
            public void onResponse(Call<ProfilePic>call, Response<ProfilePic> response)
            {
                progressDialog.dismiss();

                String Message = response.body().getMessage();
                String Description = response.body().getDescription();

                if(Message.equals("Success"))
                {

                    Toast.makeText(getApplicationContext(),Description,Toast.LENGTH_SHORT).show();

                    onBackPressed();
                }
                else {
                    Toast.makeText(getApplicationContext(),Description,Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ProfilePic>call, Throwable t) {
                // Log error here since request failed

                System.out.println("##onFailure"+ t.toString());
                progressDialog.dismiss();
            }
        });
    }



    @Override
    public void onBackPressed() {
        if (IS_IMAGE_UPLOAD) {
            Intent intent = new Intent();
            setResult(44, intent);
            finish();
        } else {
            finish();
        }
    }
}
