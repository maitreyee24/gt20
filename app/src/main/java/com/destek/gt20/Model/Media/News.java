package com.destek.gt20.Model.Media;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class News {

    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("Error")
    @Expose
    private String error;
    @SerializedName("Data")
    @Expose
    private List<Datum_News> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Datum_News> getData() {
        return data;
    }

    public void setData(List<Datum_News> data) {
        this.data = data;
    }

}