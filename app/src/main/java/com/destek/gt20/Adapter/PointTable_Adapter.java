package com.destek.gt20.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.destek.gt20.Model.PointTable.Datum;
import com.destek.gt20.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PointTable_Adapter  extends RecyclerView.Adapter<PointTable_Adapter.MyViewHolder>
{
    private List<Datum> point_list;
    Context context;

    public PointTable_Adapter(List<Datum> point_list,Context context)
    {  this.point_list = point_list;
        this.context = context;
    }

    @NonNull
    @Override
    public PointTable_Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pointable_row, parent, false);
        return new PointTable_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PointTable_Adapter.MyViewHolder holder, int position)
    {
        Datum points = point_list.get(position);
        holder.Txt_pos.setText("" + Integer.parseInt(String.valueOf(position+1)) + ".");
        System.out.println("##points.getTeamName()"+points.getTeamName());
        holder.Txt_team_name.setText(points.getTeamName());
        holder.Txt_M.setText(""+Integer.parseInt(String.valueOf(points.getM())));
        holder.Txt_W.setText(""+Integer.parseInt(String.valueOf(points.getW())));
        holder.Txt_D.setText(""+points.getD());
        holder.Txt_L.setText(""+points.getL());
        holder.Txt_PTS.setText(""+points.getPoints());
        holder.Txt_NRR.setText(""+points.getNRR());


        System.out.println("##points.getTeamLogo()"+points.getTeamLogo());

      //Glide.with(context).load(points.getTeamLogo()).error(R.mipmap.ic_launcher_round).into(holder.Img_team_logo);

        Picasso.with(context).load(points.getTeamLogo()).resize(6000, 2000)
                .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(holder.Img_team_logo);
    }

    @Override
    public int getItemCount() {
        return point_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView Txt_pos,Txt_team_name,Txt_M,Txt_W,Txt_D,Txt_L,Txt_PTS,Txt_NRR;
        public ImageView  Img_team_logo;

        public MyViewHolder(View view) {
            super(view);
            Txt_pos = view.findViewById(R.id.Txt_pos);
            Txt_team_name = view.findViewById(R.id.Txt_team_name);
            Txt_M = view.findViewById(R.id.Txt_M);
            Txt_W = view.findViewById(R.id.Txt_W);
            Txt_D = view.findViewById(R.id.Txt_D);
            Txt_L = view.findViewById(R.id.Txt_L);
            Txt_PTS = view.findViewById(R.id.Txt_PTS);
            Txt_NRR = view.findViewById(R.id.Txt_NRR);
            Img_team_logo = view.findViewById(R.id.Img_team_logo);
        }
    }
}
