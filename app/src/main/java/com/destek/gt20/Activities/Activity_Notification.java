package com.destek.gt20.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.destek.gt20.Adapter.Notification_Adapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Notification.Datum_notification;
import com.destek.gt20.Model.Notification.GetNotification;
import com.destek.gt20.Model.Notification.Notification;
import com.destek.gt20.Model.Schedule.Datum;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_Notification extends AppCompatActivity
{
    LinearLayout LL_Notification, LL_invite;
    EditText Edit_time;
    TextView txt_teams, txt_date, txt_match, txt_time, txt_place, txt_minutes,txt_hrs,txt_days,txt_week;
    ImageView Img_cancel, Img_team1, Img_team2;
    Button Btn_add,Btn_save;
    private RecyclerView recyclerView;
    Intent shareIntent;
    String shareBody = "Follow GT20 at https://desteksolutions.com/";
    private Datum datum;
    private Notification_Adapter mAdapter;
    private List<Datum_notification> notificationList = new ArrayList<>();
    private PreferenceSettings preferenceSettings;
    private String duration;
    private LinearLayout llNotificationList;
    Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();

        LL_Notification = findViewById(R.id.LL_Notification);
        txt_teams = findViewById(R.id.txt_teams);
        txt_date = findViewById(R.id.txt_date);
        txt_match = findViewById(R.id.txt_match);
        txt_time = findViewById(R.id.txt_time);
        txt_place = findViewById(R.id.txt_place);
        txt_minutes = findViewById(R.id.txt_minutes);
        llNotificationList = findViewById(R.id.ll_notification_list);

        Img_team1 = findViewById(R.id.Img_team1);
        Img_team2 = findViewById(R.id.Img_team2);
        Btn_save = findViewById(R.id.Btn_save);
        Btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });


        datum = (Datum) getIntent().getSerializableExtra("Model");

        txt_teams.setText(datum.getTeam1Name() + " VS " + datum.getTeam2Name());
        txt_date.setText(datum.getMatchDate());
        txt_time.setText(datum.getMatchTime());
        txt_place.setText(datum.getStadium());

        Glide.with(getApplicationContext()).load(datum.getTeam1Logo()).error(R.mipmap.ic_launcher_round).into(Img_team1);
        Glide.with(getApplicationContext()).load(datum.getTeam2Logo()).error(R.mipmap.ic_launcher_round).into(Img_team2);


        if (ProjectConfig.isNetworkConnected(Activity_Notification.this)) {
            GetNotification();
        } else {
            Toast.makeText(Activity_Notification.this, getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }


        LL_Notification = findViewById(R.id.LL_Notification);
        LL_Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(Activity_Notification.this);
                int width = ViewGroup.LayoutParams.MATCH_PARENT;
                int height = ViewGroup.LayoutParams.WRAP_CONTENT;
                dialog.getWindow().setLayout(width, height);

                dialog.setContentView(R.layout.dialog_notification);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setTitle("Notification");

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                dialog.getWindow().setAttributes(lp);


                Edit_time = dialog.findViewById(R.id.Edit_time);
                final TextView tvMins = dialog.findViewById(R.id.txt_minutes);
                final TextView tvHours = dialog.findViewById(R.id.txt_hrs);
                final TextView tvDays = dialog.findViewById(R.id.txt_days);
                final TextView tvWeeks = dialog.findViewById(R.id.txt_week);
                duration = "";

                tvMins.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        duration = "Minutes";
                        tvMins.setTextColor(getResources().getColor(R.color.colorAccent));
                        tvWeeks.setTextColor(getResources().getColor(R.color.black));
                        tvDays.setTextColor(getResources().getColor(R.color.black));
                        tvHours.setTextColor(getResources().getColor(R.color.black));
                    }
                });
                tvHours.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        duration = "Hours Before";
                        tvMins.setTextColor(getResources().getColor(R.color.black));
                        tvWeeks.setTextColor(getResources().getColor(R.color.black));
                        tvDays.setTextColor(getResources().getColor(R.color.black));
                        tvHours.setTextColor(getResources().getColor(R.color.colorAccent));
                    }
                });
                tvDays.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        duration = "Days";
                        tvMins.setTextColor(getResources().getColor(R.color.black));
                        tvWeeks.setTextColor(getResources().getColor(R.color.black));
                        tvDays.setTextColor(getResources().getColor(R.color.colorAccent));
                        tvHours.setTextColor(getResources().getColor(R.color.black));
                    }
                });
                tvWeeks.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        duration = "Weeks";
                        tvMins.setTextColor(getResources().getColor(R.color.black));
                        tvWeeks.setTextColor(getResources().getColor(R.color.colorAccent));
                        tvDays.setTextColor(getResources().getColor(R.color.black));
                        tvHours.setTextColor(getResources().getColor(R.color.black));
                    }
                });



                Btn_add = dialog.findViewById(R.id.Btn_add);
                Btn_add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        boolean flag = true;
                        String number = Edit_time.getText().toString().trim();


                        if (number.isEmpty()) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_amount), Toast.LENGTH_SHORT).show();
                            flag = false;
                        }
                        if (duration.isEmpty()) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_duration), Toast.LENGTH_SHORT).show();
                            flag = false;
                        }

                        if (flag) {
                            if (ProjectConfig.isNetworkConnected(getApplicationContext()))
                            {
                                dialog.dismiss();
                                AddNotification(number, duration);
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
                Img_cancel = dialog.findViewById(R.id.Img_cancel);
                Img_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });


        LL_invite = findViewById(R.id.LL_invite);
        LL_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "My App");
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(shareIntent, "Invite via"));

            }
        });
    }


    private void AddNotification(String number, String duration)
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(Activity_Notification.this);

        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("FixtureId", "6");
        jsonObject.addProperty("UserId", "2");
        jsonObject.addProperty("RemindarType", duration);
        jsonObject.addProperty("RemindarNo", number);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Notification> call = apiService.AddNotification("Bearer " + preferenceSettings.getTokenId(), jsonObject);
        call.enqueue(new Callback<Notification>() {
            @Override
            public void onResponse(Call<Notification> call, Response<Notification> response) {

                progressDialog.dismiss();

                String Message = response.body().getMessage();
                String Description = response.body().getDescription();

                if (Message.equals("Success")) {

                    //Toast.makeText(getApplicationContext(), Description, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                   /* Intent mainIntent = new Intent(getApplicationContext(), Activity_Notification.class);
                    startActivity(mainIntent);
                    finish();*/
                }
            }
        @Override
            public void onFailure(Call<Notification> call, Throwable t) {
                // Log error here since request failed

                System.out.println("##onFailure" + t.toString());
                progressDialog.dismiss();
            }
        });
    }

    private void GetNotification() {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(Activity_Notification.this);
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("FixtureId", "6");
        jsonObject.addProperty("UserId", "2");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<GetNotification> call = apiService.GetNotification("Bearer " + preferenceSettings.getTokenId(), jsonObject);
        call.enqueue(new Callback<GetNotification>() {
            @Override
            public void onResponse(Call<GetNotification> call, Response<GetNotification> response) {

                progressDialog.dismiss();

                String Message = response.body().getMessage();
                String Description = response.body().getDescription();
                notificationList = response.body().getData();

                if (Message.equals("Success")) {

                    /*mAdapter = new Notification_Adapter(notificationList,getApplicationContext());
                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext());
                    recyclerView.setLayoutManager(mLayoutManager1);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(mAdapter);*/
                    addNotifications();
                }
            }

            @Override
            public void onFailure(Call<GetNotification> call, Throwable t) {
                // Log error here since request failed

                System.out.println("##onFailure" + t.toString());
                progressDialog.dismiss();
            }
        });
    }

    private void addNotifications() {
        if (llNotificationList.getChildCount() > 0) {
            llNotificationList.removeAllViews();
        }

        if (notificationList.size() > 0) {

            View view = null;

            for (int i = 0; i < notificationList.size(); i++) {
                view = LayoutInflater.from(Activity_Notification.this).inflate(R.layout.notify_row, llNotificationList,false);
                TextView tvTime = view.findViewById(R.id.txt_time);
                final ImageView ivClose = view.findViewById(R.id.Img_cancel);

                tvTime.setText(""+notificationList.get(i).getRemindarNo()+" "+notificationList.get(i).getRemindarType());
                ivClose.setTag(i);

                ivClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if(ProjectConfig.isNetworkConnected(Activity_Notification.this))
                        {
                            delete_notification(notificationList.get((Integer) ivClose.getTag()).getNotificationId(),
                                    (Integer) ivClose.getTag());
                        }
                        else
                        {
                            Toast.makeText(Activity_Notification.this,"Please Check Internet Connection...", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                llNotificationList.addView(view);
            }

        }
    }

    private void delete_notification(Integer notificationId, final int position)
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(Activity_Notification.this);
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("NotificationId", notificationId);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Notification> call = apiService.DeleteNotification("Bearer "+preferenceSettings.getTokenId(),jsonObject);
        call.enqueue(new Callback<Notification>() {
            @Override
            public void onResponse(Call<Notification> call, Response<Notification> response) {

                progressDialog.dismiss();

                String Message = response.body().getMessage();
                String Description = response.body().getDescription();

                if (Message.equals("Success")) {

                    Toast.makeText(Activity_Notification.this, Description, Toast.LENGTH_SHORT).show();
                    llNotificationList.removeViewAt(position);
                    notificationList.remove(position);


//                    Intent mainIntent = new Intent(Activity_Notification.this, MainActivity.class);
//                    Activity_Notification.this. startActivity(mainIntent);

                }
            }
            @Override
            public void onFailure(Call<Notification> call, Throwable t) {
                // Log error here since request failed

                System.out.println("##onFailure" + t.toString());
                progressDialog.dismiss();
            }
        });
    }
}