package com.destek.gt20.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.destek.gt20.Model.Seasons.Datum;
import com.destek.gt20.R;
import java.util.List;

public class CustomSpinnerAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflter;
    List<Datum> seasonList;
    public CustomSpinnerAdapter(Context applicationContext, List<Datum> seasonList) {
        this.context = applicationContext;
        inflter = (LayoutInflater.from(applicationContext));

        this.seasonList = seasonList;
    }

    @Override
    public int getCount() {
        return seasonList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_spinner_row, null);
        TextView season = (TextView) view.findViewById(R.id.txt_season);
        TextView date = (TextView) view.findViewById(R.id.txt_date);

        season.setText(seasonList.get(i).getSeason());
        date.setText(seasonList.get(i).getStartDate() + "-" + seasonList.get(i).getEndDate());
        return view;
    }
}