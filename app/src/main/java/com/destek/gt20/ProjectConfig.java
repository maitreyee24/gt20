package com.destek.gt20;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ProjectConfig
{
    public static final String SHARED_PREF = "ah_firebase";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";


    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }
}
