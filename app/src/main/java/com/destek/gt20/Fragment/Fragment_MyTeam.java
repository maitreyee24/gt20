package com.destek.gt20.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.destek.gt20.Activities.Activity_Login;
import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.Adapter.My_ListTeam_Adapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Team.Datum;
import com.destek.gt20.Model.Team.Team;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_MyTeam extends Fragment
{
    private List<Datum> team_List =null;
    RecyclerView vertical_recycler_view_home;
    private My_ListTeam_Adapter listTeam_adapter;
    private PreferenceSettings preferenceSettings;
    public LinearLayout LL_record,LL_norecord;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        View view = inflater.inflate(R.layout.fragment_myteam, container, false);
        vertical_recycler_view_home = view.findViewById(R.id.vertical_recycler_view_home);

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();

        LL_record =view.findViewById(R.id.LL_record);
        LL_norecord = view.findViewById(R.id.LL_norecord);


        if(ProjectConfig.isNetworkConnected(getActivity()))
        {
            GetTeamList();
        }
        else
        {
            Toast.makeText(getActivity(),getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }

        return  view;
    }

    private void GetTeamList()
    {

        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity());
        progressDialog.show();
        progressDialog.setCancelable(false);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Team> call = apiService.getTeamData("Bearer "+preferenceSettings.getTokenId());
        call.enqueue(new Callback<Team>() {
            @Override
            public void onResponse(Call<Team> call, Response<Team> response)
            {
               progressDialog.dismiss();
                String Message= response.body().getMessage();
                String Description= response.body().getDescription();
                if(Message.equals("Success"))
                {
                    team_List = response.body().getData();


                    if(team_List.size()==0)
                    {
                        System.out.println("##null");
                        LL_record.setVisibility(View.GONE);
                        LL_norecord.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        LL_record.setVisibility(View.VISIBLE);
                        LL_norecord.setVisibility(View.GONE);
                        listTeam_adapter = new My_ListTeam_Adapter(team_List, getActivity());
                        vertical_recycler_view_home.setHasFixedSize(true);
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
                        vertical_recycler_view_home.setLayoutManager(layoutManager);
                        vertical_recycler_view_home.setAdapter(listTeam_adapter);
                    }

                }
    }
         @Override
            public void onFailure(Call<Team> call, Throwable t) {
             progressDialog.dismiss();
            }
        });
    }

}
