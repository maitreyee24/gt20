package com.destek.gt20.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.destek.gt20.Adapter.Auction_Adapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Auction.Auction;
import com.destek.gt20.Model.Auction.Datum;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public  class Activity_auction extends AppCompatActivity
{
    private List<Datum> auctionList = new ArrayList<>();
    private RecyclerView recyclerView;
    private Auction_Adapter mAdapter;
    ImageView  Img_back,Img_player,Img_team_logo;
    private TextView txt_player_name,Txt_teamname,Tex_cost;
    private PreferenceSettings preferenceSettings;
    public LinearLayout LL_record,LL_norecord;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auction);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        recyclerView = findViewById(R.id.vertical_recycler_view_home);
        txt_player_name = findViewById(R.id.txt_player_name);
        Txt_teamname = findViewById(R.id.Txt_teamname);
        Tex_cost = findViewById(R.id.Tex_cost);
        Img_player = findViewById(R.id.Img_player);
        Img_team_logo = findViewById(R.id.Img_team_logo);

        LL_record = findViewById(R.id.LL_record);
        LL_norecord = findViewById(R.id.LL_norecord);

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();

        if(ProjectConfig.isNetworkConnected(Activity_auction.this))
        {
            prepareAuction();
        }
        else
        {
            Toast.makeText(Activity_auction.this,getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }


        Img_back = findViewById(R.id.Img_back);
        Img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
    }

    private void prepareAuction()
    {

        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(Activity_auction.this);
        progressDialog.show();
        progressDialog.setCancelable(false);



        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Auction> call = apiService.GetPlayerAuctionDetail("Bearer "+preferenceSettings.getTokenId());
        call.enqueue(new Callback<Auction>()
        {
            @Override
            public void onResponse(Call<Auction>call, Response<Auction> response)
            {
                progressDialog.dismiss();
                String Message = response.body().getMessage();
                String Description = response.body().getDescription();
                String Error = response.body().getError();


                if(Message.equals("Success"))
                {
                    try
                    {
                        auctionList = response.body().getData();

                        if(auctionList.size()==0)
                        {
                            System.out.println("##null");
                            LL_record.setVisibility(View.GONE);
                            LL_norecord.setVisibility(View.VISIBLE);
                        }
                        else
                            {
                            LL_record.setVisibility(View.VISIBLE);
                            LL_norecord.setVisibility(View.GONE);
                            txt_player_name.setText(auctionList.get(0).getName());
                            Txt_teamname.setText(auctionList.get(0).getTeamShortName());
                            Tex_cost.setText(auctionList.get(0).getCost());
                            auctionList.remove(0);


                            Glide.with(getApplicationContext()).load(auctionList.get(0).getTeamLogo()).error(R.mipmap.ic_launcher_round).into(Img_team_logo);
                            Glide.with(getApplicationContext()).load(auctionList.get(0).getPhoto()).error(R.mipmap.ic_launcher_round).into(Img_player);

                            auctionList.remove(0);
                            mAdapter = new Auction_Adapter(auctionList,getApplicationContext());
                            RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext());
                            recyclerView.setLayoutManager(mLayoutManager1);
                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            recyclerView.setAdapter(mAdapter);
                        }
                    }
                    catch(Exception e)
                    {
                        System.out.println("##Exception"+e);
                    }
                }
            }
            @Override
            public void onFailure(Call<Auction>call, Throwable t) {
                // Log error here since request failed
                progressDialog.dismiss();
                System.out.println("##onFailure"+ t.toString());
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mainIntent);
        finish();
    }
}
