package com.destek.gt20.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.destek.gt20.R;

public class Fragment_Popularity extends Fragment
{
    RelativeLayout RR_GOLD,RR_VIP,RR_CORPORATE,RR_SILVER;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        View view = inflater.inflate(R.layout.fragment_popularity, container, false);

        RR_GOLD = view.findViewById(R.id.RR_GOLD);
        RR_VIP = view.findViewById(R.id.RR_VIP);
        RR_CORPORATE = view.findViewById(R.id.RR_CORPORATE);
        RR_SILVER = view.findViewById(R.id.RR_SILVER);

        return  view;
    }
}

