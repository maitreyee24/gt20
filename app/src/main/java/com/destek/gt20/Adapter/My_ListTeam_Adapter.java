package com.destek.gt20.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.destek.gt20.Activities.Activity_MyTeam;
import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Favourite;
import com.destek.gt20.Model.Team.Datum;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class My_ListTeam_Adapter extends RecyclerView.Adapter<My_ListTeam_Adapter.MyViewHolder>
{
    List<Datum> team_List;
    private Context context;
    private  String Userid,teamid;
    private PreferenceSettings preferenceSettings;


    public My_ListTeam_Adapter(List<Datum> team_list, Context context)
    {
        team_List = team_list;
        this.context = context;
        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
    }

    @NonNull
    @Override
    public My_ListTeam_Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.team_list_row, parent, false);
        return new My_ListTeam_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final My_ListTeam_Adapter.MyViewHolder holder, final int position)
    {
        final Datum team = team_List.get(position);
        holder.shortname.setText(team.getTeamName());

        holder.LL.setBackgroundColor(Color.parseColor(team.getTeamColor()));


    // Glide.with(context).load(team.getTeamLogo()).error(R.mipmap.ic_launcher_round).into(holder.imageView1);

     Picasso.with(context).load(team.getTeamLogo()).placeholder(R.mipmap.ic_launcher_round).into(holder.imageView1);

        holder.LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Userid=  preferenceSettings.getUserId();
                teamid= String.valueOf(team.getTeamId());

                if(ProjectConfig.isNetworkConnected(context))
                {
                    Addto_Fav();
                }
                else
                {
                    Toast.makeText(context,"Please Check internet connection...", Toast.LENGTH_SHORT).show();
                }


                Intent mIntent = new Intent(context, Activity_MyTeam.class);
                mIntent.putExtra("Model", team_List.get(position));
                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(mIntent);
            }
        });
   }

    private void Addto_Fav()
    {

        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(context);
        progressDialog.show();
        progressDialog.setCancelable(false);

        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("UserId",Userid);
        jsonObject.addProperty("TeamId",teamid);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Favourite> call = apiService.addFav("Bearer "+preferenceSettings.getTokenId(),jsonObject);
        call.enqueue(new Callback<Favourite>()
        {
            @Override
            public void onResponse(Call<Favourite>call, Response<Favourite> response)
            {
                progressDialog.dismiss();
                String Message = response.body().getMessage();
                String Description = response.body().getDescription();


                if(Message.equals("Success"))
                {
                    Toast.makeText(context,Description,Toast.LENGTH_SHORT).show();

                }
                else {
                    Toast.makeText(context,Description,Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Favourite>call, Throwable t) {
                // Log error here since request failed
                progressDialog.dismiss();
                System.out.println("##onFailure"+ t.toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return team_List.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView imageView1;
        public TextView shortname;
        public RelativeLayout LL;
        public MyViewHolder(View view) {
            super(view);
            imageView1 = view.findViewById(R.id.imageView1);
            shortname = view.findViewById(R.id.shortname);
            LL = view.findViewById(R.id.LL);

        }
    }
}
