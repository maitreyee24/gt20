package com.destek.gt20.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.destek.gt20.Activities.Activity_Team;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Team.Datum;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.squareup.picasso.Picasso;
import java.util.List;

public class ListTeam_Adapter extends RecyclerView.Adapter<ListTeam_Adapter.MyViewHolder>
{
    List<Datum> team_List;
    private Context context;
    private PreferenceSettings preferenceSettings;

    public ListTeam_Adapter(List<Datum> team_list, Context context)
    {
        team_List = team_list;
        this.context = context;
        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.team_list_row, parent, false);
        return new ListTeam_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position)
    {
        Datum team = team_List.get(position);
        holder.shortname.setText(team.getTeamName());

        holder.LL.setBackgroundColor(Color.parseColor(team.getTeamColor()));

        Picasso.with(context).load(team.getTeamLogo()).placeholder(R.mipmap.ic_launcher_round).into(holder.imageView1);
       // Picasso.with(context).load(team.getTeamLogo()).resize(600, 200).placeholder(R.mipmap.ic_launcher_round).into(holder.imageView1);


        holder.LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
                SharedPreferences.Editor edit = pref.edit();
                edit.putString("teamid",""+position);
                edit.commit();*/
//                preferenceSettings.setTeamId(String.valueOf(position));

                Intent mIntent = new Intent(context, Activity_Team.class);
                mIntent.putExtra("Model", team_List.get(position));
                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(mIntent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return team_List.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView imageView1;
        public TextView  shortname;
        public RelativeLayout LL;
        public MyViewHolder(View view) {
            super(view);
            imageView1 = view.findViewById(R.id.imageView1);
            shortname = view.findViewById(R.id.shortname);
            LL = view.findViewById(R.id.LL);

        }
    }
}
