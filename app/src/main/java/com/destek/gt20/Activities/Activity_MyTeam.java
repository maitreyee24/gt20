package com.destek.gt20.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Fragment.Fragment_Fixture;
import com.destek.gt20.Fragment.Fragment_MyTeam_Media;
import com.destek.gt20.Fragment.Fragment_Results;
import com.destek.gt20.Fragment.Fragment_TeamDetails;
import com.destek.gt20.Model.Favourite;
import com.destek.gt20.Model.Team.Datum;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_MyTeam extends AppCompatActivity
{
        private TabLayout tabLayout;
        private ViewPager viewPager;
        ImageView Img_back,Img_logo,Img_fav;
        TextView  Txt_team_name,Txt_coach_name,Txt_change_team,txt_team_name;
        String teamid,UserId;
        LinearLayout LL_fav;
        private Datum team;
        private List<Datum> team_List =null;
        public  int team_id;
        private PreferenceSettings preferenceSettings;
        String TeamName,TeamLogo,CoachName,ShortName;


        @Override
        protected void onCreate(Bundle savedInstanceState)
        {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myteam);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();

               /* Intent i=getIntent();
                teamid=  i.getExtras().getString("teamid");*/


                Txt_team_name = findViewById(R.id.Txt_team_name);
                Txt_coach_name = findViewById(R.id.Txt_coach_name);
                Txt_change_team = findViewById(R.id.Txt_change_team);
                Img_logo = findViewById(R.id.Img_logo);
                Img_fav = findViewById(R.id.Img_fav);
                LL_fav = findViewById(R.id.LL_fav);
                txt_team_name = findViewById(R.id.txt_team_name);


                team= (Datum) getIntent().getSerializableExtra("Model");   //intent My_list_adapter
                team_id= team.getTeamId();
                TeamName=team.getTeamName();
                CoachName=team.getCoachName();
                System.out.println("##TeamName"+TeamName);

                Glide.with(getApplicationContext()).load(TeamLogo).error(R.mipmap.ic_launcher_round).into(Img_logo);
                Txt_team_name.setText(TeamName);
                txt_team_name.setText(TeamName);
                Txt_coach_name.setText(CoachName);


                preferenceSettings.setTeamId(String.valueOf(team_id));


                LL_fav.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v)
                        {
                                if(ProjectConfig.isNetworkConnected(Activity_MyTeam.this))
                                {
                                        delete_Fav();
                                }
                                else
                                {
                                        Toast.makeText(Activity_MyTeam.this,getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
                                }

                               /* Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                i.putExtra("fragmentPosition",2);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);*/

                        }
                });

                viewPager = findViewById(R.id.viewpager);
                setupViewPager(viewPager);

                tabLayout = findViewById(R.id.tabs);
                tabLayout.setupWithViewPager(viewPager);

                Img_back = findViewById(R.id.Img_back);
                Img_back.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                i.putExtra("fragmentPosition",2);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                        }
                });

          // display();
        }


        private void delete_Fav()
        {
                final TransparentProgressDialog progressDialog = new TransparentProgressDialog(Activity_MyTeam.this);
                progressDialog.show();
                progressDialog.setCancelable(false);

                JsonObject jsonObject=new JsonObject();
                jsonObject.addProperty("UserId",preferenceSettings.getUserId());


                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                Call<Favourite> call = apiService.delFav("Bearer "+preferenceSettings.getTokenId(),jsonObject);
                call.enqueue(new Callback<Favourite>()
                {
                        @Override
                        public void onResponse(Call<Favourite>call, Response<Favourite> response)
                        {
                                progressDialog.dismiss();
                                String Message = response.body().getMessage();
                                String Description = response.body().getDescription();


                                if(Message.equals("Success"))
                                {
                                        Toast.makeText(getApplicationContext(),Description,Toast.LENGTH_SHORT).show();

                                        Intent mainIntent = new Intent(getApplicationContext(),MainActivity.class);
                                        mainIntent.putExtra("fragmentPosition",2);
                                        startActivity(mainIntent);
                                        finish();

                                }
                                else
                                {
                                        //Toast.makeText(getApplicationContext(),Description,Toast.LENGTH_SHORT).show();
                                }
                        }
                        @Override
                        public void onFailure(Call<Favourite>call, Throwable t) {
                                // Log error here since request failed
                                progressDialog.dismiss();
                                System.out.println("##onFailure"+ t.toString());
                        }
                });

        }


       /* private void display()
        {
                JsonObject jsonObject=new JsonObject();
                jsonObject.addProperty("TeamId","2");

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                Call<Team> call = apiService.GetTeamByTeamId(jsonObject);
                call.enqueue(new Callback<Team>()
                {
                        @Override
                        public void onResponse(Call<Team>call, Response<Team> response)
                        {
                                if(response.body().getMessage().equals("Success"))
                                {
                                 team_List = response.body().getData();

                                 for(int i=0;i<team_List.size();i++)
                                           {
                                                    TeamName = team_List.get(i).getTeamName();
                                                    TeamLogo = team_List.get(i).getTeamName();
                                                    CoachName = team_List.get(i).getTeamName();
                                                    ShortName = team_List.get(i).getTeamName();

                                                   System.out.println("##TeamName"+TeamName);
                                                   System.out.println("##CoachName"+CoachName);
                                                   System.out.println("##TeamLogo"+TeamLogo);
                                          }

                                }
                        }
                        @Override
                        public void onFailure(Call<Team>call, Throwable t) {

                                System.out.println("##onFailure"+ t.toString());
                        }
                });
        }*/


        private void setupViewPager(ViewPager viewPager)
        {
                ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
                adapter.addFragment(new Fragment_Fixture(), "Fixture");
                adapter.addFragment(new Fragment_Results(), "Results");
                adapter.addFragment(new Fragment_TeamDetails(), "Team");
                adapter.addFragment(new Fragment_MyTeam_Media(), "Media");
                viewPager.setAdapter(adapter);
        }

        static class ViewPagerAdapter extends FragmentPagerAdapter
        {
                private final List<Fragment> mFragmentList = new ArrayList<>();
                private final List<String> mFragmentTitleList = new ArrayList<>();

                public ViewPagerAdapter(FragmentManager manager) {
                        super(manager);
                }

                @Override
                public Fragment getItem(int position) {
                        return mFragmentList.get(position);
                }

                @Override
                public int getCount() {
                        return mFragmentList.size();
                }

                public void addFragment(Fragment fragment, String title) {
                        mFragmentList.add(fragment);
                        mFragmentTitleList.add(title);
                }

                @Override
                public CharSequence getPageTitle(int position) {
                        return mFragmentTitleList.get(position);
                }
        }
}
