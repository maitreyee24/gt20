package com.destek.gt20.Model.Home_Stream;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetStreamCommentDetail {

    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("Comments")
    @Expose
    private String comments;
    @SerializedName("Time")
    @Expose
    private String time;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}