package com.destek.gt20.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.IsMobileNumberExist;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import com.hbb20.CountryCodePicker;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Activity_Signup extends AppCompatActivity
{
    Button Btn_signup;
    EditText txt_name, txt_email, txt_number, txt_login_Pass, txt_confirm_Pass;
    String name, email, number, passwrd, confirm_passwrd, countryCode;
    CountryCodePicker ccpLogin;
    TextView Txt_Login,txt_terms,txt_privacy,txt_rules;
    public CountryCodePicker countryCodePicker;
    public PreferenceSettings preferenceSettings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        txt_name = findViewById(R.id.txt_name);
        txt_email = findViewById(R.id.txt_email);
        txt_number = findViewById(R.id.txt_number);
        txt_login_Pass = findViewById(R.id.txt_login_Pass);
        txt_confirm_Pass = findViewById(R.id.txt_confirm_Pass);
        Txt_Login = findViewById(R.id.Txt_Login);
        ccpLogin = findViewById(R.id.ccp_login);

        txt_terms = findViewById(R.id.txt_terms);
        txt_privacy = findViewById(R.id.txt_privacy);
        txt_rules = findViewById(R.id.txt_rules);

        countryCodePicker = (CountryCodePicker) findViewById(R.id.ccp_login);
        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();


        //remove Emojis
//        txt_name.setFilters(new InputFilter[]{EMOJI_FILTER});
//        txt_name.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS|InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

//        txt_email.setFilters(new InputFilter[]{EMOJI_FILTER});
//        txt_email.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS|InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);


//        txt_login_Pass.setFilters(new InputFilter[]{EMOJI_FILTER});
//        txt_login_Pass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS|InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);


//        txt_confirm_Pass.setFilters(new InputFilter[]{EMOJI_FILTER});
//        txt_confirm_Pass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS|InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

        txt_login_Pass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                String sample = s.toString();
                if(sample.contains(" "))
                {
                    sample = sample.replace(" ","");
                    txt_login_Pass.setText(new SpannableStringBuilder(sample));
                    txt_login_Pass.setSelection(txt_login_Pass.length());
                }

            }
        });

        txt_confirm_Pass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                String sample = s.toString();
                if(sample.contains(" "))
                {
                    sample = sample.replace(" ","");
                    txt_confirm_Pass.setText(new SpannableStringBuilder(sample));
                    txt_confirm_Pass.setSelection(txt_confirm_Pass.length());
                }

            }
        });

        txt_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                String sample = s.toString();
                if(sample.contains(" "))
                {
                    sample = sample.replace(" ","");
                    txt_email.setText(new SpannableStringBuilder(sample));
                    txt_email.setSelection(txt_email.length());
                }

            }
        });


        Txt_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(getApplicationContext(), Activity_Login.class);
                startActivity(mainIntent);
                finish();
            }
        });


        txt_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(getApplicationContext(), Activity_Term.class);
                loginIntent.putExtra("header", "Terms and Conditions");
                loginIntent.putExtra("contentLink", "http://13.234.196.151/Img/StaticPages/t&c.html");
                startActivity(loginIntent);
            }
        });
        txt_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(getApplicationContext(), Activity_Term.class);
                loginIntent.putExtra("header", "Privacy Policy");
                loginIntent.putExtra("contentLink", "http://13.234.196.151/Img/StaticPages/privacy.html");
                startActivity(loginIntent);
            }
        });
        txt_rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent loginIntent = new Intent(getApplicationContext(), Activity_Term.class);
                loginIntent.putExtra("header", "Rules and Regulations");
                loginIntent.putExtra("contentLink", "http://13.234.196.151/Img/StaticPages/rules.html");
                startActivity(loginIntent);
            }
        });



        Btn_signup = findViewById(R.id.Btn_signup);
        Btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid())
                {
                    IsMobileNumberExist();
                }
            }
        });
    }

    public static boolean isEmailValid(String email)
    {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    private boolean isValid()
    {
        name = txt_name.getText().toString().trim();
        email = txt_email.getText().toString().trim();
        number = txt_number.getText().toString().trim();
        passwrd = txt_login_Pass.getText().toString().trim();
        confirm_passwrd = txt_confirm_Pass.getText().toString().trim();
        countryCode = ccpLogin.getSelectedCountryCode();


        if (name.isEmpty()) {
            txt_name.setError("Please enter Name");
            return false;
        } else if (email.isEmpty()) {
            txt_email.setError("Please enter Email");
            return false;
        }else if(!isEmailValid(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            txt_email.setError("Please enter Valid Email");
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            txt_email.setError("Please enter valid Email");
            return false;
        } else if (number.isEmpty()) {
            txt_number.setError("Please enter Mobile Number");
            return false;
        } else if (!android.util.Patterns.PHONE.matcher(number).matches() || number.length() != 10) {
            txt_number.setError("Please enter valid Mobile Number");
            return false;
        } else if (passwrd.isEmpty()) {
            txt_login_Pass.setError("Please Enter Password");
            return false;
        } else if (passwrd.length() < 6) {
            txt_login_Pass.setError("Password must have minimum 6 characters");
            return false;
        } else if (confirm_passwrd.isEmpty()) {
            txt_confirm_Pass.setError("Please Enter Confirm Password");
            return false;
        } else if (confirm_passwrd.length() < 6) {
            txt_confirm_Pass.setError("Confirm Password must have minimum 6 characters");
            return false;
        } else if (!passwrd.equals(confirm_passwrd)) {
            Toast.makeText(getApplicationContext(), "Password Mis-match", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }



    private void IsMobileNumberExist()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(Activity_Signup.this);
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("CountryCode",countryCodePicker.getSelectedCountryCode());
        jsonObject.addProperty("Mobile", number);
        jsonObject.addProperty("Email", " ");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        System.out.println("##preferenceSettings.getTokenId()"+preferenceSettings.getTokenId());
        Call<IsMobileNumberExist> call = apiService.IsMobileNumberExist(jsonObject);
        call.enqueue(new Callback<IsMobileNumberExist>() {
            @Override
            public void onResponse(Call<IsMobileNumberExist> call, Response<IsMobileNumberExist> response)
            {

                progressDialog.dismiss();

                String Message = response.body().getMessage();
                String Description = response.body().getDescription();
                Boolean IsRegistred = response.body().getIsRegistred();

                if (IsRegistred==true)
                {
                    Toast.makeText(getApplicationContext(), "Mobile number already exist", Toast.LENGTH_SHORT).show();

                } else
                    {
                        Intent mainIntent = new Intent(getApplicationContext(), Activity_OTP_Verification.class);
                        mainIntent.putExtra("name", name);
                        mainIntent.putExtra("email", email);
                        mainIntent.putExtra("number", number);
                        mainIntent.putExtra("passwrd", passwrd);
                        mainIntent.putExtra("country_code", countryCode);
                        startActivity(mainIntent);
                        finish();
                }
            }

            @Override
            public void onFailure(Call<IsMobileNumberExist> call, Throwable t) {
                // Log error here since request failed
                System.out.println("##onFailure" + t.toString());
                progressDialog.dismiss();
            }
        });
    }


    /*private void SendOTP()
    {

        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(Activity_Signup.this);
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("CountryCode","+"+ccpLogin.getSelectedCountryCode());
        jsonObject.addProperty("Mobile",number);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

//        Call<SendOTP> call = apiService.SendOTP("Bearer "+preferenceSettings.getTokenId(),jsonObject);
        Call<SendOTP> call = apiService.SendOTP(jsonObject);
        call.enqueue(new Callback<SendOTP>()
        {
            @Override
            public void onResponse(Call<SendOTP>call, Response<SendOTP> response)
            {
                progressDialog.dismiss();
                System.out.println("##response"+response);
                Message = response.body().getMessage();
                Description = response.body().getDescription();
                OTP = response.body().getOTP();

                System.out.println("##OTP"+OTP);

                if(Message.equals("Success"))
                {
                    Toast.makeText(getApplicationContext(),Description,Toast.LENGTH_SHORT).show();
                   *//* if(!otp.equals(""))
                    { VerifyOTP();}
*//*
                }
                else
                {
                    Toast.makeText(getApplicationContext(),Description,Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<SendOTP>call, Throwable t) {
                // Log error here since request failed
                progressDialog.dismiss();
                System.out.println("##onFailure"+ t.toString());
            }
        });

    }
*/



    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent mainIntent = new Intent(getApplicationContext(), Activity_Login.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mainIntent);
        finish();
    }

}
