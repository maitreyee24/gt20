package com.destek.gt20.VR;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import com.destek.gt20.R;
import com.google.vrtoolkit.cardboard.CardboardActivity;
import com.google.vrtoolkit.cardboard.CardboardView;

import java.io.IOException;

public class CardBoardActivity extends CardboardActivity
{

    private CardboardView cardboardView;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // set all views
        setContentView(R.layout.activity_cardboard_view);
        cardboardView = (CardboardView) findViewById(R.id.cardboard_view);


        // init media player for video
        //Uri uri =  Uri.parse( "https://r3---sn-np2a-2o9e.googlevideo.com/videoplayback?ei=cLQ1XMz8CInQz7sPhpyMuAs&ms=au%2Crdu&ipbits=0&mv=m&pl=24&ip=183.87.70.24&id=o-AE29-kSXj5gndmVM-y4dYpySTzxjSnSKyx5WfeTMEzHu&mt=1547023363&mm=31%2C29&expire=1547045072&txp=5531332&itag=22&mn=sn-np2a-2o9e%2Csn-cvh76n7k&sparams=dur%2Cei%2Cgcr%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cpl%2Cratebypass%2Crequiressl%2Csource%2Cexpire&signature=B22F638634C4036AB8747B426541CE999F4EAC54.5B81B2DAE7D9A188C15694589CBB915E2D06CFEE&requiressl=yes&initcwndbps=975000&source=youtube&gcr=in&mime=video%2Fmp4&key=yt6&ratebypass=yes&dur=117.283&lmt=1538037980462434&fvip=3&c=WEB" );
        //Uri uri =  Uri.parse("https://media-a.kpoint.com/data.us-east-1.kpoint/ktpl.kpoint.in/ktpl.kpoint.com/kapsule/gcc-1d8be971-97eb-4471-92d9-fd0455d14566/v5/view/html5/master.m3u8?Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiaHR0cHM6Ly9tZWRpYS1hLmtwb2ludC5jb20vZGF0YS51cy1lYXN0LTEua3BvaW50L2t0cGwua3BvaW50LmluL2t0cGwua3BvaW50LmNvbS9rYXBzdWxlL2djYy0xZDhiZTk3MS05N2ViLTQ0NzEtOTJkOS1mZDA0NTVkMTQ1NjYvdjUvdmlldy9odG1sNS8qIiwiQ29uZGl0aW9uIjp7IkRhdGVMZXNzVGhhbiI6eyJBV1M6RXBvY2hUaW1lIjoxNTQ3NjI4NDg4fX19XX0_&Signature=ScFM66CJ0evwLpaOf9R7xF4SY6oRtabvyNYxvelhD6urpN~AUabadQsIlGoZVYooA49aY7tiBpbmCwOzsiEsCz8tv54LNu4mqlYzqqaIymZQq5iSyFUyctXazXRbw6cKdTC56XSd5Au40cIb~uQIHE4SbzimAOWHRBwGDqN53IlHdU0dwYbkSLlHVxI78rbKsg1w0-982QiFQtj3LKClxGqvGVjW-Qo8e5KqPAaq8HivFPDB0sdNN7g76JWg-DFkfjgvolkrdTxoviF4LRzJ9RHCvaCdcVzJ96xUT86dSl7gYKCM1EWfF1N-a61Gt3MdZ6RYXzNSicMin1wKRN23nQ__&Key-Pair-Id=APKAJAYO3WONEQCUMGWQ&cch=m1&kPolicy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiaHR0cHM6Ly9tZWRpYS1hLmtwb2ludC5jb20vZGF0YS51cy1lYXN0LTEua3BvaW50L2t0cGwua3BvaW50LmluL2t0cGwua3BvaW50LmNvbS9rYXBzdWxlL2djYy0xZDhiZTk3MS05N2ViLTQ0NzEtOTJkOS1mZDA0NTVkMTQ1NjYvdjUvdmlldy9odG1sNS8qIiwiQ29uZGl0aW9uIjp7IkRhdGVMZXNzVGhhbiI6eyJBV1M6RXBvY2hUaW1lIjoxNTQ3NjI4NDg4fX19XX0_&kSignature=ScFM66CJ0evwLpaOf9R7xF4SY6oRtabvyNYxvelhD6urpN~AUabadQsIlGoZVYooA49aY7tiBpbmCwOzsiEsCz8tv54LNu4mqlYzqqaIymZQq5iSyFUyctXazXRbw6cKdTC56XSd5Au40cIb~uQIHE4SbzimAOWHRBwGDqN53IlHdU0dwYbkSLlHVxI78rbKsg1w0-982QiFQtj3LKClxGqvGVjW-Qo8e5KqPAaq8HivFPDB0sdNN7g76JWg-DFkfjgvolkrdTxoviF4LRzJ9RHCvaCdcVzJ96xUT86dSl7gYKCM1EWfF1N-a61Gt3MdZ6RYXzNSicMin1wKRN23nQ__&kKey_Pair_Id=APKAJAYO3WONEQCUMGWQ&kcch=m1" );
        //Uri uri = Uri.parse("https://s3-eu-west-1.amazonaws.com/dahlia-bucket-hls/hls/demo.m3u8");


      Uri uri = Uri.parse(getIntent().getStringExtra("uri"));

      System.out.println("##uri::"+uri);

       mediaPlayer = MediaPlayer.create(this, uri);
       mediaPlayer.setLooping(true);

        // create renderer for all opengl stuff
        CardboardView.StereoRenderer renderer = new VideoPanoramaRenderer(this, mediaPlayer);
        // associate a renderer with cardboardView
        cardboardView.setRenderer(renderer);

        // associate the cardboardView with this activity
        setCardboardView(cardboardView);
    }

    @Override
    public void onCardboardTrigger() {
        super.onCardboardTrigger();

        // toggle vr mode on touch/trigger
        cardboardView.setVRModeEnabled(!cardboardView.getVRMode());
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mediaPlayer != null) {
            mediaPlayer.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mediaPlayer != null) {
            mediaPlayer.start();
        }
    }
}