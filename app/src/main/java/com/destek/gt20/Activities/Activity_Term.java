package com.destek.gt20.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import com.destek.gt20.R;

public  class Activity_Term extends AppCompatActivity
{
    private ImageView  Img_back;
    private TextView txtHeader;
    private WebView webView;

    private String header, contentLink;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        header = getIntent().getStringExtra("header");
        contentLink = getIntent().getStringExtra("contentLink");

        findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        txtHeader = findViewById(R.id.terms_header);
        txtHeader.setText(header);
        webView = findViewById(R.id.webView);
        webView.loadUrl(contentLink);

    }
}
