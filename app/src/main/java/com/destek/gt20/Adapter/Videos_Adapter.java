package com.destek.gt20.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.destek.gt20.Model.Media.Datum_Videos;
import com.destek.gt20.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Videos_Adapter extends RecyclerView.Adapter<Videos_Adapter.MyViewHolder>
{
    private List<Datum_Videos> video_list;
    public Context context;

    public Videos_Adapter(List<Datum_Videos> video_list, Context context) {
        this.video_list = video_list;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_list_row, parent, false);
        return new Videos_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Datum_Videos videos = video_list.get(position);
        holder.Txt_subtitle.setText(videos.getHeading());
        holder.Txt_date.setText(videos.getInsDt());

        System.out.println("##videos.getThumnails()"+videos.getThumnails());

       // Glide.with(context).load(videos.getThumnails()).error(R.mipmap.ic_launcher_round).into(holder.mWebView);

        Picasso.with(context).load(videos.getThumnails()).resize(6000, 2000)
                .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(holder.mWebView);


        holder.mWebView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                playVideo(videos.getImageName());
            }
        });
    }


    public void playVideo(String key)
    {

        Uri uri = Uri.parse(key);

        // With this line the Youtube application, if installed, will launch immediately.
        // Without it you will be prompted with a list of the application to choose.
        uri = Uri.parse("vnd.youtube:" + uri.getQueryParameter("v"));
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        context.startActivity(intent);
    }


    @Override
    public int getItemCount() {
        return video_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView Txt_title, Txt_subtitle, Txt_date, Txt_time;
        public ImageView mWebView;


        public MyViewHolder(View view) {
            super(view);
            Txt_title = view.findViewById(R.id.Txt_title);
            Txt_subtitle = view.findViewById(R.id.Txt_subtitle);
            Txt_date = view.findViewById(R.id.Txt_date);
            Txt_time = view.findViewById(R.id.Txt_time);
            mWebView = view.findViewById(R.id.mWebView);
        }
    }
}
