package com.destek.gt20.Model;

public class Prize
{
    String title,subtitle,cost;

    public Prize(String title, String subtitle, String cost) {
        this.title = title;
        this.subtitle = subtitle;
        this.cost = cost;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }
}
