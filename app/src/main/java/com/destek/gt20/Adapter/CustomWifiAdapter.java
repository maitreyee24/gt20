package com.destek.gt20.Adapter;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Wifi;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomWifiAdapter extends RecyclerView.Adapter<CustomWifiAdapter.MyViewHolder> {
    private List<ScanResult> wifilist;
    Context context;
    String wifi_name, password;
    private PreferenceSettings preferenceSettings;

    WifiManager wifiManager;

    public CustomWifiAdapter(Context context, List<ScanResult> auctionList) {
        this.wifilist = auctionList;
        this.context = context;
        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.connect_wifi_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.Txt_name.setText(wifilist.get(position).SSID);
        holder.btn_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Verifywifi(wifilist.get(position).SSID);
            }
        });
    }

    private void Verifywifi(final String wifi_name)
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(context);
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("UserName", wifi_name);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Wifi> call = apiService.GetWifiCredential("Bearer " + preferenceSettings.getTokenId(), jsonObject);
        call.enqueue(new Callback<Wifi>() {
            @Override
            public void onResponse(Call<Wifi> call, Response<Wifi> response) {

                progressDialog.dismiss();

                String Message = response.body().getMessage();

                if (Message.equals("Success")) {
                    password = response.body().getData();
                    boolean result = ConnectToWiFi(wifi_name, password, context);
                }
            }

            @Override
            public void onFailure(Call<Wifi> call, Throwable t) {
                // Log error here since request failed
                System.out.println("##onFailure" + t.toString());
                progressDialog.dismiss();
            }
        });
    }


    @Override
    public int getItemCount() {
        return wifilist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView Txt_name;
        private Button btn_connect;


        public MyViewHolder(View view) {
            super(view);
            Txt_name = view.findViewById(R.id.Txt_name);
            btn_connect = view.findViewById(R.id.btn_connect);
        }
    }


    static public boolean ConnectToWiFi(String ssid, String key, Context ctx) {

        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", ssid);
        wifiConfig.preSharedKey = String.format("\"%s\"", key);
        WifiManager wifiManager = (WifiManager) ctx.getSystemService(ctx.WIFI_SERVICE);
        int networkId = wifiManager.getConnectionInfo().getNetworkId();
        wifiManager.removeNetwork(networkId);
        wifiManager.saveConfiguration();
        Toast.makeText(ctx, "Connected to the network", Toast.LENGTH_SHORT).show();

        //remember id
        int netId = wifiManager.addNetwork(wifiConfig);
        wifiManager.disconnect();
        boolean result = wifiManager.enableNetwork(netId, true);
        wifiManager.reconnect();

        return result;
    }

}
