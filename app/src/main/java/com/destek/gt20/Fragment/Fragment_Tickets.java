package com.destek.gt20.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.destek.gt20.Activities.Activity_auction;
import com.destek.gt20.Activities.MainActivity;
import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.Adapter.Auction_Adapter;
import com.destek.gt20.Adapter.Ticket_Adapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Ticket_Booking.Datum;
import com.destek.gt20.Model.Ticket_Booking.Tickets;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_Tickets extends Fragment
{
    private List<Datum> Tickets_List = new ArrayList<>();
    private RecyclerView recyclerView;
    private Ticket_Adapter mAdapter;
     private PreferenceSettings preferenceSettings;
    public LinearLayout LL_record,LL_norecord;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        View view = inflater.inflate(R.layout.fragment_tickets, container, false);

        recyclerView = view.findViewById(R.id.vertical_recycler_view_home);
              preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();

        LL_record =view.findViewById(R.id.LL_record);
        LL_norecord = view.findViewById(R.id.LL_norecord);


        if(ProjectConfig.isNetworkConnected(getActivity()))
        {
            prepareTickets();
        }
        else
        {
            Toast.makeText(getActivity(),getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }


        return  view;
    }

    private void prepareTickets()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity());
        progressDialog.show();
        progressDialog.setCancelable(false);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Tickets> call = apiService.GetUpcommingMatches("Bearer "+preferenceSettings.getTokenId());
        call.enqueue(new Callback<Tickets>() {
            @Override
            public void onResponse(Call<Tickets> call, Response<Tickets> response)
            {
                progressDialog.dismiss();
                String Message = response.body().getMessage();
                String Description = response.body().getDescription();
                String Error = response.body().getError();


                if (Message.equals("Success"))
                {
                    Tickets_List=response.body().getData();


                    if(Tickets_List.size()==0)
                    {
                        System.out.println("##null");
                        LL_record.setVisibility(View.GONE);
                        LL_norecord.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        LL_record.setVisibility(View.VISIBLE);
                        LL_norecord.setVisibility(View.GONE);

                        mAdapter = new Ticket_Adapter(Tickets_List,getActivity());
                        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
                        recyclerView.setLayoutManager(mLayoutManager1);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(mAdapter);


                    }
                }
            }

            @Override
            public void onFailure(Call<Tickets> call, Throwable t) {
                // Log error here since request failed
                progressDialog.dismiss();
                System.out.println("##onFailure" + t.toString());
            }
        });

    }
}
