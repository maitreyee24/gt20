package com.destek.gt20.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.destek.gt20.Model.Offer;
import com.destek.gt20.R;

import java.util.List;

public class Offer_Adapter  extends RecyclerView.Adapter<Offer_Adapter.MyViewHolder>
{
    private List<Offer> offerList;
    public Offer_Adapter(List<Offer> offerList) {
        this.offerList=offerList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.offer_list_row, parent, false);
        return new Offer_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Offer offer = offerList.get(position);
        holder.Txt_title.setText(offer.getTitle());
        holder.Txt_subtitle.setText(offer.getSubtitle());
        holder.Tex_cost.setText(offer.getCost());
    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView Txt_title, Txt_subtitle, Tex_cost;

        public MyViewHolder(View view) {
            super(view);
            Txt_title = view.findViewById(R.id.Txt_title);
            Txt_subtitle = view.findViewById(R.id.Txt_subtitle);
            Tex_cost = view.findViewById(R.id.Tex_cost);
        }
    }
}
