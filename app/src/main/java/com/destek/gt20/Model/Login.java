package com.destek.gt20.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login
{
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("MatchDays")
    @Expose
    private String matchDays;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("IsFormFilled")
    @Expose
    private Boolean isFormFilled;
    @SerializedName("ProfileImg")
    @Expose
    private String profileImg;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMatchDays() {
        return matchDays;
    }

    public void setMatchDays(String matchDays) {
        this.matchDays = matchDays;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getIsFormFilled() {
        return isFormFilled;
    }

    public void setIsFormFilled(Boolean isFormFilled) {
        this.isFormFilled = isFormFilled;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

}