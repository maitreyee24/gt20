package com.destek.gt20.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.destek.gt20.Activities.Activity_auction;
import com.destek.gt20.Activities.MainActivity;
import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Notification.Datum_notification;
import com.destek.gt20.Model.Notification.Notification;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Notification_Adapter extends RecyclerView.Adapter<Notification_Adapter.MyViewHolder>
{
    private List<Datum_notification> notificationList;
    Context context;
    private PreferenceSettings preferenceSettings;

    public Notification_Adapter(List<Datum_notification> notificationList, Context context)
    {
        this.notificationList = notificationList;
        this.context = context;
        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notify_row, parent, false);
        return new Notification_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final Datum_notification notify = notificationList.get(position);
        holder.txt_time.setText(notify.getRemindarNo()+" "+notify.getRemindarType());
        holder.Img_cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(ProjectConfig.isNetworkConnected(context))
                {
                    delete_notification(notify.getNotificationId());
                }
                else
                {
                    Toast.makeText(context,"Please Check Internet Connection...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void delete_notification(Integer notificationId)
    {
      final TransparentProgressDialog progressDialog = new TransparentProgressDialog(context);
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("NotificationId", "1");


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Notification> call = apiService.DeleteNotification("Bearer "+preferenceSettings.getTokenId(),jsonObject);
        call.enqueue(new Callback<Notification>() {
            @Override
            public void onResponse(Call<Notification> call, Response<Notification> response) {

                progressDialog.dismiss();

                String Message = response.body().getMessage();
                String Description = response.body().getDescription();

                if (Message.equals("Success")) {

                    Toast.makeText(context, Description, Toast.LENGTH_SHORT).show();

                    Intent mainIntent = new Intent(context, MainActivity.class);
                    context. startActivity(mainIntent);

                }
            }
            @Override
            public void onFailure(Call<Notification> call, Throwable t) {
                // Log error here since request failed

                System.out.println("##onFailure" + t.toString());
               progressDialog.dismiss();
            }
        });
    }


    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_time;
        public ImageView Img_cancel;

        public MyViewHolder(View view) {
            super(view);
            txt_time = view.findViewById(R.id.txt_time);
            Img_cancel = view.findViewById(R.id.Img_cancel);
        }
    }
}
