package com.destek.gt20.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.destek.gt20.Model.Player_OfSeries.Datum;
import com.destek.gt20.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PlayerofSeries_Adapter extends RecyclerView.Adapter
{

    private List<Datum> playerof_series_list = new ArrayList<>();
    private Context context;


    public PlayerofSeries_Adapter(List<Datum> playerof_series_list, Context context)
    {
        this.playerof_series_list = playerof_series_list;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;
        switch (viewType)
        {
            case 0:
                view = LayoutInflater.from(context).inflate(R.layout.players_top_view, viewGroup, false);
                return new TopViewHolder(view);
            case 1:
                view = LayoutInflater.from(context).inflate(R.layout.playerofserieslist_row, viewGroup, false);
                return new OtherViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i)
    {
        if (i == 0)
    {
            ((TopViewHolder) holder).Txt_name.setText(playerof_series_list.get(0).getName());
            ((TopViewHolder) holder).Txt_team_name.setText(playerof_series_list.get(0).getShortName());
            ((TopViewHolder) holder).Txt_wickets.setText("" + playerof_series_list.get(0).getHighestWickets());
            ((TopViewHolder) holder).Txt_runs.setText("" + playerof_series_list.get(0).getHighestRuns());

            Glide.with(context).load(playerof_series_list.get(0).getTeamLogo()).error(R.mipmap.ic_launcher_round).into(((TopViewHolder) holder).Img_team_logo);
            Glide.with(context).load(playerof_series_list.get(0).getPhoto()).error(R.mipmap.ic_launcher_round).into(((TopViewHolder) holder).Img_player);
        } else {
            Datum playerofSeries = playerof_series_list.get(i);

            ((OtherViewHolder) holder).Txt_pos.setText("" + Integer.parseInt(String.valueOf(i + 1)) + ".");
            ((OtherViewHolder) holder).Txt_name.setText(playerofSeries.getName());
            ((OtherViewHolder) holder).Txt_Runs.setText("" + playerofSeries.getHighestRuns());
            ((OtherViewHolder) holder).Txt_wickets.setText("" + playerofSeries.getHighestWickets());

           // Glide.with(context).load(playerofSeries.getPhoto()).error(R.mipmap.ic_launcher_round).into(((OtherViewHolder) holder).Img_player);
           // Glide.with(context).load(playerofSeries.getTeamLogo()).error(R.mipmap.ic_launcher_round).into(((OtherViewHolder) holder).Img_team_logo);

            Picasso.with(context).load(playerofSeries.getPhoto()).resize(6000, 2000)
                    .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(((OtherViewHolder) holder).Img_player);
            Picasso.with(context).load(playerofSeries.getTeamLogo()).resize(6000, 2000)
                    .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(((OtherViewHolder) holder).Img_team_logo);

        }
    }

    @Override
    public int getItemCount() {
        return playerof_series_list.size();
    }

    public static class TopViewHolder extends RecyclerView.ViewHolder
    {
        private TextView Txt_name, Txt_team_name, Txt_wickets, Txt_runs;
        private ImageView Img_team_logo, Img_player;

        TopViewHolder(View view) {
            super(view);
            Txt_name = view.findViewById(R.id.Txt_name);
            Txt_wickets = view.findViewById(R.id.Txt_wickets);
            Txt_runs = view.findViewById(R.id.Txt_runs);
            Txt_team_name = view.findViewById(R.id.Txt_team_name);
            Img_team_logo = view.findViewById(R.id.Img_team_logo);
            Img_player = view.findViewById(R.id.Img_player);

        }
    }

    public static class OtherViewHolder extends RecyclerView.ViewHolder
    {
        TextView Txt_pos, Txt_name, Txt_wickets, Txt_Runs;
        private ImageView Img_player, Img_team_logo;

        OtherViewHolder(View view) {
            super(view);
            Txt_pos = view.findViewById(R.id.Txt_pos);
            Txt_name = view.findViewById(R.id.Txt_name);
            Txt_Runs = view.findViewById(R.id.Txt_Runs);
            Txt_wickets = view.findViewById(R.id.Txt_wickets);

            Img_player = view.findViewById(R.id.Img_player);
            Img_team_logo = view.findViewById(R.id.Img_team_logo);

        }
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0) {
            return 0;
        }
        return 1;
    }
}
