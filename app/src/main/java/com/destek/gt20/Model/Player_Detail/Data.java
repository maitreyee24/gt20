package com.destek.gt20.Model.Player_Detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("PlayerId")
    @Expose
    private Integer playerId;
    @SerializedName("PlayedMatches")
    @Expose
    private String playedMatches;
    @SerializedName("TotalRuns")
    @Expose
    private String totalRuns;
    @SerializedName("HighestRuns")
    @Expose
    private String highestRuns;
    @SerializedName("Fours")
    @Expose
    private String fours;
    @SerializedName("Sixes")
    @Expose
    private String sixes;
    @SerializedName("StrikeRate")
    @Expose
    private String strikeRate;
    @SerializedName("HighestWickets")
    @Expose
    private String highestWickets;
    @SerializedName("TotalWickets")
    @Expose
    private String totalWickets;
    @SerializedName("Economy")
    @Expose
    private String economy;
    @SerializedName("BowlingMatch")
    @Expose
    private String bowlingMatch;

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getPlayedMatches() {
        return playedMatches;
    }

    public void setPlayedMatches(String playedMatches) {
        this.playedMatches = playedMatches;
    }

    public String getTotalRuns() {
        return totalRuns;
    }

    public void setTotalRuns(String totalRuns) {
        this.totalRuns = totalRuns;
    }

    public String getHighestRuns() {
        return highestRuns;
    }

    public void setHighestRuns(String highestRuns) {
        this.highestRuns = highestRuns;
    }

    public String getFours() {
        return fours;
    }

    public void setFours(String fours) {
        this.fours = fours;
    }

    public String getSixes() {
        return sixes;
    }

    public void setSixes(String sixes) {
        this.sixes = sixes;
    }

    public String getStrikeRate() {
        return strikeRate;
    }

    public void setStrikeRate(String strikeRate) {
        this.strikeRate = strikeRate;
    }

    public String getHighestWickets() {
        return highestWickets;
    }

    public void setHighestWickets(String highestWickets) {
        this.highestWickets = highestWickets;
    }

    public String getTotalWickets() {
        return totalWickets;
    }

    public void setTotalWickets(String totalWickets) {
        this.totalWickets = totalWickets;
    }

    public String getEconomy() {
        return economy;
    }

    public void setEconomy(String economy) {
        this.economy = economy;
    }

    public String getBowlingMatch() {
        return bowlingMatch;
    }

    public void setBowlingMatch(String bowlingMatch) {
        this.bowlingMatch = bowlingMatch;
    }

}