package com.destek.gt20.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.destek.gt20.Activities.Activity_News_Detail;
import com.destek.gt20.Model.Media.Datum_News;
import com.destek.gt20.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class News_Adapter  extends RecyclerView.Adapter<News_Adapter.MyViewHolder>
{
    private List<Datum_News> news_list;
    public Context context;

    public News_Adapter(List<Datum_News> news_list,Context context)
    {
        this.news_list = news_list;
        this.context=context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_list_row, parent, false);
        return new News_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        Datum_News news = news_list.get(position);
        holder.Txt_title.setText(news.getHeading());
        holder.Txt_subtitle.setText(news.getNewsDetail());
        holder.Txt_date.setText(news.getInsDt());
        holder.Txt_time.setText(news.getTime());

        //Glide.with(context).load(news.getThumnails()).error(R.mipmap.ic_launcher_round).into(holder.Img);


        Picasso.with(context).load(news.getThumnails()).resize(6000, 2000)
                .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(holder.Img);

        holder.master_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Activity_News_Detail.class);
                intent.putExtra("News", news_list.get(position));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return news_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView Txt_title, Txt_subtitle, Txt_date,Txt_time;
        public ImageView Img;
        public RelativeLayout master_layout;

        public MyViewHolder(View view) {
            super(view);
            Txt_title = view.findViewById(R.id.Txt_title);
            Txt_subtitle = view.findViewById(R.id.Txt_subtitle);
            Txt_date = view.findViewById(R.id.Txt_date);
            Txt_time = view.findViewById(R.id.Txt_time);
            Img = view.findViewById(R.id.Img);
            master_layout = view.findViewById(R.id.master_layout);
        }
    }
}
