package com.destek.gt20;

import com.destek.gt20.Model.Advertisement.Advertisement;
import com.destek.gt20.Model.Auction.Auction;
import com.destek.gt20.Model.Favourite;
import com.destek.gt20.Model.GetDays.DayList;
import com.destek.gt20.Model.HighestRuns.HighestRuns;
import com.destek.gt20.Model.HighestWickets.HighestWickets;
import com.destek.gt20.Model.Home_Stream.HomeStream;
import com.destek.gt20.Model.IsMobileNumberExist;
import com.destek.gt20.Model.Login;
import com.destek.gt20.Model.Media.News;
import com.destek.gt20.Model.Media.Pictures;
import com.destek.gt20.Model.Media.Videos;
import com.destek.gt20.Model.Notification.GetNotification;
import com.destek.gt20.Model.Notification.Notification;
import com.destek.gt20.Model.OTPVerification;
import com.destek.gt20.Model.OTPVerified;
import com.destek.gt20.Model.Password.Password;
import com.destek.gt20.Model.Player_Detail.PlayerDetail;
import com.destek.gt20.Model.Player_OfSeries.PlayerofSeries;
import com.destek.gt20.Model.PointTable.PointTable;
import com.destek.gt20.Model.ProfilePicture.ProfilePic;
import com.destek.gt20.Model.ReceivedNotification.GetReceivedNotification;
import com.destek.gt20.Model.Registration;
import com.destek.gt20.Model.Schedule.Schedule;
import com.destek.gt20.Model.Scorecard.Scorecard;
import com.destek.gt20.Model.Seasons.Seasons;
import com.destek.gt20.Model.SendOTP;
import com.destek.gt20.Model.SocialLogin;
import com.destek.gt20.Model.Team.Team;
import com.destek.gt20.Model.PlayerList.TeamPictures;
import com.destek.gt20.Model.Ticket_Booking.Tickets;
import com.destek.gt20.Model.Wifi;
import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;


public interface ApiInterface
{

    @POST("User/Registration")
    Call<Registration> doRegistration(@Body JsonObject jsonObject);

    @POST("OTP/GenerationAndSaveOTP")
    Call<OTPVerification> OTPVerification(@Body JsonObject jsonObject);

    @POST("Login/GetLogin")
    Call<Login> doLogin(@Body JsonObject jsonObject);

    @POST("User/IsSocialMediaTokenExist")
    Call<SocialLogin> SocialLogin(@Body JsonObject jsonObject);

    @POST("OTP/OTPVerification")
    Call<OTPVerified> OTPVerified(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("OTP/SendOTP")
    Call<SendOTP> SendOTP(@Body JsonObject jsonObject);

    @POST("Team/GetTeamList")
    Call<Team> getTeamData(@Header("Authorization") String token);

    @POST("Team/GetTeamByTeamId")
    Call<Team> GetTeamByTeamId(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("FavouriteTeam/AddUserFavouriteTeam")
    Call<Favourite> addFav(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("FavouriteTeam/DeleteUserFavouriteTeam")
    Call<Favourite> delFav(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("Player/GetPlayerList")
    Call<TeamPictures>GetPlayerList(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("Player/GetPlayersDetails")
    Call<PlayerDetail>GetPlayersDetails(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("Team/GetHighestRuns")
    Call<HighestRuns>GetHighestRuns(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("Team/GetHighestWickets")
    Call<HighestWickets>GetHighestWickets(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("Team/GetPlayerOfSeries")
    Call<PlayerofSeries>GetPlayerOfSeries(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("Fixtures/GetScoreCardByTeam")
    Call<Scorecard>GetScoreCardByTeam(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("Fixtures/GetMediaDetail")
    Call<News>GetMediaDetail_News(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("Fixtures/GetMediaDetail")
    Call<Pictures> GetMediaDetail_Pictures(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("Fixtures/GetMediaDetail")
    Call<Videos>GetMediaDetail_Videos(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("Login/ChangePassword")
    Call<Password>ChangePassword(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("FCM/AddNotification")
    Call<Notification>AddNotification(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("FCM/DeleteNotification")
    Call<Notification>DeleteNotification(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("FCM/GetNotification")
    Call<GetNotification>GetNotification(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("Fixtures/GetNoDays")
    Call<DayList> GetNoDays(@Header("Authorization") String token);

    @POST("Team/GetAllSeason")
    Call<Seasons>GetAllSeason(@Header("Authorization") String token);

    @POST("TicketBooking/GetUpcommingMatches")
    Call<Tickets>GetUpcommingMatches(@Header("Authorization") String token);

    @POST("Auction/GetPlayerAuctionDetail")
    Call<Auction>GetPlayerAuctionDetail(@Header("Authorization") String token);

    @POST("Fixtures/GetScheduleByDayOrTeam")
    Call<Schedule> GetFixtureByDay(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("OTP/IsMobileNumberExist")
    Call<IsMobileNumberExist>IsMobileNumberExist(@Body JsonObject jsonObject);

    @POST("Points/GetPointTableDetail")
    Call<PointTable>GetPointTableDetail(@Header("Authorization") String token);

    @POST("WifiCredential/GetWifiCredential")
    Call<Wifi>GetWifiCredential(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("Fixtures/GetStreamDetail")
    Call<HomeStream>GetStreamDetail(@Header("Authorization") String token,@Body JsonObject jsonObject);

     @POST("FCM/GetReceivedNotification")
     Call<GetReceivedNotification>GetReceivedNotification(@Header("Authorization") String token,@Body JsonObject jsonObject);

    @POST("TicketBooking/GetAdvertisementDetail")
    Call<Advertisement>GetAdvertisementDetail(@Header("Authorization") String token, @Body JsonObject jsonObject);

    @POST("User/ProfileImageUpload")
    Call<ProfilePic>ProfileImageUpload(@Header("Authorization") String token, @Body JsonObject jsonObject);
}
