package com.destek.gt20.Model.GetDays;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("DayMappingId")
    @Expose
    private Integer dayMappingId;
    @SerializedName("Day")
    @Expose
    private String day;
    @SerializedName("MatchDate")
    @Expose
    private String matchDate;

    public Integer getDayMappingId() {
        return dayMappingId;
    }

    public void setDayMappingId(Integer dayMappingId) {
        this.dayMappingId = dayMappingId;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(String matchDate) {
        this.matchDate = matchDate;
    }

}