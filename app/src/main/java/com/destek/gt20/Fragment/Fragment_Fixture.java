package com.destek.gt20.Fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.destek.gt20.Activities.Activity_auction;
import com.destek.gt20.Adapter.MyteamFixture_Adapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Schedule.Datum;
import com.destek.gt20.Model.Schedule.Schedule;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_Fixture extends Fragment
{
    RecyclerView vertical_recycler_view_home;
    private MyteamFixture_Adapter teamfixture_adapter;
    private TextView Txt_MatchDate;
    private List<Datum> scheduleList = new ArrayList<>();
    private static SharedPreferences pref;
    private PreferenceSettings preferenceSettings;
    public  String Teamid,UserId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        View view = inflater.inflate(R.layout.fragment_fixture, container, false);

        vertical_recycler_view_home = view.findViewById(R.id.vertical_recycler_view_home);

        Txt_MatchDate = view.findViewById(R.id.Txt_MatchDate);

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
        Teamid=  preferenceSettings.getTeamId();
        UserId=  preferenceSettings.getUserId();

        if(ProjectConfig.isNetworkConnected(getActivity()))
        {
            prepare_teamfixture_List();
        }
        else
        {
            Toast.makeText(getActivity(),getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }

        return  view;
    }

    private void prepare_teamfixture_List()
    {

        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("DayMappingId","null");
        jsonObject.addProperty("TeamId",Teamid);
        jsonObject.addProperty("Userid",UserId);
        jsonObject.addProperty("Status","Fixture");


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Schedule> call = apiService.GetFixtureByDay("Bearer "+preferenceSettings.getTokenId(),jsonObject);
        call.enqueue(new Callback<Schedule>()
        {
            @Override
            public void onResponse(Call<Schedule>call, Response<Schedule> response)
            {

                String Message = response.body().getMessage();
                System.out.println("##Message"+Message);
                String Description = response.body().getDescription();
                String Error = response.body().getError();


                if(Message.equals("Success"))
                {
                    scheduleList = response.body().getData();

                    for(int i=0;i<scheduleList.size();i++)
                    {
                        String date=scheduleList.get(i).getMatchDate();
                        Txt_MatchDate.setText(date);
                    }

                    teamfixture_adapter = new MyteamFixture_Adapter(scheduleList,getActivity());
                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
                    vertical_recycler_view_home.setLayoutManager(mLayoutManager1);
                    vertical_recycler_view_home.setItemAnimator(new DefaultItemAnimator());
                    vertical_recycler_view_home.setAdapter(teamfixture_adapter);
                }
            }
            @Override
            public void onFailure(Call<Schedule>call, Throwable t) {
                // Log error here since request failed

                System.out.println("##onFailure::"+ t.toString());
            }
        });
    }
}
