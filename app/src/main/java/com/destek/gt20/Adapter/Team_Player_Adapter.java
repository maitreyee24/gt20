package com.destek.gt20.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.destek.gt20.Model.PlayerList.Datum;
import com.destek.gt20.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Team_Player_Adapter  extends RecyclerView.Adapter<Team_Player_Adapter.MyViewHolder>
{
    private List<Datum> teamplayer_list;
    private Context context;
    public Team_Player_Adapter(List<Datum> teamplayer_list, Context context)
    {
        this.teamplayer_list=teamplayer_list;
        this.context=context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.teamplayer_list_row, parent, false);
        return new Team_Player_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position)
    {
        Datum playerofSeries = teamplayer_list.get(position);
        holder.Txt_name.setText(playerofSeries.getName());
        holder.Txt_role.setText(playerofSeries.getRole());


        //Glide.with(context).load(playerofSeries.getPlayerPic()).error(R.mipmap.ic_launcher_round).into(holder.Img_player);

        System.out.println("##playerofSeries.getPlayerPic():"+playerofSeries.getPlayerPic());
        if(playerofSeries.getPlayerPic().equals(""))
        {  Picasso.with(context).load(R.mipmap.ic_launcher_round).resize(6000, 2000)
                .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(holder.Img_player);
        }
        else
        {  Picasso.with(context).load(playerofSeries.getPlayerPic()).resize(6000, 2000)
                .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(holder.Img_player);
        }

        if (position % 2 == 0)
        {
            holder.LL_Master.setBackgroundColor(Color.parseColor("#e6e6ea"));
        }else
        {
            holder.LL_Master.setBackgroundColor(Color.parseColor("#ffffff"));
        }
    }

    @Override
    public int getItemCount() {
        return teamplayer_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView  Txt_name, Txt_role;
        ImageView Img_player;
        public RelativeLayout LL_Master;

        public MyViewHolder(View view) {
            super(view);
            Img_player = view.findViewById(R.id.Img_player);
            Txt_name = view.findViewById(R.id.Txt_name);
            Txt_role = view.findViewById(R.id.Txt_role);
            LL_Master = view.findViewById(R.id.LL_Master);
        }
    }
}
