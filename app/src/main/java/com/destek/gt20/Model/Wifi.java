package com.destek.gt20.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wifi {

    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("Error")
    @Expose
    private String error;
    @SerializedName("Data")
    @Expose
    private String data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}