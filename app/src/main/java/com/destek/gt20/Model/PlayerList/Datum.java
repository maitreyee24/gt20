package com.destek.gt20.Model.PlayerList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("PlayerId")
    @Expose
    private Integer playerId;
    @SerializedName("SeasonId")
    @Expose
    private Integer seasonId;
    @SerializedName("TeamId")
    @Expose
    private Integer teamId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Role")
    @Expose
    private String role;
    @SerializedName("PlayerPic")
    @Expose
    private String playerPic;
    @SerializedName("TeamLogo")
    @Expose
    private String teamLogo;

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(Integer seasonId) {
        this.seasonId = seasonId;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPlayerPic() {
        return playerPic;
    }

    public void setPlayerPic(String playerPic) {
        this.playerPic = playerPic;
    }

    public String getTeamLogo() {
        return teamLogo;
    }

    public void setTeamLogo(String teamLogo) {
        this.teamLogo = teamLogo;
    }

}