package com.destek.gt20.Fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.destek.gt20.Activities.Activity_auction;
import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.Adapter.Videos_Adapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Media.Datum_Videos;
import com.destek.gt20.Model.Media.Videos;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Fragment_Videos extends Fragment
{
    private List<Datum_Videos> Video_List = new ArrayList<>();
    private RecyclerView recyclerView;
    private Videos_Adapter mAdapter;
    private static SharedPreferences pref;
    private PreferenceSettings preferenceSettings;
    public  String Teamid;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        View view = inflater.inflate(R.layout.fragment_videos, container, false);

        recyclerView = view.findViewById(R.id.vertical_recycler_view_home);
        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
        Teamid=  preferenceSettings.getTeamId();

        if(ProjectConfig.isNetworkConnected(getActivity()))
        {
            prepareVideos();
        }
        else
        {
            Toast.makeText(getActivity(),getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }

        return  view;
    }

    private void prepareVideos()
    {


      final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity());
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("MediaType","Video");
        jsonObject.addProperty("FixtureId","null");
        jsonObject.addProperty("TeamId",Teamid);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Videos> call = apiService.GetMediaDetail_Videos("Bearer "+preferenceSettings.getTokenId(),jsonObject);
        call.enqueue(new Callback<Videos>() {
            @Override
            public void onResponse(Call<Videos> call, Response<Videos> response)
            {
               progressDialog.dismiss();

                if(response.body().getMessage().equals("Success"))
                {
                    Video_List = response.body().getData();
                    mAdapter = new Videos_Adapter(Video_List, getActivity());
                    recyclerView.setHasFixedSize(true);
                    RecyclerView.LayoutManager mLayoutManager1 = new GridLayoutManager(getActivity(), 2);
                    recyclerView.setLayoutManager(mLayoutManager1);
                    recyclerView.setAdapter(mAdapter);
                }
            }
            @Override
            public void onFailure(Call<Videos> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}
