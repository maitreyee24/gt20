package com.destek.gt20.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.destek.gt20.Activities.Activity_Home_Result;
import com.destek.gt20.Activities.Activity_Notification;
import com.destek.gt20.Model.Schedule.Datum;
import com.destek.gt20.R;
import com.destek.gt20.VR.CardBoardActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.MyViewHolder>
{

    private List<Datum> scheduleList;
    private Context context;

    public ScheduleAdapter(List<Datum> scheduleList, Context context) {
        this.scheduleList = scheduleList;
        this.context = context;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView Tex_place, Tex_t1, Tex_t2, Tex_status, Texover, Tex_over2,txt_time,txt_team_name,txt_team_name2;
        public LinearLayout LL_Schedule;
        private Context context;
        private ImageView Img_team1, Img_team2;
        private LinearLayout LL,LL_master;

        public MyViewHolder(View view) {
            super(view);
            Tex_place = view.findViewById(R.id.Tex_place);
            Tex_t1 = view.findViewById(R.id.Tex_t1);
            Tex_t2 = view.findViewById(R.id.Tex_t2);
            Texover = view.findViewById(R.id.Texover);
            Tex_over2 = view.findViewById(R.id.Tex_over2);
            txt_team_name = view.findViewById(R.id.txt_team_name);
            txt_team_name2 = view.findViewById(R.id.txt_team_name2);

            txt_time = view.findViewById(R.id.txt_time);
            Tex_status = view.findViewById(R.id.Tex_status);
            LL_Schedule = view.findViewById(R.id.LL_Schedule);
            LL = view.findViewById(R.id.LL);
            LL_master = view.findViewById(R.id.LL_master);
            Img_team1 = view.findViewById(R.id.Img_team1);
            Img_team2 = view.findViewById(R.id.Img_team2);
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.schedule_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position)
    {
        final Datum schedule = scheduleList.get(position);
        holder.Tex_place.setText(schedule.getStadium());
        holder.Tex_t1.setText(schedule.getTeam1Score() + "/" + schedule.getTeam1Wickets());
        holder.Tex_t2.setText(schedule.getTeam2Score() + "/" + schedule.getTeam2Wickets());
        holder.Texover.setText("Overs:" + schedule.getTeam1Overs());
        holder.Tex_over2.setText("Overs:" + schedule.getTeam2Overs());
        holder.Tex_status.setText(schedule.getResult());
        System.out.println("##schedule.getTeam1SN()"+schedule.getTeam1SN());
        holder.txt_team_name.setText(schedule.getTeam1SN());
        holder.txt_team_name2.setText(schedule.getTeam2SN());

        if(schedule.getResult().equals("Reminder"))
        {
            holder.LL.setVisibility(View.VISIBLE);
            holder.Tex_status.setVisibility(View.GONE);

            holder.txt_time.setVisibility(View.VISIBLE);
            holder.LL_master.setVisibility(View.GONE);
            holder.txt_time.setText(schedule.getMatchTime());

            holder.LL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, Activity_Notification.class);
                    i.putExtra("Model", scheduleList.get(position));
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context. startActivity(i);
                }
            });

        }

        System.out.println("##schedule.getTeam1Logo()"+schedule.getTeam1Logo());

      Glide.with(context).load(schedule.getTeam1Logo()).error(R.mipmap.ic_launcher_round).into(holder.Img_team1);
      Glide.with(context).load(schedule.getTeam2Logo()).error(R.mipmap.ic_launcher_round).into(holder.Img_team2);

     /*  Picasso.with(context).load(schedule.getTeam1Logo()).placeholder(R.mipmap.ic_launcher_round).into(holder.Img_team1);
        Picasso.with(context).load(schedule.getTeam2Logo()).placeholder(R.mipmap.ic_launcher_round).into(holder.Img_team2);*/


  /*  Picasso.with(context)
                .load(schedule.getTeam1Logo())
                .fit()
                .centerCrop()
                .error(R.mipmap.ic_launcher_round)
                .into(holder.Img_team1);

        Picasso.with(context)
                .load(schedule.getTeam2Logo())
                .fit()
                .error(R.mipmap.ic_launcher_round)
                .centerCrop()
                .into(holder.Img_team2);
*/


        holder.LL_Schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Activity_Home_Result.class);
                intent.putExtra("Model", scheduleList.get(position));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });



        holder.Tex_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


              /*  PackageManager packageManager = context.getPackageManager();
                boolean gyroExists = packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_GYROSCOPE);
                System.out.println("##gyroExists::"+gyroExists);*/


              //https://youtu.be/_V7mhD9lL_o    GT20 VR video link

              // String uri = "https://s3-eu-west-1.amazonaws.com/dahlia-bucket-hls/hls/demo.m3u8";


                String uri ="https://bitmovin-a.akamaihd.net/content/playhouse-vr/m3u8s/105560.m3u8";    //mona
               // String uri ="https://www.youtube.com/watch?v=cXbtWX26VDw";

               if( getPackageNames())
               {
                   Intent intent = new Intent(context, CardBoardActivity.class);
                   intent.putExtra("uri", uri);
                   context.startActivity(intent);}
               else { Toast.makeText(context,"VR NOT supported",Toast.LENGTH_SHORT).show();}
                

            }
        });
    }

    private boolean getPackageNames()
    {
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> pkgAppsList = context.getPackageManager().queryIntentActivities( mainIntent, 0);


        for (int i=0;i<pkgAppsList.size();i++)
        {
            if(pkgAppsList.get(i).activityInfo.packageName.equals("com.google.samples.apps.cardboarddemo"))
            {
                System.out.println("##true::"+pkgAppsList.get(i).activityInfo.packageName);
                return  true;
            }
            else
            {
                System.out.println("##false::"+pkgAppsList.get(i).activityInfo.packageName);
                return  false;}
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return scheduleList.size();
    }
}