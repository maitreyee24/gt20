package com.destek.gt20.Fragment;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.destek.gt20.R;
import java.util.ArrayList;
import java.util.List;

public class Fragment_scorecard extends Fragment
{

    private RecyclerView recyclerView;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        View view = inflater.inflate(R.layout.fragment_scorecard, container, false);
        recyclerView = view.findViewById(R.id.vertical_recycler_view_home);
        viewPager = view.findViewById(R.id.viewpager);
        viewPager = view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);


        tabLayout = view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setUpTabs(view, "First", "Second");
        return  view;
    }
    private void setupViewPager(ViewPager viewPager)
    {
        Fragment_scorecard.ViewPagerAdapter adapter = new Fragment_scorecard.ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new Fragment_Team1_Scorecard(), "CWI");
        adapter.addFragment(new Fragment_Team2_Scorecard(), "MONSTREAL TIGER");
        viewPager.setAdapter(adapter);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    TextView txt = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.tvtab1);
                    txt.setBackground(getContext().getResources().getDrawable(R.drawable.tab_layout_middle));
                    txt.setTextColor(getContext().getResources().getColor(R.color.white));

                    TextView txt2 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.tvtab2);
                    txt2.setBackground(getContext().getResources().getDrawable(R.drawable.button_layout));
                    txt2.setTextColor(getContext().getResources().getColor(R.color.colorAccent));
                } else {
                    TextView txt = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.tvtab1);
                    txt.setBackground(getContext().getResources().getDrawable(R.drawable.button_layout));
                    txt.setTextColor(getContext().getResources().getColor(R.color.colorAccent));

                    TextView txt2 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.tvtab2);
                    txt2.setBackground(getContext().getResources().getDrawable(R.drawable.tab_layout_middle));
                    txt2.setTextColor(getContext().getResources().getColor(R.color.white));
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setUpTabs(View view, String team1, String team2)
    {

        View headerView = ((LayoutInflater) view.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.custom_tab, null, false);

        LinearLayout linearLayoutOne = headerView.findViewById(R.id.ll);
        LinearLayout linearLayout2 = headerView.findViewById(R.id.ll2);

        tabLayout.getTabAt(0).setCustomView(linearLayoutOne);
        tabLayout.getTabAt(1).setCustomView(linearLayout2);

        TextView txt = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.tvtab1);
        txt.setBackground(getContext().getResources().getDrawable(R.drawable.tab_layout_middle));
        txt.setTextColor(getContext().getResources().getColor(R.color.white));

        TextView txt2 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.tvtab2);
        txt2.setBackground(getContext().getResources().getDrawable(R.drawable.button_layout));
        txt2.setTextColor(getContext().getResources().getColor(R.color.colorAccent));

    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
