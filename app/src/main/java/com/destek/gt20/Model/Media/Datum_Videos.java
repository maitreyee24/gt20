package com.destek.gt20.Model.Media;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum_Videos
{

    @SerializedName("Heading")
    @Expose
    private String heading;
    @SerializedName("ImageName")
    @Expose
    private String imageName;
    @SerializedName("Thumnails")
    @Expose
    private String thumnails;
    @SerializedName("InsDt")
    @Expose
    private String insDt;
    @SerializedName("Time")
    @Expose
    private String time;

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getThumnails() {
        return thumnails;
    }

    public void setThumnails(String thumnails) {
        this.thumnails = thumnails;
    }

    public String getInsDt() {
        return insDt;
    }

    public void setInsDt(String insDt) {
        this.insDt = insDt;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}