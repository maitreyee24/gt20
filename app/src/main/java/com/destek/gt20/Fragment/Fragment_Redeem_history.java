package com.destek.gt20.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.destek.gt20.Adapter.Redeem_Adapter;
import com.destek.gt20.Model.Redeem;
import com.destek.gt20.R;
import java.util.ArrayList;
import java.util.List;


public class Fragment_Redeem_history extends Fragment
{
    private List<Redeem> redeemList = new ArrayList<>();
    private RecyclerView recyclerView;
    private Redeem_Adapter mAdapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        View view = inflater.inflate(R.layout.fragment_redeem_history, container, false);
        recyclerView = view.findViewById(R.id.vertical_recycler_view_home);
        mAdapter = new Redeem_Adapter(redeemList);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager1);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        prepareRedeem();
        return  view;
    }

    private void prepareRedeem()
    {
        Redeem redeem1 = new Redeem("Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s","400");
        redeemList.add(redeem1);
        Redeem redeem2 = new Redeem("Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BCc","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s","500");
        redeemList.add(redeem2);
        Redeem redeem3 = new Redeem("Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500sm","400");
        redeemList.add(redeem3);
        Redeem redeem4 = new Redeem("Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BCc","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s","800");
        redeemList.add(redeem4);
        Redeem redeem5 = new Redeem("Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s","200");
        redeemList.add(redeem5);
        Redeem redeem6 = new Redeem("Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s","400");
        redeemList.add(redeem6);

        mAdapter.notifyDataSetChanged();
    }
}
