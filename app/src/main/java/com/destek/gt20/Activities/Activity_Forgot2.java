package com.destek.gt20.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Password.Password;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_Forgot2  extends AppCompatActivity
{
    Button btn_proceed,back_btn;
    EditText txt_login_Pass;
    String password,mobile;
    private PreferenceSettings preferenceSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password3);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        txt_login_Pass = findViewById(R.id.txt_login_Pass);

        mobile = getIntent().getStringExtra("Mobile");

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();

        txt_login_Pass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 6) {
                    btn_proceed.setBackgroundResource(R.drawable.card_border_fill);
                    btn_proceed.setEnabled(true);
                    btn_proceed.setTextColor(getResources().getColor(R.color.white));
                } else {
                    btn_proceed.setBackgroundResource(R.drawable.card_border);
                    btn_proceed.setEnabled(false);
                    btn_proceed.setTextColor(getResources().getColor(R.color.grey));
                }

            }
        });


        btn_proceed = findViewById(R.id.btn_proceed);
        btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                password=txt_login_Pass.getText().toString();
                if(!password.equals(""))
                {
                        Changepassword();
                }
                else
                 {
                     txt_login_Pass.setError("Please Enter Password");
                 }

                Intent mainIntent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        });


        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void Changepassword()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(Activity_Forgot2.this);
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("Password",password);
        jsonObject.addProperty("Mobile",mobile);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Password> call = apiService.ChangePassword("Bearer "+preferenceSettings.getTokenId(),jsonObject);
        call.enqueue(new Callback<Password>()
        {
            @Override
            public void onResponse(Call<Password>call, Response<Password> response)
            {
                progressDialog.dismiss();


                System.out.println("##response"+response);
                String Message = response.body().getMessage();
                String Description = response.body().getDescription();

                if(Message.equals("Success"))
                {
                    Toast.makeText(getApplicationContext(),Description,Toast.LENGTH_SHORT).show();
                    Intent mainIntent = new Intent(getApplicationContext(),Activity_Login.class);
                    startActivity(mainIntent);
                    finish();
                }
                else {
                    Toast.makeText(getApplicationContext(),Description,Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Password>call, Throwable t) {

                System.out.println("##onFailure"+t.toString());
                progressDialog.dismiss();
            }
        });

    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent mainIntent = new Intent(getApplicationContext(), Activity_Forgot1.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mainIntent);
        finish();
    }
}
