package com.destek.gt20.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Fragment.Fragment_TeamStats;
import com.destek.gt20.Fragment.Fragment_Team_Media;
import com.destek.gt20.Fragment.Fragment_Team_Players;
import com.destek.gt20.Fragment.Fragment_Team_Results;
import com.destek.gt20.Model.Team.Datum;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import java.util.ArrayList;
import java.util.List;

public class Activity_Team  extends AppCompatActivity
{
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ImageView  Img_back;
    TextView txt_team_name;
    private Datum team;
    public String team_logo,team_name,position,coach_name;
    public  int team_id;
    private PreferenceSettings preferenceSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
        txt_team_name = findViewById(R.id.txt_team_name);


        /*viewPager = (ViewPager)findViewById(R.id.viewpager);
        setupViewPager(viewPager);*/

       final MyViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setPagingEnabled(false);
        setupViewPager(viewPager);


        team= (Datum) getIntent().getSerializableExtra("Model");
        team_logo=team.getTeamLogo();
        team_name=team.getTeamName();
        team_id= team.getTeamId();
        coach_name=team.getCoachName();


        txt_team_name.setText(team_name);

        preferenceSettings.setCoachName(coach_name);
        preferenceSettings.setTeamLogo(team_logo);
        preferenceSettings.setTeamName(team_name);
        preferenceSettings.setTeamId(""+team_id);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        Img_back = findViewById(R.id.Img_back);
        Img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("fragmentPosition",1);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }

    private void setupViewPager(ViewPager viewPager)
    {
        Activity_Team.ViewPagerAdapter adapter = new Activity_Team.ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Fragment_Team_Results(), "Results");
        adapter.addFragment(new Fragment_Team_Players(), "Players");
        adapter.addFragment(new Fragment_TeamStats(), "Stats");
        adapter.addFragment(new Fragment_Team_Media(), "Media");
        viewPager.setAdapter(adapter);
    }

    static class ViewPagerAdapter extends FragmentPagerAdapter
    {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
