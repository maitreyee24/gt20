package com.destek.gt20.Model.ReceivedNotification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("TeamName1")
    @Expose
    private String teamName1;
    @SerializedName("TeamName2")
    @Expose
    private String teamName2;
    @SerializedName("MatchDate")
    @Expose
    private String matchDate;
    @SerializedName("MatchTime")
    @Expose
    private String matchTime;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTeamName1() {
        return teamName1;
    }

    public void setTeamName1(String teamName1) {
        this.teamName1 = teamName1;
    }

    public String getTeamName2() {
        return teamName2;
    }

    public void setTeamName2(String teamName2) {
        this.teamName2 = teamName2;
    }

    public String getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(String matchDate) {
        this.matchDate = matchDate;
    }

    public String getMatchTime() {
        return matchTime;
    }

    public void setMatchTime(String matchTime) {
        this.matchTime = matchTime;
    }

}