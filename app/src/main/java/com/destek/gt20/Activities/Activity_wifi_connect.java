package com.destek.gt20.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.destek.gt20.Adapter.CustomWifiAdapter;
import com.destek.gt20.R;

import java.util.ArrayList;
import java.util.List;

public class Activity_wifi_connect extends AppCompatActivity {

    private WifiManager wifiManager;
    private RecyclerView recyclerView;
    private Button buttonScan;
    private int size = 0;
    private List<ScanResult> results;
    private CustomWifiAdapter customWifiAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_connect);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        recyclerView = findViewById(R.id.wifiList);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager1);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        buttonScan = findViewById(R.id.scanBtn);
        buttonScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanWifi();
            }
        });

        if (!wifiManager.isWifiEnabled()) {
            Toast.makeText(this, "WiFi is disabled ... We need to enable it", Toast.LENGTH_LONG).show();
            wifiManager.setWifiEnabled(true);
        } else {
            scanWifi();
        }

        customWifiAdapter = new CustomWifiAdapter(Activity_wifi_connect.this, results);
        recyclerView.setAdapter(customWifiAdapter);

    }


    private void scanWifi() {
//        Log.e("List", results.toString());
        results = new ArrayList<ScanResult>();
        registerReceiver(wifiReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        wifiManager.startScan();
        Toast.makeText(this, "Scanning WiFi ...", Toast.LENGTH_SHORT).show();
    }

    BroadcastReceiver wifiReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            results.addAll(wifiManager.getScanResults());
            unregisterReceiver(this);
            customWifiAdapter.notifyDataSetChanged();
        }
    };

    private void getPassword() {


    }
}
