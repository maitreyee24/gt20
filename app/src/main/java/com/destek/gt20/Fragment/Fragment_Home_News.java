package com.destek.gt20.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.destek.gt20.Activities.Activity_auction;
import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.Adapter.News_Adapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Media.Datum_News;
import com.destek.gt20.Model.Media.News;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public  class Fragment_Home_News extends Fragment
{
    private List<Datum_News> News_List = new ArrayList<>();
    private RecyclerView recyclerView;
    private News_Adapter mAdapter;
    private PreferenceSettings preferenceSettings;
    public  String Fixtureid;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_news, container, false);

        recyclerView = view.findViewById(R.id.vertical_recycler_view_home);

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
        Fixtureid=  preferenceSettings.getFixtureid();

        if(ProjectConfig.isNetworkConnected(getActivity()))
        {
            prepareNews();
        }
        else
        {
            Toast.makeText(getActivity(),getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }


       return  view;
    }

    private void prepareNews()
    {

       final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity());
        progressDialog.show();
        progressDialog.setCancelable(false);

        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("MediaType","News");
        jsonObject.addProperty("FixtureId",Fixtureid);
        jsonObject.addProperty("TeamId","null");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<News> call = apiService.GetMediaDetail_News("Bearer "+preferenceSettings.getTokenId(),jsonObject);
        call.enqueue(new Callback<News>() {
            @Override
            public void onResponse(Call<News> call, Response<News> response)
            {
                 progressDialog.dismiss();

                if(response.body().getMessage().equals("Success"))
                {
                    News_List = response.body().getData();
                    mAdapter = new News_Adapter(News_List,getActivity());
                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(mLayoutManager1);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(mAdapter);

                }
            }
            @Override
            public void onFailure(Call<News> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}
