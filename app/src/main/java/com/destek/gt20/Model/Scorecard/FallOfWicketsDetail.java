package com.destek.gt20.Model.Scorecard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FallOfWicketsDetail {

    @SerializedName("PlayerName")
    @Expose
    private String playerName;
    @SerializedName("Score")
    @Expose
    private String score;
    @SerializedName("Over")
    @Expose
    private Integer over;

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public Integer getOver() {
        return over;
    }

    public void setOver(Integer over) {
        this.over = over;
    }

}