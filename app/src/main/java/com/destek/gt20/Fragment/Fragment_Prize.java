package com.destek.gt20.Fragment;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.destek.gt20.R;
import java.util.ArrayList;
import java.util.List;

public class Fragment_Prize extends Fragment
{
    private RecyclerView recyclerView;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        View view = inflater.inflate(R.layout.fragment_prize, container, false);
        recyclerView = view.findViewById(R.id.vertical_recycler_view_home);
        viewPager = view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setUpTabs(getActivity(), "First", "Second","Three");
        return  view;
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setUpTabs(Context view, String team1, String team2, String team3)
    {

        View headerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.custom_tab1, null, false);

        LinearLayout linearLayoutOne = headerView.findViewById(R.id.ll);
        LinearLayout linearLayout2 = headerView.findViewById(R.id.ll2);
        LinearLayout linearLayout3 = headerView.findViewById(R.id.ll3);

        tabLayout.getTabAt(0).setCustomView(linearLayoutOne);
        tabLayout.getTabAt(1).setCustomView(linearLayout2);
        tabLayout.getTabAt(2).setCustomView(linearLayout3);

        TextView txt = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.tvtab1);
        txt.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_layout_middle));
        txt.setTextColor(getActivity().getResources().getColor(R.color.white));

        TextView txt2 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.tvtab2);
        txt2.setBackground(getActivity().getResources().getDrawable(R.drawable.button_layout));
        txt2.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));

        TextView txt3= tabLayout.getTabAt(2).getCustomView().findViewById(R.id.tvtab3);
        txt3.setBackground(getActivity().getResources().getDrawable(R.drawable.button_layout));
        txt3.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));

    }


    private void setupViewPager(ViewPager viewPager)
    {
        Fragment_Prize.ViewPagerAdapter adapter = new Fragment_Prize.ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new Fragment_MyPrize(), "My Prize");
        adapter.addFragment(new Fragment_Offers(), "Offers");
        adapter.addFragment(new Fragment_Redeem_history(), "Redeem History");
        viewPager.setAdapter(adapter);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    TextView txt = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.tvtab1);
                    txt.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_layout_middle));
                    txt.setTextColor(getActivity().getResources().getColor(R.color.white));

                    TextView txt2 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.tvtab2);
                    txt2.setBackground(getActivity().getResources().getDrawable(R.drawable.button_layout));
                    txt2.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));

                    TextView txt3= tabLayout.getTabAt(2).getCustomView().findViewById(R.id.tvtab3);
                    txt3.setBackground(getActivity().getResources().getDrawable(R.drawable.button_layout));
                    txt3.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                } else if (position == 1) {
                    TextView txt = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.tvtab1);
                    txt.setBackground(getActivity().getResources().getDrawable(R.drawable.button_layout));
                    txt.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));

                    TextView txt2 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.tvtab2);
                    txt2.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_layout_middle));
                    txt2.setTextColor(getActivity().getResources().getColor(R.color.white));

                    TextView txt3 = tabLayout.getTabAt(2).getCustomView().findViewById(R.id.tvtab3);
                    txt3.setBackground(getActivity().getResources().getDrawable(R.drawable.button_layout));
                    txt3.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                } else{
                    TextView txt = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.tvtab1);
                    txt.setBackground(getActivity().getResources().getDrawable(R.drawable.button_layout));
                    txt.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));

                    TextView txt2 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.tvtab2);
                    txt2.setBackground(getActivity().getResources().getDrawable(R.drawable.button_layout));
                    txt2.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));

                    TextView txt3 = tabLayout.getTabAt(2).getCustomView().findViewById(R.id.tvtab3);
                    txt3.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_layout_middle));
                    txt3.setTextColor(getActivity().getResources().getColor(R.color.white));


                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
