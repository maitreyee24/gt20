package com.destek.gt20.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.Adapter.Home_Player_Adapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.PlayerList.Datum;
import com.destek.gt20.Model.PlayerList.TeamPictures;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_Team2 extends Fragment
{
    private Home_Player_Adapter mAdapter;
    private List<Datum> Team_List =null;
    private RecyclerView recyclerView;
    private PreferenceSettings preferenceSettings;
    private  String TeamId;
    public LinearLayout LL_record,LL_norecord;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_team1, container, false);

        recyclerView = view.findViewById(R.id.vertical_recycler_view_home);
        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
        TeamId=  preferenceSettings.getTeam2id();


        LL_record =view. findViewById(R.id.LL_record);
        LL_norecord = view.findViewById(R.id.LL_norecord);

        if(ProjectConfig.isNetworkConnected(getActivity()))
        {
            prepareTeamPlayer();
        }
        else
        {
            Toast.makeText(getActivity(),getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }

        return  view;
    }


    private void prepareTeamPlayer()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity());
        progressDialog.show();
        progressDialog.setCancelable(false);

        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("TeamId",TeamId);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<TeamPictures> call = apiService.GetPlayerList("Bearer "+preferenceSettings.getTokenId(),jsonObject);
        call.enqueue(new Callback<TeamPictures>() {
            @Override
            public void onResponse(Call<TeamPictures> call, Response<TeamPictures> response)
            {
                progressDialog.dismiss();

                if(response.body().getMessage().equals("Success"))
                {
                    Team_List = response.body().getData();
                    if(Team_List.size()==0)
                    {
                        System.out.println("##null");
                        LL_record.setVisibility(View.GONE);
                        LL_norecord.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        LL_record.setVisibility(View.VISIBLE);
                        LL_norecord.setVisibility(View.GONE);
                        mAdapter = new Home_Player_Adapter(Team_List,getActivity());
                        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
                        recyclerView.setLayoutManager(mLayoutManager1);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(mAdapter);
                    }

                 }
            }
            @Override
            public void onFailure(Call<TeamPictures> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

}
