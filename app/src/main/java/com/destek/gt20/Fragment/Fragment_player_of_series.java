package com.destek.gt20.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.destek.gt20.Activities.Activity_auction;
import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.Adapter.PlayerofSeries_Adapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Player_OfSeries.Datum;
import com.destek.gt20.Model.Player_OfSeries.PlayerofSeries;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public  class Fragment_player_of_series extends Fragment
{

    private List<Datum> playerof_series_List = new ArrayList<>();
    private RecyclerView recyclerView;
    private PlayerofSeries_Adapter mAdapter;
    private TextView Txt_name,Txt_team_name,Txt_wickets,Txt_runs;
    private ImageView Img_team_logo,Img_player;
    private PreferenceSettings preferenceSettings;
    public  String Teamid,Seasonid;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_player_of_series, container, false);

        recyclerView = view.findViewById(R.id.vertical_recycler_view_home);
        Txt_name = view.findViewById(R.id.Txt_name);
        Txt_wickets = view.findViewById(R.id.Txt_wickets);
        Txt_runs = view.findViewById(R.id.Txt_runs);
        Txt_team_name = view.findViewById(R.id.Txt_team_name);
        Img_team_logo = view.findViewById(R.id.Img_team_logo);
        Img_player = view.findViewById(R.id.Img_player);


        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
        Teamid=  preferenceSettings.getTeamId();
        Seasonid=  preferenceSettings.getSeasonid();

        if(ProjectConfig.isNetworkConnected(getActivity()))
        {
            prepareHighestWickets();
        }
        else
        {
            Toast.makeText(getActivity(),getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }


        return  view;
    }

    private void prepareHighestWickets()
    {

        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity());
        progressDialog.show();
        progressDialog.setCancelable(false);

        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("SeasonId","1");


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<PlayerofSeries> call = apiService.GetPlayerOfSeries("Bearer "+preferenceSettings.getTokenId(),jsonObject);


        call.enqueue(new Callback<PlayerofSeries>()
        {
            @Override
            public void onResponse(Call<PlayerofSeries>call, Response<PlayerofSeries> response)
            {
                progressDialog.dismiss();
                String Message = response.body().getMessage();
                String Description = response.body().getDescription();
                String Error = response.body().getError();


                if(Message.equals("Success"))
                {
                    playerof_series_List = response.body().getData();

                  /*  Txt_name.setText(playerof_series_List.get(0).getName());
                    Txt_team_name.setText(playerof_series_List.get(0).getShortName());
                    Txt_wickets.setText(""+playerof_series_List.get(0).getHighestWickets());
                    Txt_runs.setText(""+playerof_series_List.get(0).getHighestRuns());

                    Glide.with(getActivity()).load(playerof_series_List.get(0).getTeamLogo()).error(R.mipmap.ic_launcher_round).into(Img_team_logo);
                    Glide.with(getActivity()).load(playerof_series_List.get(0).getPhoto()).error(R.mipmap.ic_launcher_round).into(Img_player);*/


                    mAdapter = new PlayerofSeries_Adapter(playerof_series_List,getActivity());
                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(mLayoutManager1);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(mAdapter);
                }
            }
            @Override
            public void onFailure(Call<PlayerofSeries>call, Throwable t) {
                // Log error here since request failed
                progressDialog.dismiss();
                System.out.println("##onFailure"+ t.toString());
            }
        });
    }
}
