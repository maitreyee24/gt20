package com.destek.gt20.App;


import android.app.Application;
import com.crashlytics.android.Crashlytics;
import com.destek.gt20.Utils.PreferenceSettings;
import io.fabric.sdk.android.Fabric;


public class AppSingleton extends Application
{

    public static final String TAG = AppSingleton.class
            .getSimpleName();
    private static AppSingleton mInstance;
    private PreferenceSettings mPreferenceSettings;


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mInstance = this;
    }

    public AppSingleton() {

    }


    public static synchronized AppSingleton getInstance() {
        if (mInstance == null) {
            mInstance = new AppSingleton();
        }

        return mInstance;
    }

    public PreferenceSettings getPreferenceSettings() {
        if (mPreferenceSettings == null) {
            mPreferenceSettings = new PreferenceSettings(getApplicationContext());
        }
        return mPreferenceSettings;
    }


}


