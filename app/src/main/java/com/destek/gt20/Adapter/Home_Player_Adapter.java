package com.destek.gt20.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.destek.gt20.Model.PlayerList.Datum;
import com.destek.gt20.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Home_Player_Adapter  extends RecyclerView.Adapter<Home_Player_Adapter.MyViewHolder>
{
    private List<Datum> teamplayer_list;
    private Context context;

    public Home_Player_Adapter(List<Datum> teamplayer_list,Context context)
    {
        this.teamplayer_list=teamplayer_list;
        this.context=context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.homeplayer_list_row, parent, false);
        return new Home_Player_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Datum playerofSeries = teamplayer_list.get(position);
        holder.Txt_name.setText(playerofSeries.getName());
        holder.Txt_role.setText(playerofSeries.getRole());

       Glide.with(context).load(playerofSeries.getPlayerPic()).error(R.mipmap.ic_launcher_round).into(holder.Img_player);

      /*  Picasso.with(context).load(playerofSeries.getPlayerPic()).resize(6000, 2000)
                .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(holder.Img_player);*/
    }

    @Override
    public int getItemCount() {
        return teamplayer_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView Txt_name, Txt_role;
        public ImageView Img_player;

        public MyViewHolder(View view) {
            super(view);
            Img_player = view.findViewById(R.id.Img_player);
            Txt_name = view.findViewById(R.id.Txt_name);
            Txt_role = view.findViewById(R.id.Txt_role);
        }
    }
}
