package com.destek.gt20.Model.HighestRuns;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum
{

    @SerializedName("TeamId")
    @Expose
    private Integer teamId;
    @SerializedName("TeamName")
    @Expose
    private String teamName;
    @SerializedName("ShortName")
    @Expose
    private String shortName;
    @SerializedName("TeamLogo")
    @Expose
    private String teamLogo;
    @SerializedName("PlayerId")
    @Expose
    private Integer playerId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Photo")
    @Expose
    private String photo;
    @SerializedName("PlayerStatus")
    @Expose
    private String playerStatus;
    @SerializedName("HighestRuns")
    @Expose
    private int highestRuns;

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
    public String getTeamLogo() {
        return teamLogo;
    }

    public void setTeamLogo(String teamLogo) {
        this.teamLogo = teamLogo;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPlayerStatus() {
        return playerStatus;
    }

    public void setPlayerStatus(String playerStatus) {
        this.playerStatus = playerStatus;
    }

    public int getHighestRuns() {
        return highestRuns;
    }

    public void setHighestRuns(int highestRuns) {
        this.highestRuns = highestRuns;
    }

}