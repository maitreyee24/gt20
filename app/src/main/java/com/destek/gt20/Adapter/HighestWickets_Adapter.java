package com.destek.gt20.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.destek.gt20.Model.HighestWickets.Datum;
import com.destek.gt20.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HighestWickets_Adapter extends RecyclerView.Adapter
{
    private List<Datum> highestWickets_list;
    Context  context;

    public HighestWickets_Adapter(List<Datum> highestWickets_list, Context  context)
    {
        this.highestWickets_list = highestWickets_list;
        this.context=context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType)
    {
        View view;
        switch (viewType) {
            case 0:
                view = LayoutInflater.from(context).inflate(R.layout.highestwickets_players_top_view, viewGroup, false);
                return new TopViewHolder_Highest_Wickets(view);
            case 1:
                view = LayoutInflater.from(context).inflate(R.layout.highestwickets_list_row, viewGroup, false);
                return new OtherViewHolder_Highest_Wickets(view);
        }
        return null;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i)
    {
        if (i == 0)
        {
            ((TopViewHolder_Highest_Wickets) holder).Txt_name.setText(highestWickets_list.get(0).getName());
            ((TopViewHolder_Highest_Wickets) holder).Txt_team_name.setText(highestWickets_list.get(0).getShortName());
            ((TopViewHolder_Highest_Wickets) holder).Txt_wickets.setText("" + highestWickets_list.get(0).getHighestWickets());

            //Glide.with(context).load(highestWickets_list.get(0).getTeamLogo()).error(R.mipmap.ic_launcher_round).into(((TopViewHolder_Highest_Wickets) holder).Img_team_logo);
          //  Glide.with(context).load(highestWickets_list.get(0).getPhoto()).error(R.mipmap.ic_launcher_round).into(((TopViewHolder_Highest_Wickets) holder).Img_player);

            Picasso.with(context).load(highestWickets_list.get(0).getTeamLogo()).resize(6000, 2000)
                    .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(((TopViewHolder_Highest_Wickets) holder).Img_team_logo);
            Picasso.with(context).load(highestWickets_list.get(0).getPhoto()).resize(6000, 2000)
                    .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(((TopViewHolder_Highest_Wickets) holder).Img_player);

        } else
            {
            Datum highestwickets = highestWickets_list.get(i);

            ((OtherViewHolder_Highest_Wickets) holder).Txt_pos.setText("" + Integer.parseInt(String.valueOf(i + 1)) + ".");
            ((OtherViewHolder_Highest_Wickets) holder).Txt_name.setText(highestwickets.getName());
            ((OtherViewHolder_Highest_Wickets) holder).Txt_wickets.setText("" + highestwickets.getHighestWickets());

           // Glide.with(context).load(highestwickets.getPhoto()).error(R.mipmap.ic_launcher_round).into(((OtherViewHolder_Highest_Wickets) holder).Img_Player);
           // Glide.with(context).load(highestwickets.getTeamLogo()).error(R.mipmap.ic_launcher_round).into(((OtherViewHolder_Highest_Wickets) holder).Img_team_logo);

                Picasso.with(context).load(highestwickets.getPhoto()).resize(6000, 2000)
                        .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(((OtherViewHolder_Highest_Wickets) holder).Img_Player);
                Picasso.with(context).load(highestwickets.getTeamLogo()).resize(6000, 2000)
                        .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(((OtherViewHolder_Highest_Wickets) holder).Img_team_logo);
        }
    }

    @Override
    public int getItemCount() {

        return highestWickets_list.size();
    }


    public static class TopViewHolder_Highest_Wickets extends RecyclerView.ViewHolder
    {
         TextView Txt_name, Txt_team_name, Txt_wickets;
         ImageView Img_team_logo, Img_player;

        TopViewHolder_Highest_Wickets(View view) {
            super(view);
            Txt_name = view.findViewById(R.id.Txt_name);
            Txt_wickets = view.findViewById(R.id.Txt_wickets);
            Txt_team_name = view.findViewById(R.id.Txt_team_name);
            Img_team_logo = view.findViewById(R.id.Img_team_logo);
            Img_player = view.findViewById(R.id.Img_player);

        }
    }


    public static class OtherViewHolder_Highest_Wickets extends RecyclerView.ViewHolder
    {
         TextView Txt_pos, Txt_name, Txt_wickets;
         ImageView Img_Player,Img_team_logo;

        OtherViewHolder_Highest_Wickets(View view) {
            super(view);
            Txt_pos = view.findViewById(R.id.Txt_pos);
            Txt_name = view.findViewById(R.id.Txt_name);
            Txt_wickets = view.findViewById(R.id.Txt_wickets);
            Img_Player = view.findViewById(R.id.Img_Player);
            Img_team_logo = view.findViewById(R.id.Img_team_logo);
        }
    }
    @Override
    public int getItemViewType(int position) {

        if (position == 0) {
            return 0;
        }
        return 1;
    }

}
