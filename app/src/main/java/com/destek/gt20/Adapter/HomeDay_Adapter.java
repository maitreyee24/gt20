package com.destek.gt20.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.destek.gt20.Model.GetDays.Datum;
import com.destek.gt20.R;

import java.util.List;

public class HomeDay_Adapter extends RecyclerView.Adapter<HomeDay_Adapter.MyViewHolder> implements View.OnClickListener {
    private List<Datum> homeday_list;
    private Context context;
    private static SharedPreferences pref;
    OnShareClickedListener mCallback;
    public int selectedPosition = 0;

    public HomeDay_Adapter(List<Datum> homeday_list, Context context) {
        this.homeday_list = homeday_list;
        this.context = context;

    }

    @Override
    public void onClick(View v) {

    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView TxtVie_days;
        private LinearLayout master_layout;

        public MyViewHolder(View view) {
            super(view);
            TxtVie_days = view.findViewById(R.id.TxtVie_days);
            master_layout = view.findViewById(R.id.master_layout);

            pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_days, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position)
    {
        final Datum day = homeday_list.get(position);
        holder.TxtVie_days.setText(day.getDay());


        holder.master_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.ShareClicked(String.valueOf(day.getDayMappingId()));
                selectedPosition = position;
                notifyDataSetChanged();

            }
        });

        if (position == selectedPosition)
            holder.master_layout.setBackgroundResource(R.drawable.match_day_selected);
        else
            holder.master_layout.setBackgroundResource(R.drawable.match_day_unselected);
    }

    public interface OnShareClickedListener {
        void ShareClicked(String url);
    }

    public void setOnShareClickedListener(OnShareClickedListener mCallback) {
        this.mCallback = mCallback;
    }


    @Override
    public int getItemCount() {
        return homeday_list.size();
    }
}


