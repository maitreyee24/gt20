package com.destek.gt20.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.destek.gt20.Model.Media.Datum_News;
import com.destek.gt20.R;

public class Activity_News_Detail extends AppCompatActivity {
    private Datum_News datum;
    public ImageView Img_news, Img_back;
    public TextView Txt_news_heading, Txt_news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        Txt_news_heading = findViewById(R.id.Txt_news_heading);
        Txt_news = findViewById(R.id.Txt_news);
        Img_news = findViewById(R.id.Img_news);
        Img_back = findViewById(R.id.Img_back);

        datum = (Datum_News) getIntent().getSerializableExtra("News");

        Txt_news_heading.setText(datum.getHeading());
        Txt_news.setText(datum.getNewsDetail());
        Glide.with(getApplicationContext()).load(datum.getImageName()).error(R.mipmap.ic_launcher_round).into(Img_news);

        Img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent i=new Intent(Activity_News_Detail,Fragment_News);

                onBackPressed();

            }
        });
    }
}
