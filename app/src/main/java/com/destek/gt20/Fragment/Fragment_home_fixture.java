package com.destek.gt20.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.Adapter.HomeDay_Adapter;
import com.destek.gt20.Adapter.ScheduleAdapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.GetDays.DayList;
import com.destek.gt20.Model.Schedule.Datum;
import com.destek.gt20.Model.Schedule.Schedule;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_home_fixture extends Fragment implements HomeDay_Adapter.OnShareClickedListener {

    RecyclerView vertical_recycler_view_home, horizontal_recycler_view_home;
    private HomeDay_Adapter homeday_adapter;
    private List<com.destek.gt20.Model.GetDays.Datum> homeday_list = new ArrayList<>();
    private List<Datum> scheduleList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ScheduleAdapter mAdapter;
    private TextView Txt_MatchDate;
    private PreferenceSettings preferenceSettings;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_home_fixture, container, false);

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
        context = view.getContext();
        Txt_MatchDate = view.findViewById(R.id.Txt_MatchDate);

        horizontal_recycler_view_home = view.findViewById(R.id.horizontal_recycler_view_home);

        homeday_list = new ArrayList<>();
        homeday_adapter = new HomeDay_Adapter(homeday_list, getActivity());
        homeday_adapter.setOnShareClickedListener(this);

        if (ProjectConfig.isNetworkConnected(context))
        {
            Getdays();
            prepareFixtureData("353");
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }

        recyclerView = view.findViewById(R.id.vertical_recycler_view_home);


        return view;
    }


    private void prepareFixtureData(String dayMappingId)
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(context);
        progressDialog.show();
        progressDialog.setCancelable(false);

        System.out.println("##prepareFixtureData"+dayMappingId);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("DayMappingId", dayMappingId);
        jsonObject.addProperty("TeamId", "null");
        jsonObject.addProperty("Userid", "null");
        jsonObject.addProperty("Status", "Fixture");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Schedule> call = apiService.GetFixtureByDay("Bearer " + preferenceSettings.getTokenId(), jsonObject);
        call.enqueue(new Callback<Schedule>() {
            @Override
            public void onResponse(Call<Schedule> call, Response<Schedule> response)
            {

                progressDialog.dismiss();

                String Message = response.body().getMessage();
                System.out.println("##Message" + Message);
                String Description = response.body().getDescription();
                String Error = response.body().getError();


                if (Message.equals("Success")) {
                    scheduleList = response.body().getData();

                    for (int i = 0; i < scheduleList.size(); i++) {
                        String date = scheduleList.get(i).getMatchDate();
                        Txt_MatchDate.setText(date);
                    }

                    ScheduleAdapter mAdapter = new ScheduleAdapter(scheduleList, getActivity());
                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(mLayoutManager1);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<Schedule> call, Throwable t) {
                // Log error here since request failed
                progressDialog.dismiss();
                System.out.println("##onFailure::" + t.toString());
            }
        });
    }


    private void Getdays()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(context);
        progressDialog.show();
        progressDialog.setCancelable(false);



        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<DayList> call = apiService.GetNoDays("Bearer " + preferenceSettings.getTokenId());
        call.enqueue(new Callback<DayList>() {
            @Override
            public void onResponse(Call<DayList> call, Response<DayList> response) {
                progressDialog.dismiss();

                String Message = response.body().getMessage();
                String Description = response.body().getDescription();
                String Error = response.body().getError();


                if (Message.equals("Success")) {
                    homeday_list.addAll(response.body().getData());
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                    horizontal_recycler_view_home.setLayoutManager(layoutManager);
                    horizontal_recycler_view_home.setItemAnimator(new DefaultItemAnimator());
                    horizontal_recycler_view_home.setAdapter(homeday_adapter);
                }
            }

            @Override
            public void onFailure(Call<DayList> call, Throwable t) {
                // Log error here since request failed
                progressDialog.dismiss();
                System.out.println("##onFailure" + t.toString());
            }
        });
    }


    @Override
    public void ShareClicked(String id)
    {
       // Toast.makeText(getContext(), id, Toast.LENGTH_SHORT).show();
        prepareFixtureData(id);
    }
}
