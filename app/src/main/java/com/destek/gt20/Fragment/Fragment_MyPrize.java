package com.destek.gt20.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.destek.gt20.Adapter.Prize_Adapter;
import com.destek.gt20.Model.Prize;
import com.destek.gt20.R;

import java.util.ArrayList;
import java.util.List;


public class Fragment_MyPrize extends Fragment
{
    private List<Prize> PrizeListmList = new ArrayList<>();
    private RecyclerView recyclerView;
    private Prize_Adapter mAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        View view = inflater.inflate(R.layout.fragment_myprize, container, false);
        recyclerView = view.findViewById(R.id.vertical_recycler_view_home);
        mAdapter = new Prize_Adapter(PrizeListmList);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager1);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        preparePrize();
        return  view;
    }

    private void preparePrize()
    {
        Prize prize1 = new Prize("Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s","400");
        PrizeListmList.add(prize1);
        Prize prize2 = new Prize("Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BCc","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s","500");
        PrizeListmList.add(prize2);
        Prize prize3 = new Prize("Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500sm","400");
        PrizeListmList.add(prize3);
        Prize prize4 = new Prize("Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BCc","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s","800");
        PrizeListmList.add(prize4);
        Prize prize5 = new Prize("Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s","200");
        PrizeListmList.add(prize5);
        Prize prize6 = new Prize("Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s","400");
        PrizeListmList.add(prize6);

        mAdapter.notifyDataSetChanged();
    }
}
