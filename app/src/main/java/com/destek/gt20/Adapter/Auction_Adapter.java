package com.destek.gt20.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.destek.gt20.Model.Auction.Datum;
import com.destek.gt20.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Auction_Adapter extends RecyclerView.Adapter<Auction_Adapter.MyViewHolder>
{
    private List<Datum> auctionList;
    Context context;

    public Auction_Adapter(List<Datum> auctionList, Context context) {
        this.auctionList = auctionList;
        this.context = context;
    }

    @NonNull
    @Override
    public Auction_Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.auction_row, parent, false);
        return new Auction_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Auction_Adapter.MyViewHolder holder, int position)
    {
        Datum auction = auctionList.get(position);
        holder.Txt_pos.setText("" + Integer.parseInt(String.valueOf(position+1)) + ".");
        holder.Txt_name.setText(auction.getName());
        holder.Txt_price.setText(auction.getCost());

       // Glide.with(context).load(auction.getPhoto()).error(R.mipmap.ic_launcher_round).into(holder.Img_player);
      //  Glide.with(context).load(auction.getTeamLogo()).error(R.mipmap.ic_launcher_round).into(holder.Img_team_logo);

        if(auction.getPhoto().equals(""))
        {
         Picasso.with(context).load(R.mipmap.ic_launcher_round).resize(6000, 2000)
                    .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(holder.Img_player);
        } else
        {
           Picasso.with(context).load(auction.getPhoto()).resize(6000, 2000)
                    .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(holder.Img_player);
        }

        if(auction.getTeamLogo().equals(""))
        {
            Picasso.with(context).load(R.mipmap.ic_launcher_round).resize(6000, 2000)
                    .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(holder.Img_team_logo);
        }
        else{
            Picasso.with(context).load(auction.getTeamLogo()).resize(6000, 2000)
                    .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(holder.Img_team_logo);
        }
    }

    @Override
    public int getItemCount() {
        return auctionList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView Txt_pos, Txt_name, Txt_price;
        public ImageView Img_player, Img_team_logo;

        public MyViewHolder(View view) {
            super(view);
            Txt_pos = view.findViewById(R.id.Txt_pos);
            Txt_name = view.findViewById(R.id.Txt_name);
            Txt_price = view.findViewById(R.id.Txt_price);
            Img_player = view.findViewById(R.id.Img_player);
            Img_team_logo = view.findViewById(R.id.Img_team_logo);

        }
    }
}
