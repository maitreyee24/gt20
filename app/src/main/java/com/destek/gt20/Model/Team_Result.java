package com.destek.gt20.Model;

public class Team_Result
{
    private String place,over,runs,wickets,status;

    public Team_Result(String place, String over, String runs, String wickets, String status) {
        this.place = place;
        this.over = over;
        this.runs = runs;
        this.wickets = wickets;
        this.status = status;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getOver() {
        return over;
    }

    public void setOver(String over) {
        this.over = over;
    }

    public String getRuns() {
        return runs;
    }

    public void setRuns(String runs) {
        this.runs = runs;
    }

    public String getWickets() {
        return wickets;
    }

    public void setWickets(String wickets) {
        this.wickets = wickets;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
