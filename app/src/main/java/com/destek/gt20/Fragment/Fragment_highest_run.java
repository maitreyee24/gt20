package com.destek.gt20.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.Adapter.HighestRuns_Adapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.HighestRuns.Datum;
import com.destek.gt20.Model.HighestRuns.HighestRuns;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Fragment_highest_run extends Fragment {
    private List<Datum> Highestruns_List;
    private RecyclerView recyclerView;
    private HighestRuns_Adapter mAdapter;
    private TextView Txt_name, txt_team_name, Txt_runs;
    private ImageView img_team_logo, Img_player;
    private PreferenceSettings preferenceSettings;
    public String Teamid, Seasonid;

    public int pageNumber = 1;
    private boolean itShouldLoadMore = true;

    private RecyclerView.LayoutManager layoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_highestrun, container, false);


        Txt_name = view.findViewById(R.id.Txt_name);
        Txt_runs = view.findViewById(R.id.Txt_runs);
        txt_team_name = view.findViewById(R.id.txt_team_name);
        img_team_logo = view.findViewById(R.id.img_team_logo);
        Img_player = view.findViewById(R.id.Img_player);

        Highestruns_List = new ArrayList<>();

        mAdapter = new HighestRuns_Adapter(Highestruns_List, view.getContext());
        mAdapter.notifyDataSetChanged();
        recyclerView = view.findViewById(R.id.vertical_recycler_view_home);
        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
        Teamid = preferenceSettings.getTeamId();
        Seasonid = preferenceSettings.getSeasonid();



        layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        if (ProjectConfig.isNetworkConnected(getActivity())) {
            prepareHighestruns(pageNumber);
//            itShouldLoadMore = true;
//            mAdapter.notifyDataSetChanged();
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    // Recycle view scrolling downwards...
                    // this if statement detects when user reaches the end of recyclerView, this is only time we should load more
                    if (!recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN)) {
                        // remember "!" is the same as "== false"
                        // here we are now allowed to load more, but we need to be careful
                        // we must check if itShouldLoadMore variable is true [unlocked]
                        if (itShouldLoadMore) {
                            pageNumber++;
                            if (ProjectConfig.isNetworkConnected(getActivity())) {
                                prepareHighestruns(pageNumber);
                            } else {
                                Toast.makeText(getActivity(), getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });

        return view;
    }

    private void prepareHighestruns(int page) {

        itShouldLoadMore = false;

        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity());
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("SeasonId", Seasonid);
        jsonObject.addProperty("TeamId", Teamid);
        jsonObject.addProperty("PageNumber", page);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<HighestRuns> call = apiService.GetHighestRuns("Bearer " + preferenceSettings.getTokenId(), jsonObject);
        call.enqueue(new Callback<HighestRuns>() {
            @Override
            public void onResponse(Call<HighestRuns> call, Response<HighestRuns> response) {
                itShouldLoadMore = true;

                progressDialog.dismiss();
                String Message = response.body().getMessage();

                if (Message.equals("Success")) {

                    Highestruns_List.addAll(response.body().getData());

                    mAdapter.notifyDataSetChanged();

                }

            }

            @Override
            public void onFailure(Call<HighestRuns> call, Throwable t) {
                itShouldLoadMore = true;

                // Log error here since request failed
                progressDialog.dismiss();
                System.out.println("##onFailure" + t.toString());
            }
        });
    }
}
