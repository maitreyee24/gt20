package com.destek.gt20.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.destek.gt20.Model.Redeem;
import com.destek.gt20.R;

import java.util.List;

public class Redeem_Adapter extends RecyclerView.Adapter<Redeem_Adapter.MyViewHolder>
{
    private List<Redeem> redeemList;
    public Redeem_Adapter(List<Redeem> redeemList)
    {
        this.redeemList=redeemList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.redeem_list_row, parent, false);
        return new Redeem_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Redeem redeem = redeemList.get(position);
        holder.Txt_title.setText(redeem.getTitle());
        holder.Txt_subtitle.setText(redeem.getSubtitle());
        holder.Tex_cost.setText(redeem.getCost());
    }

    @Override
    public int getItemCount() {
        return redeemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView Txt_title, Txt_subtitle, Tex_cost;

        public MyViewHolder(View view) {
            super(view);
            Txt_title = view.findViewById(R.id.Txt_title);
            Txt_subtitle = view.findViewById(R.id.Txt_subtitle);
            Tex_cost = view.findViewById(R.id.Tex_cost);
        }
    }
}
