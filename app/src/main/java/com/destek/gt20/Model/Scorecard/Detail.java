package com.destek.gt20.Model.Scorecard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detail {

    @SerializedName("PlayerName")
    @Expose
    private String playerName;
    @SerializedName("R")
    @Expose
    private Integer r;
    @SerializedName("B")
    @Expose
    private Integer b;
    @SerializedName("SR")
    @Expose
    private Integer sR;
    @SerializedName("Four")
    @Expose
    private Integer four;
    @SerializedName("Six")
    @Expose
    private Integer six;

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Integer getR() {
        return r;
    }

    public void setR(Integer r) {
        this.r = r;
    }

    public Integer getB() {
        return b;
    }

    public void setB(Integer b) {
        this.b = b;
    }

    public Integer getSR() {
        return sR;
    }

    public void setSR(Integer sR) {
        this.sR = sR;
    }

    public Integer getFour() {
        return four;
    }

    public void setFour(Integer four) {
        this.four = four;
    }

    public Integer getSix() {
        return six;
    }

    public void setSix(Integer six) {
        this.six = six;
    }

}