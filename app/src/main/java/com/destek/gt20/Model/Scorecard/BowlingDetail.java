package com.destek.gt20.Model.Scorecard;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BowlingDetail
{
    @SerializedName("Total")
    @Expose
    private String total;
    @SerializedName("Extras")
    @Expose
    private String extras;
    @SerializedName("TotalExtras")
    @Expose
    private Integer totalExtras;
    @SerializedName("TotalOver")
    @Expose
    private Integer totalOver;
    @SerializedName("ExtrasWickets")
    @Expose
    private Integer extrasWickets;
    @SerializedName("ExtrasLB")
    @Expose
    private Integer extrasLB;
    @SerializedName("Detail")
    @Expose
    private List<Detail_> detail = null;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public Integer getTotalExtras() {
        return totalExtras;
    }

    public void setTotalExtras(Integer totalExtras) {
        this.totalExtras = totalExtras;
    }

    public Integer getTotalOver() {
        return totalOver;
    }

    public void setTotalOver(Integer totalOver) {
        this.totalOver = totalOver;
    }

    public Integer getExtrasWickets() {
        return extrasWickets;
    }

    public void setExtrasWickets(Integer extrasWickets) {
        this.extrasWickets = extrasWickets;
    }

    public Integer getExtrasLB() {
        return extrasLB;
    }

    public void setExtrasLB(Integer extrasLB) {
        this.extrasLB = extrasLB;
    }

    public List<Detail_> getDetail() {
        return detail;
    }

    public void setDetail(List<Detail_> detail) {
        this.detail = detail;
    }

}