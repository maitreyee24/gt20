package com.destek.gt20.Activities;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Fragment.Fragment_Home;
import com.destek.gt20.Fragment.Fragment_Media;
import com.destek.gt20.Fragment.Fragment_MyTeam;
import com.destek.gt20.Fragment.Fragment_Team;
import com.destek.gt20.Fragment.Fragment_Tickets;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
    private android.widget.Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    DrawerLayout drawer;
    boolean doubleBackToExitPressedOnce = false;
    Intent shareIntent;
    ImageView img_user;
    TextView txt_name, txt_email;
    String shareBody = "Follow GT20 at https://desteksolutions.com/";
    private PreferenceSettings preferenceSettings;
    String UserName, Email;
    private final String[] PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    int fragmentPosition=0;
    ViewPagerAdapter adapter;
    private Boolean exit = false;

    private int[] tabIcons = {
            R.drawable.ic_home_border,
            R.drawable.ic_teamunselect,
            R.drawable.ic_plus_unselect,
            R.drawable.ic_video_camera_border_unselect,
            R.drawable.ic_ticket_unselect
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        if (!hasPermissions(this, PERMISSIONS)) {
            int PERMISSION_ALL = 1;
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        UserName = preferenceSettings.getUserName();
        Email = preferenceSettings.getEmail();
        System.out.println("##UserName" + UserName);
        System.out.println("##Email" + Email);


        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.bottom_tabs);
        //viewPager.setPagingEnabled(false);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        setupTabIcons();



        View header = navigationView.getHeaderView(0);
        txt_name = (TextView) header.findViewById(R.id.txt_name);
        txt_email = (TextView) header.findViewById(R.id.txt_email);
        img_user = (ImageView) header.findViewById(R.id.img_user);
        txt_name.setText(UserName);
        txt_email.setText(Email);
        img_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Activity_ProfilePic.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(i,444);
            }
        });



        tabLayout.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {

                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        super.onTabSelected(tab);

                        if (tab.getText().equals("Home")) {
                            tab.setIcon(R.drawable.ic_home_border);
                        } else if (tab.getText().equals("Team")) {
                            tab.setIcon(R.drawable.ic_team_border);
                        } else if (tab.getText().equals("My Team")) {
                            tab.setIcon(R.drawable.ic_plus);
                        } else if (tab.getText().equals("Media")) {
                            tab.setIcon(R.drawable.ic_video_camera_border);
                        } else if (tab.getText().equals("Tickets")) {
                            tab.setIcon(R.drawable.ic_tickets);
                        }
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                        if (tab.getText().equals("Home")) {
                            tab.setIcon(R.drawable.ic_home_unselect);
                        } else if (tab.getText().equals("Team")) {
                            tab.setIcon(R.drawable.ic_teamunselect);
                        } else if (tab.getText().equals("My Team")) {
                            tab.setIcon(R.drawable.ic_plus_unselect);
                        } else if (tab.getText().equals("Media")) {
                            tab.setIcon(R.drawable.ic_video_camera_border_unselect);
                        } else if (tab.getText().equals("Tickets")) {
                            tab.setIcon(R.drawable.ic_ticket_unselect);
                        }
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }
                });

        setupTabIcons();
    }


    @Override
    protected void onResume()
    {
        super.onResume();

        try{
            Intent intent=getIntent();
            fragmentPosition= intent.getExtras().getInt("fragmentPosition");
            System.out.println("##fragmentPosition::"+fragmentPosition);

            viewPager.setCurrentItem(fragmentPosition);
            adapter.notifyDataSetChanged();


        }catch (Exception e)
        {
            fragmentPosition=0;
        }

    }



    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void openDrawer()
    {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.openDrawer(GravityCompat.START);
    }

    private void setupTabIcons()
    {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
        tabLayout.getTabAt(4).setIcon(tabIcons[4]);

    }

    private void setupViewPager(ViewPager viewPager)
    {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Fragment_Home(), "Home");
        adapter.addFragment(new Fragment_Team(), "Team");
        adapter.addFragment(new Fragment_MyTeam(), "My Team");
        adapter.addFragment(new Fragment_Media(), "Media");
        adapter.addFragment(new Fragment_Tickets(), "Tickets");
        viewPager.setAdapter(adapter);
    }


    static class ViewPagerAdapter extends FragmentPagerAdapter
    {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout layout = findViewById(R.id.drawer_layout);
        if (layout.isDrawerOpen(GravityCompat.START)) {
            layout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

        if (exit) {
            finish(); // finish activity

            MainActivity.this.finish();
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(startMain);

        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity__navigation_drawer_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        switch (id) {
            case R.id.My_tickets:
                Intent i1 = new Intent(getApplicationContext(), Activity_My_tickets_list.class);
                i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i1);
                break;

            case R.id.Notifications:
                Intent i2 = new Intent(getApplicationContext(), Activity_Received_Notification.class);
                i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i2);
                break;
            case R.id.Play_win:
                Intent i3 = new Intent(getApplicationContext(), Activity_PlayWin.class);
                i3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i3);
                break;

            case R.id.Myprizes:
                Intent i4 = new Intent(getApplicationContext(), Activity_Myprizes.class);
                i4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i4);
                break;
            case R.id.wifi:
                Intent i5 = new Intent(getApplicationContext(), Activity_wifi.class);
                i5.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i5);
                break;
            case R.id.Auction:
                Intent i6 = new Intent(getApplicationContext(), Activity_auction.class);
                i6.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i6);
                break;
            case R.id.term_condition:
                Intent i7 = new Intent(getApplicationContext(), Activity_Term.class);
                i7.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i7);
                break;
            case R.id.about:
                Intent i8 = new Intent(getApplicationContext(), Activity_About.class);
                i8.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i8);
                break;

            case R.id.logout:
                preferenceSettings.clearPreferences();
                Intent i9 = new Intent(getApplicationContext(), Activity_Login.class);
                i9.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i9);
                finish();
                break;
            case R.id.Sharefriends:
                shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "My App");
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(shareIntent, "share via"));

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 444)
        {
            if(resultCode == 44)
            {

            }
            else
            {

            }
        }
    }
}
