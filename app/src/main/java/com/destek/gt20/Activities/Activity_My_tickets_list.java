package com.destek.gt20.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import com.destek.gt20.Adapter.Ticket_List_Adapter;
import com.destek.gt20.Model.Tickets_List;
import com.destek.gt20.R;

import java.util.ArrayList;
import java.util.List;

public  class Activity_My_tickets_list  extends AppCompatActivity
{
    private List<Tickets_List> tickets_lists = new ArrayList<>();
    private RecyclerView recyclerView;
    private Ticket_List_Adapter mAdapter;
    ImageView Img_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_tickets_list);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        recyclerView = findViewById(R.id.vertical_recycler_view_home);
        mAdapter = new Ticket_List_Adapter(tickets_lists,getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager1);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        prepareTickets();

        Img_back = findViewById(R.id.Img_back);
        Img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }

    private void prepareTickets()
    {
        Tickets_List ticket1= new Tickets_List("Match 1","Tuesday,","24 July,","10.30");
        tickets_lists.add(ticket1);
        Tickets_List ticket2= new Tickets_List("Match 2","Tuesday,","24 July,","10.30");
        tickets_lists.add(ticket2);
        Tickets_List ticket3= new Tickets_List("Match 3","Friday,","24 July,","10.30");
        tickets_lists.add(ticket3);
        Tickets_List ticket4= new Tickets_List("Match 4","Tuesday,","28 July,","11.30");
        tickets_lists.add(ticket4);
        Tickets_List ticket5= new Tickets_List("Match 5","Monday,","24 July,","10.30");
        tickets_lists.add(ticket5);
        Tickets_List ticket6= new Tickets_List("Match 6","Tuesday,","24 July,","10.30");
        tickets_lists.add(ticket6);
        Tickets_List ticket7= new Tickets_List("Match 7","Saturday,","30 July,","08.30");
        tickets_lists.add(ticket7);
        Tickets_List ticket8= new Tickets_List("Match 8","Tuesday,","24 July,","10.30");
        tickets_lists.add(ticket8);



        mAdapter.notifyDataSetChanged();
    }
}
