package com.destek.gt20.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.IsMobileNumberExist;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import com.hbb20.CountryCodePicker;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_Forgot extends AppCompatActivity
{
    Button btn_next,back_btn;
    EditText edx_mob;
    String Mobile;
    public CountryCodePicker countryCodePicker;
    public PreferenceSettings preferenceSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        edx_mob = findViewById(R.id.edx_mob);
        btn_next = findViewById(R.id.btn_next);
        back_btn = findViewById(R.id.back_btn);
        countryCodePicker = (CountryCodePicker) findViewById(R.id.ccp_login);


        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();

        edx_mob.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 10) {
                    btn_next.setBackgroundResource(R.drawable.card_border_fill);
                    btn_next.setEnabled(true);
                    btn_next.setTextColor(getResources().getColor(R.color.white));
                } else {
                    btn_next.setBackgroundResource(R.drawable.card_border);
                    btn_next.setEnabled(false);
                    btn_next.setTextColor(getResources().getColor(R.color.grey));
                }

            }
        });

    btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Mobile= edx_mob.getText().toString();
                if (!Mobile.equals(""))
                {
                    IsMobileNumberExist();
                } else {
                    edx_mob.setError("Please Enter Mobile number !! ");
                }

            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              onBackPressed();
           }
        });

    }

    private void IsMobileNumberExist()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(Activity_Forgot.this);
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("CountryCode", countryCodePicker.getSelectedCountryCode());
        jsonObject.addProperty("Mobile", Mobile);
        jsonObject.addProperty("Email", " ");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<IsMobileNumberExist> call = apiService.IsMobileNumberExist(jsonObject);
        call.enqueue(new Callback<IsMobileNumberExist>() {
            @Override
            public void onResponse(Call<IsMobileNumberExist> call, Response<IsMobileNumberExist> response) {

                progressDialog.dismiss();

                String Message = response.body().getMessage();
                String Description = response.body().getDescription();
                Boolean IsRegistred = response.body().getIsRegistred();


                if (IsRegistred==true)
                {
                    System.out.println("##Mobile number already exist");

                    Intent mainIntent = new Intent(getApplicationContext(), Activity_Forgot1.class);
                    mainIntent.putExtra("Mobile", Mobile);
                    startActivity(mainIntent);
                    finish();
                } else{
                    System.out.println("##Mobile number DOES NOT already exist");
                    Toast.makeText(getApplicationContext(), "Mobile number does not exist.Please Sign-Up!!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<IsMobileNumberExist> call, Throwable t) {
                // Log error here since request failed
                System.out.println("##onFailure" + t.toString());
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent mainIntent = new Intent(getApplicationContext(), Activity_Login.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mainIntent);
        finish();
    }
}
