package com.destek.gt20.Model;

public class MyTeamresults {

    String date,place,runs,wickets,overs,result;

    public MyTeamresults(String date, String place, String runs, String wickets, String overs, String result) {
        this.date = date;
        this.place = place;
        this.runs = runs;
        this.wickets = wickets;
        this.overs = overs;
        this.result = result;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getRuns() {
        return runs;
    }

    public void setRuns(String runs) {
        this.runs = runs;
    }

    public String getWickets() {
        return wickets;
    }

    public void setWickets(String wickets) {
        this.wickets = wickets;
    }

    public String getOvers() {
        return overs;
    }

    public void setOvers(String overs) {
        this.overs = overs;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
