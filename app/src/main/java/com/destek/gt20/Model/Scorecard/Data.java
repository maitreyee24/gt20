package com.destek.gt20.Model.Scorecard;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data
{

    @SerializedName("BattingDetail")
    @Expose
    private List<BattingDetail> battingDetail = null;
    @SerializedName("BowlingDetail")
    @Expose
    private List<BowlingDetail> bowlingDetail = null;
    @SerializedName("FallOfWicketsDetail")
    @Expose
    private List<FallOfWicketsDetail> fallOfWicketsDetail = null;

    public List<BattingDetail> getBattingDetail() {
        return battingDetail;
    }

    public void setBattingDetail(List<BattingDetail> battingDetail) {
        this.battingDetail = battingDetail;
    }

    public List<BowlingDetail> getBowlingDetail() {
        return bowlingDetail;
    }

    public void setBowlingDetail(List<BowlingDetail> bowlingDetail) {
        this.bowlingDetail = bowlingDetail;
    }

    public List<FallOfWicketsDetail> getFallOfWicketsDetail() {
        return fallOfWicketsDetail;
    }

    public void setFallOfWicketsDetail(List<FallOfWicketsDetail> fallOfWicketsDetail) {
        this.fallOfWicketsDetail = fallOfWicketsDetail;
    }

}