package com.destek.gt20.Model.Home_Stream;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("ImageName")
    @Expose
    private String imageName;
    @SerializedName("MatchSummary")
    @Expose
    private String matchSummary;
    @SerializedName("GetStreamCommentDetail")
    @Expose
    private List<GetStreamCommentDetail> getStreamCommentDetail = null;

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getMatchSummary() {
        return matchSummary;
    }

    public void setMatchSummary(String matchSummary) {
        this.matchSummary = matchSummary;
    }

    public List<GetStreamCommentDetail> getGetStreamCommentDetail() {
        return getStreamCommentDetail;
    }

    public void setGetStreamCommentDetail(List<GetStreamCommentDetail> getStreamCommentDetail) {
        this.getStreamCommentDetail = getStreamCommentDetail;
    }

}