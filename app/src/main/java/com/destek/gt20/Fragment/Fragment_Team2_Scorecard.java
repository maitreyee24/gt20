package com.destek.gt20.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.Adapter.Home_Scorecard_batting__Adapter;
import com.destek.gt20.Adapter.Home_Scorecard_bowling__Adapter;
import com.destek.gt20.Adapter.Home_Scorecardfallofwickets;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Scorecard.BattingDetail;
import com.destek.gt20.Model.Scorecard.BowlingDetail;
import com.destek.gt20.Model.Scorecard.Detail;
import com.destek.gt20.Model.Scorecard.Detail_;
import com.destek.gt20.Model.Scorecard.FallOfWicketsDetail;
import com.destek.gt20.Model.Scorecard.Scorecard;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public  class Fragment_Team2_Scorecard extends Fragment
{
    private Home_Scorecard_batting__Adapter mAdapter;
    private Home_Scorecard_bowling__Adapter mbowlingAdapter;
    private Home_Scorecardfallofwickets mfallofwicketsAdapter;

    private List<BowlingDetail> score_team_bowling__List = new ArrayList<>();
    private List<Detail_> bowling_detail = new ArrayList<>();
    private List<BattingDetail> score_team_batting__List = new ArrayList<>();
    private List<Detail> batting_detail = new ArrayList<>();
    private List<FallOfWicketsDetail> score_team_fallofwickets__List = new ArrayList<>();


    private RecyclerView batting_recycler_view_home,bowling_recycler_view_home,fallofwickets_recycler_view_home;

    private PreferenceSettings preferenceSettings;
    public  String Fixtureid,Total,Extra,Total_batting,Extra_batting,Total_main,Extra_Main,Total_main_bowl,Extra_Main_bowl;
    public TextView txt_extra_main,txt_extras,txt_total_main,txt_total,txt_extra_main_batting,txt_extras_batting,txt_total_main_batting,txt_total_batting;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_team2_scoreboard, container, false);

        batting_recycler_view_home = view.findViewById(R.id.batting_recycler_view_home);
        bowling_recycler_view_home = view.findViewById(R.id.bowling_recycler_view_home);
        fallofwickets_recycler_view_home = view.findViewById(R.id.fallofwickets_recycler_view_home);


        txt_extra_main = view.findViewById(R.id.txt_extra_main);
        txt_extras = view.findViewById(R.id.txt_extras);
        txt_total_main = view.findViewById(R.id.txt_total_main);
        txt_total = view.findViewById(R.id.txt_total);


        txt_extra_main_batting = view.findViewById(R.id.txt_extra_main_batting);
        txt_extras_batting = view.findViewById(R.id.txt_extras_batting);
        txt_total_main_batting = view.findViewById(R.id.txt_total_main_batting);
        txt_total_batting = view.findViewById(R.id.txt_total_batting);




        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
        Fixtureid=  preferenceSettings.getFixtureid();

        if(ProjectConfig.isNetworkConnected(getActivity()))
        {
            prepareScorecard();
        }
        else
        {
            Toast.makeText(getActivity(),getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }

        return  view;
    }

    private void prepareScorecard()
    {

        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity());
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("TeamId",2);
        jsonObject.addProperty("FixtureMatchResultId",Fixtureid);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Scorecard> call = apiService.GetScoreCardByTeam("Bearer "+preferenceSettings.getTokenId(),jsonObject);
        call.enqueue(new Callback<Scorecard>() {
            @Override
            public void onResponse(Call<Scorecard> call, Response<Scorecard> response)
            {
                progressDialog.dismiss();

                if(response.body().getMessage().equals("Success"))
                {
                    score_team_bowling__List = response.body().getData().getBowlingDetail();
                    score_team_batting__List = response.body().getData().getBattingDetail();
                    score_team_fallofwickets__List = response.body().getData().getFallOfWicketsDetail();

                    System.out.println("##onResponse score_team1_bowling__List::" + score_team_bowling__List.size());
                    System.out.println("##onResponse score_team_batting__List::" + score_team_batting__List.size());
                    System.out.println("##onResponse score_team_fallofwickets__List::" + score_team_fallofwickets__List.size());

                    for (int i = 0; i < score_team_bowling__List.size(); i++)
                    {
                        Total = score_team_bowling__List.get(i).getTotal();
                        Extra = String.valueOf(score_team_bowling__List.get(i).getTotalExtras());
                        Total_main_bowl= String.valueOf(score_team_batting__List.get(i).getTotal());
                        Extra_Main_bowl= String.valueOf(score_team_batting__List.get(i).getExtras());
                        bowling_detail = score_team_bowling__List.get(i).getDetail();


                    }

                    txt_extra_main.setText(Total_main_bowl);
                    txt_total_main.setText(Extra_Main_bowl);
                    txt_extras.setText(Extra);
                    txt_total.setText(Total);

                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    bowling_recycler_view_home.setLayoutManager(mLayoutManager);
                    // bowling_recycler_view_home.setItemAnimator(new DefaultItemAnimator());
                    mbowlingAdapter = new Home_Scorecard_bowling__Adapter(bowling_detail,getActivity());
                    bowling_recycler_view_home.setAdapter(mbowlingAdapter);


                    for (int i = 0; i < score_team_batting__List.size(); i++) {
                        Total_batting = score_team_batting__List.get(i).getTotal();
                        Extra_batting = String.valueOf(score_team_batting__List.get(i).getTotalExtras());
                        Total_main= String.valueOf(score_team_batting__List.get(i).getTotal());
                        Extra_Main= String.valueOf(score_team_batting__List.get(i).getExtras());
                        batting_detail = score_team_batting__List.get(i).getDetail();


                    }

                    txt_extra_main_batting .setText(Total_main);
                    txt_total_main_batting.setText(Extra_Main);
                    txt_extras_batting.setText(Extra_batting);
                    txt_total_batting.setText(Total_batting);

                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
                    batting_recycler_view_home.setLayoutManager(mLayoutManager1);
                    batting_recycler_view_home.setItemAnimator(new DefaultItemAnimator());
                    mAdapter = new Home_Scorecard_batting__Adapter(batting_detail, getActivity());
                    batting_recycler_view_home.setAdapter(mAdapter);


                    RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(getActivity());
                    fallofwickets_recycler_view_home.setLayoutManager(mLayoutManager2);
                    // fallofwickets_recycler_view_home.setItemAnimator(new DefaultItemAnimator());
                    mfallofwicketsAdapter = new Home_Scorecardfallofwickets(score_team_fallofwickets__List, getActivity());
                    fallofwickets_recycler_view_home.setAdapter(mfallofwicketsAdapter);
                }
            }
            @Override
            public void onFailure(Call<Scorecard> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}
