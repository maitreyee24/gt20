package com.destek.gt20.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IsMobileNumberExist
{

    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("OTP")
    @Expose
    private Object oTP;
    @SerializedName("IsRegistred")
    @Expose
    private Boolean isRegistred;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getOTP() {
        return oTP;
    }

    public void setOTP(Object oTP) {
        this.oTP = oTP;
    }

    public Boolean getIsRegistred() {
        return isRegistred;
    }

    public void setIsRegistred(Boolean isRegistred) {
        this.isRegistred = isRegistred;
    }

}