package com.destek.gt20.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.destek.gt20.Adapter.CustomSpinnerAdapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Player_Detail.PlayerDetail;
import com.destek.gt20.Model.Seasons.Datum;
import com.destek.gt20.Model.Seasons.Seasons;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_Detail_Team extends AppCompatActivity
{
    ImageView Img_back,Img_playerpic;
    String PlayerId,PlayerPic,Name,Role;
    SharedPreferences pref;
    public Spinner spinner;
    private List<Datum> seasonList = new ArrayList<>();
    private String Season,StartDate,EndDate;
    private  int SeasonId;
    private PreferenceSettings preferenceSettings;
    private CustomSpinnerAdapter customSpinnerAdapter;
    TextView match_played,total_runs,highest_runs,total_fours,total_sixes,strike_rate,highest_wickets,maiden,economy,Txt_playername,Txt_playerrole,TxtVie_playername,total_wickets;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indivisual_player_details);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();


        match_played = findViewById(R.id.match_played);
        total_runs = findViewById(R.id.total_runs);
        highest_runs = findViewById(R.id.highest_runs);
        total_fours = findViewById(R.id.total_fours);
        total_sixes = findViewById(R.id.total_sixes);
        strike_rate = findViewById(R.id.strike_rate);
        highest_wickets = findViewById(R.id.highest_wickets);
        maiden = findViewById(R.id.maiden);
        economy = findViewById(R.id.economy);
        total_wickets = findViewById(R.id.total_wickets);

        Img_playerpic = findViewById(R.id.Img_playerpic);
        Txt_playername = findViewById(R.id.Txt_playername);
        Txt_playerrole = findViewById(R.id.Txt_playerrole);
        TxtVie_playername = findViewById(R.id.TxtVie_playername);
        spinner  = findViewById(R.id.spinner);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        PlayerId=  pref.getString("PlayerId",null);
        PlayerPic=  pref.getString("PlayerPic",null);
        Name=  pref.getString("Name",null);
        Role=  pref.getString("Role",null);


        Glide.with(getApplicationContext()).load(PlayerPic).error(R.mipmap.ic_launcher_round).into(Img_playerpic);
        Txt_playername.setText(Name);
        Txt_playerrole.setText(Role);
        TxtVie_playername.setText(Name);


        if(ProjectConfig.isNetworkConnected(Activity_Detail_Team.this))
        {
            GetPlayerDetails();
            GetSeasons();
        }
        else
        {
            Toast.makeText(Activity_Detail_Team.this,getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }



        Img_back = findViewById(R.id.Img_back);
        Img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),Activity_MyTeam.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }

    private void GetSeasons()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(Activity_Detail_Team.this);
        progressDialog.show();
        progressDialog.setCancelable(false);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Seasons> call = apiService.GetAllSeason("Bearer "+preferenceSettings.getTokenId());
        call.enqueue(new Callback<Seasons>()
        {
            @Override
            public void onResponse(Call<Seasons>call, Response<Seasons> response)
            {
                progressDialog.dismiss();
                String Message = response.body().getMessage();
                String Description = response.body().getDescription();
                String Error = response.body().getError();
                ArrayList<String> seasons = new ArrayList<String>();

                if(Message.equals("Success"))
                {
                    seasonList=response.body().getData();
                    for(int i=0;i<seasonList.size();i++)
                    {
                        SeasonId=seasonList.get(i).getSeasonId();
                        Season=seasonList.get(i).getSeason();
                        StartDate=seasonList.get(i).getStartDate();
                        EndDate=seasonList.get(i).getEndDate();


                        customSpinnerAdapter = new CustomSpinnerAdapter(getApplicationContext(), seasonList);
                        spinner.setAdapter(customSpinnerAdapter);
                    }
                }
            }
            @Override
            public void onFailure(Call<Seasons>call, Throwable t) {
                // Log error here since request failed
                progressDialog.dismiss();
                System.out.println("##onFailure"+ t.toString());
            }
        });
    }


    private void GetPlayerDetails()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(Activity_Detail_Team.this);
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject=new JsonObject();
        System.out.println("##PlayerId"+PlayerId);
        jsonObject.addProperty("PlayerId",PlayerId);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<PlayerDetail> call = apiService.GetPlayersDetails("Bearer "+preferenceSettings.getTokenId(),jsonObject);
        call.enqueue(new Callback<PlayerDetail>()
        {
            @Override
            public void onResponse(Call<PlayerDetail>call, Response<PlayerDetail> response)
            {
                progressDialog.dismiss();

            String Message=response.body().getMessage();
           if(Message.equals("Success"))
             {
                String PlayedMatches=response.body().getData().getPlayedMatches();
                String TotalRuns = response.body().getData().getTotalRuns();
                String HighestRuns = response.body().getData().getHighestRuns();
                String Fours = response.body().getData().getFours();
                String Sixes = response.body().getData().getSixes();
                String StrikeRate = response.body().getData().getStrikeRate();
                String HighestWickets = response.body().getData().getHighestWickets();
                String TotalWickets = response.body().getData().getTotalWickets();
                String Economy = response.body().getData().getEconomy();
                String BowlingMatch = response.body().getData().getBowlingMatch();

                match_played.setText(PlayedMatches);
                total_runs.setText(TotalRuns);
                highest_runs.setText(HighestRuns);
                total_fours.setText(Fours);
                total_sixes.setText(Sixes);
                strike_rate.setText(StrikeRate);
                highest_wickets.setText(HighestWickets);
                maiden.setText(BowlingMatch);
                total_wickets.setText(TotalWickets);
                economy.setText(Economy);
              }
            }
            @Override
            public void onFailure(Call<PlayerDetail>call, Throwable t)
            {
                 progressDialog.dismiss();
                 System.out.println("##onFailure"+ t.toString());
            }
        });
    }
}
