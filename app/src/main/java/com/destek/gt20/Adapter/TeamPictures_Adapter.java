package com.destek.gt20.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.destek.gt20.Activities.Activity_Detail_Team;
import com.destek.gt20.Model.PlayerList.Datum;
import com.destek.gt20.R;
import com.squareup.picasso.Picasso;
import java.util.List;

public class TeamPictures_Adapter extends RecyclerView.Adapter<TeamPictures_Adapter.MyViewHolder>
{

    private List<Datum> team_List;
    private Context context;
    private static SharedPreferences pref;

    public TeamPictures_Adapter(List<Datum> team_List, Context context)
    {
        this.team_List=team_List;
        this.context=context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.teamgallery_list_row, parent, false);
        return new TeamPictures_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position)
    {
        final Datum team = team_List.get(position);
        holder.txt_name.setText(team.getName());
        //Glide.with(context).load(team.getPlayerPic()).error(R.mipmap.ic_launcher_round).into(holder.Img_player);

        if(team.getPlayerPic().equals(""))
        {
            Picasso.with(context).load(R.mipmap.ic_launcher_round).resize(6000, 2000)
                .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(holder.Img_player);
        }
        else
        { Picasso.with(context).load(team.getPlayerPic()).resize(6000, 2000)
                .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(holder.Img_player);
        }


        holder.Img_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
                SharedPreferences.Editor edit = pref.edit();
                edit.putString("PlayerId",""+team.getPlayerId());
                edit.putString("PlayerPic",""+team.getPlayerPic());
                edit.putString("Name",""+team.getName());
                edit.putString("Role",""+team.getRole());
                edit.commit();


                Intent intent = new Intent(context, Activity_Detail_Team.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return team_List.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView Img_player;
        public TextView txt_name;
        public MyViewHolder(View view) {
            super(view);
            Img_player = view.findViewById(R.id.Img_player);
            txt_name = view.findViewById(R.id.txt_name);

        }
    }
}
