package com.destek.gt20.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Registration;
import com.destek.gt20.Model.SendOTP;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import com.hbb20.CountryCodePicker;
import java.util.concurrent.TimeUnit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Activity_OTP_Verification  extends AppCompatActivity
{
    Button btn_confirm,btn_resend,back_btn;
    EditText et_otp,et_mobile_num;
    String name,email,mobile,passwrd,otp,countryCode;
    String Message,Description,OTP,regId;
    BroadcastReceiver receiver;
    TextView txt_time;
    private PreferenceSettings preferenceSettings;
    CountryCodePicker ccpOtpVerification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_verification);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();


        et_otp = findViewById(R.id.et_otp);
        et_mobile_num = findViewById(R.id.et_mobile_num);
        ccpOtpVerification = findViewById(R.id.ccp_otp_ver);
        txt_time = findViewById(R.id.txt_time);


        btn_confirm = findViewById(R.id.btn_confirm);

        et_otp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 4) {
                    btn_confirm.setBackgroundResource(R.drawable.card_border_fill);
                    btn_confirm.setEnabled(true);
                    btn_confirm.setTextColor(getResources().getColor(R.color.white));

                    btn_resend.setBackgroundResource(R.drawable.card_border_fill);
                    btn_resend.setEnabled(true);
                    btn_resend.setTextColor(getResources().getColor(R.color.white));

                } else {
                    btn_confirm.setBackgroundResource(R.drawable.card_border);
                    btn_confirm.setEnabled(false);
                    btn_confirm.setTextColor(getResources().getColor(R.color.grey));
                }

            }
        });


        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mainIntent = new Intent(getApplicationContext(), Activity_Signup.class);
                startActivity(mainIntent);
                finish();
            }
        });


        startTimer();

        name = getIntent().getStringExtra("name");
        email = getIntent().getStringExtra("email");
        mobile = getIntent().getStringExtra("number");
        passwrd = getIntent().getStringExtra("passwrd");
        countryCode = getIntent().getStringExtra("country_code");

        preferenceSettings.setUserName(name);
        preferenceSettings.setEmail(email);


        System.out.println("##name"+name);
        System.out.println("##mobile"+mobile);
        System.out.println("##passwrd"+passwrd);
        System.out.println("##countrycode"+countryCode);

        et_mobile_num.setText(mobile);
        ccpOtpVerification.setCountryForPhoneCode(Integer.parseInt(countryCode));

        if(ProjectConfig.isNetworkConnected(Activity_OTP_Verification.this))
        {
            SendOTP();
        }
        else
        {
            Toast.makeText(Activity_OTP_Verification.this,getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                otp=et_otp.getText().toString();
                if(!otp.equals(""))
                {
                    if(ProjectConfig.isNetworkConnected(Activity_OTP_Verification.this))
                    {
                        VerifyOTP();
                    }
                    else
                    {
                        Toast.makeText(Activity_OTP_Verification.this,getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {et_otp.setError("Please Enter Password");}

            }
        });


        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase("otp")) {
                    final String message = intent.getStringExtra("message");

                    String[] s1 = message.split(" ");
                    String []otp = s1[1].split(",");
                    et_otp.setText(otp[0]);
                    //Do whatever you want with the cod e here
                }
            }
        };

        btn_resend = findViewById(R.id.btn_resend);
        btn_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(ProjectConfig.isNetworkConnected(Activity_OTP_Verification.this))
                {
                    SendOTP();
                }
                else
                {
                    Toast.makeText(Activity_OTP_Verification.this,getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void startTimer() {
        new CountDownTimer(90000, 1000) {

            public void onTick(long millisUntilFinished) {
                txt_time.setText("Waiting for an OTP " + String.format("%d : %d ",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                txt_time.setText( "00:00");
              //  btn_resend.setEnabled(true);

                btn_resend.setBackgroundResource(R.drawable.card_border_fill);
                btn_resend.setEnabled(true);
                btn_resend.setTextColor(getResources().getColor(R.color.white));
            }

        }.start();
    }

    private void SendOTP()
    {

        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(Activity_OTP_Verification.this);
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("CountryCode","+"+ccpOtpVerification.getSelectedCountryCode());
        jsonObject.addProperty("Mobile",mobile);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<SendOTP> call = apiService.SendOTP(jsonObject);
        call.enqueue(new Callback<SendOTP>()
        {
            @Override
            public void onResponse(Call<SendOTP>call, Response<SendOTP> response)
            {
                progressDialog.dismiss();

                 Message = response.body().getMessage();
                 Description = response.body().getDescription();
                 OTP = response.body().getOTP();

                System.out.println("##OTP"+OTP);

                if(Message.equals("Success"))
                {
                    Toast.makeText(getApplicationContext(),Description,Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),Description,Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<SendOTP>call, Throwable t) {
                // Log error here since request failed
                progressDialog.dismiss();
                System.out.println("##onFailure"+ t.toString());
            }
        });

    }


    private void VerifyOTP()
    {
        System.out.println("##OTP"+OTP);
        System.out.println("##otp"+otp);
        if(OTP.equals(otp))
        {
            if(ProjectConfig.isNetworkConnected(Activity_OTP_Verification.this))
            {
                Signup();
            }
            else
            {
                Toast.makeText(Activity_OTP_Verification.this,getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
            }
        }
        else { Toast.makeText(getApplicationContext(),"Invalid OTP",Toast.LENGTH_SHORT).show();}

    }

    private void Signup()
    {

        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(Activity_OTP_Verification.this);
        progressDialog.show();
        progressDialog.setCancelable(false);


        SharedPreferences pref = getApplicationContext().getSharedPreferences(ProjectConfig.SHARED_PREF, 0);
        regId = pref.getString("regId", null);
        System.out.println("##regId:"+regId);

        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("Name",name);
        jsonObject.addProperty("EmailId",email);
        jsonObject.addProperty("Mobile",mobile);
        jsonObject.addProperty("CountryCode","+"+ccpOtpVerification.getSelectedCountryCode());
        jsonObject.addProperty("Password",passwrd);
        jsonObject.addProperty("fbtokenId","abc");
        jsonObject.addProperty("GtokenId","efg");
        jsonObject.addProperty("DeviceId",regId);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Registration> call = apiService.doRegistration(jsonObject);
        call.enqueue(new Callback<Registration>()
        {
            @Override
            public void onResponse(Call<Registration>call, Response<Registration> response)
            {

              progressDialog.dismiss();

                String Message = response.body().getMessage();
                String Description = response.body().getDescription();
                String Token = response.body().getToken();

                if(Message.equals("Success"))
                {
                    preferenceSettings.setTokenId(Token);


                    Toast.makeText(getApplicationContext(),Description,Toast.LENGTH_SHORT).show();

                    Intent mainIntent = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(mainIntent);
                    finish();
                }
                else {
                    System.out.println("##Description:"+Description);
                    Toast.makeText(getApplicationContext(),Description,Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Registration>call, Throwable t) {
                // Log error here since request failed

                System.out.println("##onFailure"+ t.toString());
                progressDialog.dismiss();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }
}

