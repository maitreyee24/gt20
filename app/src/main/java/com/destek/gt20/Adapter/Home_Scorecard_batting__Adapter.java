package com.destek.gt20.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.destek.gt20.Model.Scorecard.Detail;
import com.destek.gt20.R;
import java.util.List;

public class Home_Scorecard_batting__Adapter extends RecyclerView.Adapter<Home_Scorecard_batting__Adapter.MyViewHolder>
{
    List<Detail> batting_detail;
    Context context;

    public Home_Scorecard_batting__Adapter(List<Detail> score_team1_batting__list, Context context)
    {
        this.batting_detail = score_team1_batting__list;
        this.context=context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.batting_list_row, parent, false);
        return new Home_Scorecard_batting__Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position)
    {
        Detail detail_batting = batting_detail.get(position);
        holder.Txt_name.setText(detail_batting.getPlayerName());
        holder.Txt_runs.setText("" + detail_batting.getR());
        holder.Txt_boundary.setText("" + detail_batting.getB());
        holder.Txt_sr.setText("" + detail_batting.getSR());
        holder.Txt_four.setText("" + detail_batting.getFour());
        holder.Txt_six.setText("" + detail_batting.getSix());

        if (position % 2 == 0)
        {
            holder.LL_Master.setBackgroundColor(Color.parseColor("#e6e6ea"));
        }else
        {
            holder.LL_Master.setBackgroundColor(Color.parseColor("#ffffff"));
        }
    }

    @Override
    public int getItemCount() {
        return batting_detail.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView Txt_name, Txt_runs, Txt_boundary,Txt_sr,Txt_four,Txt_six;
        public LinearLayout LL_Master;

        public MyViewHolder(View view) {
            super(view);
            Txt_name = view.findViewById(R.id.Txt_name);
            Txt_runs = view.findViewById(R.id.Txt_runs);
            Txt_boundary = view.findViewById(R.id.Txt_boundary);
            Txt_sr = view.findViewById(R.id.Txt_sr);
            Txt_four = view.findViewById(R.id.Txt_four);
            Txt_six = view.findViewById(R.id.Txt_six);
            LL_Master = view.findViewById(R.id.LL_Master);
        }
    }
}
