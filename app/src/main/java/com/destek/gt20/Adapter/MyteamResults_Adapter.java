package com.destek.gt20.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.destek.gt20.Model.MyTeamresults;
import com.destek.gt20.R;

import java.util.List;

public class MyteamResults_Adapter extends RecyclerView.Adapter<MyteamResults_Adapter.MyViewHolder>
{
    private List<MyTeamresults> teamresult_list;
    public MyteamResults_Adapter(List<MyTeamresults> teamresult_list)
    {
        this.teamresult_list = teamresult_list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mytemresults_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position)
    {
        MyTeamresults myTeamresults = teamresult_list.get(position);
        holder.Tex_place.setText(myTeamresults.getPlace());
        holder.Tex_t1.setText(myTeamresults.getRuns()+"/"+myTeamresults.getWickets());
        holder.Tex_t2.setText(myTeamresults.getRuns()+"/"+myTeamresults.getWickets());
        holder.Texover.setText("Overs:"+myTeamresults.getOvers());
        holder.Tex_over2.setText("Overs:"+myTeamresults.getOvers());
        holder.Tex_status.setText(myTeamresults.getResult());

    }

    @Override
    public int getItemCount()
    {
        return teamresult_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView Tex_place,Tex_t1,Tex_t2,Tex_status,Texover,Tex_over2;
        public LinearLayout LL_Schedule;
        private Context context;

        public MyViewHolder(View view)
        {
            super(view);
            Tex_place = view.findViewById(R.id.Tex_place);
            Tex_t1 = view.findViewById(R.id.Tex_t1);
            Tex_t2 = view.findViewById(R.id.Tex_t2);
            Texover = view.findViewById(R.id.Texover);
            Tex_over2 = view.findViewById(R.id.Tex_over2);
            Tex_status = view.findViewById(R.id.Tex_status);
            LL_Schedule = view.findViewById(R.id.LL_Schedule);

        }
    }
}
