package com.destek.gt20.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.destek.gt20.Model.ReceivedNotification.Datum;
import com.destek.gt20.R;
import java.util.List;

public class MyNotification_Adapter extends RecyclerView.Adapter<MyNotification_Adapter.MyViewHolder>
{

    private List<Datum> notificationList;
    Context context;


    public MyNotification_Adapter(List<Datum> notificationList, Context context)
    {
        this.notificationList = notificationList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyNotification_Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mynotification_row, parent, false);
        return new MyNotification_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyNotification_Adapter.MyViewHolder holder, int position)
    {
        Datum mynotification = notificationList.get(position);
        holder.Txt_name.setText(mynotification.getMessage());
        holder.Txt_MatchDate.setText(mynotification.getMatchDate());
        holder.Txt_MatchTime.setText(mynotification.getMatchTime());
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView  Txt_name,Txt_MatchDate,Txt_MatchTime;

        public MyViewHolder(View view) {
            super(view);
            Txt_name = view.findViewById(R.id.Txt_name);
            Txt_MatchDate = view.findViewById(R.id.Txt_MatchDate);
            Txt_MatchTime = view.findViewById(R.id.Txt_MatchTime);

        }
    }
}
