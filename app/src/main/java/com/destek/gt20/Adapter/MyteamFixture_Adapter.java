package com.destek.gt20.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.destek.gt20.Activities.Activity_Notification;
import com.destek.gt20.Model.Schedule.Datum;
import com.destek.gt20.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyteamFixture_Adapter  extends RecyclerView.Adapter<MyteamFixture_Adapter.MyViewHolder>
{
    private List<Datum> scheduleList;
    private Context context;

    public MyteamFixture_Adapter(List<Datum> scheduleList,  Context context)
    {
        this.scheduleList = scheduleList;
        this.context=context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.myteamfixture_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position)
    {
        final Datum schedule = scheduleList.get(position);
        holder.Tex_place.setText(schedule.getStadium());
        holder.Txt_date.setText(schedule.getMatchDate());
        holder.Tex_time.setText(schedule.getMatchTime());

        holder.LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, Activity_Notification.class);
                i.putExtra("Model", scheduleList.get(position));
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context. startActivity(i);
            }
        });

       // Glide.with(context).load(schedule.getTeam1Logo()).error(R.mipmap.ic_launcher_round).into(holder.Img_team1);
       // Glide.with(context).load(schedule.getTeam2Logo()).error(R.mipmap.ic_launcher_round).into(holder.Img_team2);

        Picasso.with(context).load(schedule.getTeam1Logo()).resize(6000, 2000)
                .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(holder.Img_team1);
        Picasso.with(context).load(schedule.getTeam2Logo()).resize(6000, 2000)
                .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(holder.Img_team2);

    }

    @Override
    public int getItemCount() {
        return scheduleList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView Tex_place,Txt_date,Tex_time;
        private Context context;
        private LinearLayout LL;
        public ImageView Img_team1,Img_team2;

        public MyViewHolder(View view)
        {
            super(view);
            Tex_place = view.findViewById(R.id.Tex_place);
            Txt_date = view.findViewById(R.id.Txt_date);
            Tex_time = view.findViewById(R.id.Tex_time);
            Img_team1 = view.findViewById(R.id.Img_team1);
            Img_team2 = view.findViewById(R.id.Img_team2);
            LL = view.findViewById(R.id.LL);
        }
    }

}
