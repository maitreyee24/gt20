package com.destek.gt20.Model.Ticket_Booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Datum  implements Serializable
{

    @SerializedName("MatchDate")
    @Expose
    private String matchDate;
    @SerializedName("MatchTime")
    @Expose
    private String matchTime;
    @SerializedName("Stadium")
    @Expose
    private String stadium;
    @SerializedName("Team1Id")
    @Expose
    private Integer team1Id;
    @SerializedName("Team1Name")
    @Expose
    private String team1Name;
    @SerializedName("Team1SN")
    @Expose
    private String team1SN;
    @SerializedName("Team1Logo")
    @Expose
    private String team1Logo;
    @SerializedName("Team2Id")
    @Expose
    private Integer team2Id;
    @SerializedName("Team2Name")
    @Expose
    private String team2Name;
    @SerializedName("Team2SN")
    @Expose
    private String team2SN;
    @SerializedName("Team2Logo")
    @Expose
    private String team2Logo;
    @SerializedName("Cost")
    @Expose
    private String cost;

    public String getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(String matchDate) {
        this.matchDate = matchDate;
    }

    public String getMatchTime() {
        return matchTime;
    }

    public void setMatchTime(String matchTime) {
        this.matchTime = matchTime;
    }

    public String getStadium() {
        return stadium;
    }

    public void setStadium(String stadium) {
        this.stadium = stadium;
    }

    public Integer getTeam1Id() {
        return team1Id;
    }

    public void setTeam1Id(Integer team1Id) {
        this.team1Id = team1Id;
    }

    public String getTeam1Name() {
        return team1Name;
    }

    public void setTeam1Name(String team1Name) {
        this.team1Name = team1Name;
    }

    public String getTeam1Logo() {
        return team1Logo;
    }

    public void setTeam1Logo(String team1Logo) {
        this.team1Logo = team1Logo;
    }

    public Integer getTeam2Id() {
        return team2Id;
    }

    public void setTeam2Id(Integer team2Id) {
        this.team2Id = team2Id;
    }

    public String getTeam2Name() {
        return team2Name;
    }

    public void setTeam2Name(String team2Name) {
        this.team2Name = team2Name;
    }
    public String getTeam2SN() {
        return team2SN;
    }

    public void setTeam2SN(String team2SN) {
        this.team2SN = team2SN;
    }
    public String getTeam1SN() {
        return team1SN;
    }

    public void setTeam1SN(String team1SN) {
        this.team1SN = team1SN;
    }

    public String getTeam2Logo() {
        return team2Logo;
    }

    public void setTeam2Logo(String team2Logo) {
        this.team2Logo = team2Logo;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

}