package com.destek.gt20.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Fragment.Fragment_Home_Media;
import com.destek.gt20.Fragment.Fragment_Home_Scorecard;
import com.destek.gt20.Fragment.Fragment_Home_Stream;
import com.destek.gt20.Fragment.Fragment_Home_Teams;
import com.destek.gt20.Model.Schedule.Datum;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;

import java.util.ArrayList;
import java.util.List;

public class Activity_Home_Result extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public TextView Tex_place, Tex_t1, Tex_t2, Tex_status, Texover, Tex_over2, txt_team2, txt_team1;
    private ImageView Img_team1, Img_team2, Img_back;
    private Datum datum;
    private PreferenceSettings preferenceSettings;
    public static String team1, team2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_results);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        Tex_place = findViewById(R.id.Tex_place);
        Tex_t1 = findViewById(R.id.Tex_t1);
        Tex_t2 = findViewById(R.id.Tex_t2);
        Texover = findViewById(R.id.Texover);
        Tex_over2 = findViewById(R.id.Tex_over2);
        Tex_status = findViewById(R.id.Tex_status);
        txt_team2 = findViewById(R.id.txt_team2);
        txt_team1 = findViewById(R.id.txt_team1);

        Img_team1 = findViewById(R.id.Img_team1);
        Img_team2 = findViewById(R.id.Img_team2);
        Img_back = findViewById(R.id.Img_back);

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();

        datum = (Datum) getIntent().getSerializableExtra("Model");

       /* viewPager = (ViewPager)findViewById(R.id.viewpager);ss
        setupViewPager(viewPager);*/

        preferenceSettings.setFixtureid("" + datum.getFixtureId());
        preferenceSettings.setTeam1id("" + datum.getTeam1Id());
        preferenceSettings.setTeam2id("" + datum.getTeam2Id());

        final MyViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setPagingEnabled(false);
        setupViewPager(viewPager);


        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        Tex_place.setText(datum.getStadium());
        Tex_t1.setText("" + datum.getTeam1Score().toString() + "/" + datum.getTeam1Wickets().toString());
      //  Tex_t2.setText("" + datum.getTeam2Score().toString() + "/" + datum.getTeam2Wickets().toString());
        Texover.setText("Over:" + datum.getTeam1Overs());
        Tex_over2.setText("Over:" + datum.getTeam2Overs());
        Tex_status.setText(datum.getResult());
        txt_team2.setText(datum.getTeam2SN());
        txt_team1.setText(datum.getTeam1SN());

        Glide.with(getApplicationContext()).load(datum.getTeam1Logo()).error(R.mipmap.ic_launcher_round).into(Img_team1);
        Glide.with(getApplicationContext()).load(datum.getTeam2Logo()).error(R.mipmap.ic_launcher_round).into(Img_team2);

        team1 = datum.getTeam1Name();
        team2 = datum.getTeam2Name();


        Img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("fragmentPosition",0);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        Activity_MyTeam.ViewPagerAdapter adapter = new Activity_MyTeam.ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Fragment_Home_Stream(), "Stream");
        adapter.addFragment(new Fragment_Home_Scorecard(), "Scorecard");
        adapter.addFragment(new Fragment_Home_Teams(), "Teams");
        //adapter.addFragment(new Fragment_Home_Media(), "Media");
        viewPager.setAdapter(adapter);
    }

    static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
