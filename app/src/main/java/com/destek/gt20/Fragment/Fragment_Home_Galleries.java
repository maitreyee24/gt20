package com.destek.gt20.Fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.destek.gt20.Activities.Activity_auction;
import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.Adapter.Gallery_Adapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Media.Datum_Pictures;
import com.destek.gt20.Model.Media.Pictures;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public  class Fragment_Home_Galleries extends Fragment
{
    private List<Datum_Pictures> gallery_List = new ArrayList<>();
    RecyclerView vertical_recycler_view_home;
    private Gallery_Adapter gallery_adapter;
    private static SharedPreferences pref;
    private PreferenceSettings preferenceSettings;
    public  String FixtureId;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        vertical_recycler_view_home = view.findViewById(R.id.vertical_recycler_view_home);

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
        FixtureId=  preferenceSettings.getTeamId();

        if(ProjectConfig.isNetworkConnected(getActivity()))
        {
            prepareGallery();
        }
        else
        {
            Toast.makeText(getActivity(),getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }
       return  view;
    }

    private void prepareGallery()
    {
    final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity());
        progressDialog.show();
        progressDialog.setCancelable(false);

        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("MediaType","Gallary");
        jsonObject.addProperty("FixtureId",FixtureId);
        jsonObject.addProperty("TeamId","null");


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Pictures> call = apiService.GetMediaDetail_Pictures("Bearer "+preferenceSettings.getTokenId(),jsonObject);
        call.enqueue(new Callback<Pictures>() {
            @Override
            public void onResponse(Call<Pictures> call, Response<Pictures> response)
            {
                 progressDialog.dismiss();
                if(response.body().getMessage().equals("Success"))
                {
                    gallery_List = response.body().getData();
                    gallery_adapter = new Gallery_Adapter(gallery_List, getActivity());
                    vertical_recycler_view_home.setHasFixedSize(true);
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
                    vertical_recycler_view_home.setLayoutManager(layoutManager);
                    vertical_recycler_view_home.setAdapter(gallery_adapter);
                }
            }
            @Override
            public void onFailure(Call<Pictures> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}
