package com.destek.gt20.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.destek.gt20.Fragment.Fragment_Popularity;
import com.destek.gt20.Fragment.Fragment_Price;
import com.destek.gt20.Model.Ticket_Booking.Datum;
import com.destek.gt20.R;
import java.util.ArrayList;
import java.util.List;

public class Activity_Bookticket  extends AppCompatActivity
{
    ImageView back_btn;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Datum datum;
    private TextView Txt_toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booktickets);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        viewPager = findViewById(R.id.viewpager);
        Txt_toolbar = findViewById(R.id.Txt_toolbar);

        setupViewPager(viewPager);
        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("fragmentPosition",4);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });


        datum = (Datum) getIntent().getSerializableExtra("Tickets");
        Txt_toolbar.setText(datum.getTeam1Name()+"VS"+datum.getTeam2Name());

    }
    private void setupViewPager(ViewPager viewPager)
    {
        Activity_Bookticket.ViewPagerAdapter adapter = new Activity_Bookticket.ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Fragment_Popularity(), "By Popularity");
        adapter.addFragment(new Fragment_Price(), "By Price");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
