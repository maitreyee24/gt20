package com.destek.gt20.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.destek.gt20.Activities.Activity_Home_Result;
import com.destek.gt20.Activities.Activity_Notification;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Schedule.Datum;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class TeamResult_Adapter extends RecyclerView.Adapter
{
    private List<Datum> scheduleList = new ArrayList<>();
    private Context context;
    private PreferenceSettings preferenceSettings;

    public TeamResult_Adapter(List<Datum> scheduleList, Context context)
    {
        this.scheduleList = scheduleList;
        this.context = context;
        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;
        switch (viewType)
        {
            case 0:
                view = LayoutInflater.from(context).inflate(R.layout.top_view, viewGroup, false);
                return new TopViewHolder(view);
            case 1:
                view = LayoutInflater.from(context).inflate(R.layout.bottom_view, viewGroup, false);
                return new OtherViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int i)
    {
        if (i == 0)
        {
            ((TopViewHolder) holder).Txt_teamname.setText(preferenceSettings.getTeamName());
           // ((TopViewHolder) holder).Txt_position.setText(scheduleList.get(0).get());
            ((TopViewHolder) holder).Txt_coachname.setText(preferenceSettings.getCoachName());


            //Glide.with(context).load(preferenceSettings.getTeamLogo()).error(R.mipmap.ic_launcher_round).into(((TopViewHolder) holder).Img_team_logo);

            Picasso.with(context).load(preferenceSettings.getTeamLogo()).resize(6000, 2000)
                    .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(((TopViewHolder) holder).Img_team_logo);

        }else {
                 final Datum schedule = scheduleList.get(i);
                ((OtherViewHolder) holder).Tex_place.setText(schedule.getStadium());
                ((OtherViewHolder) holder).Tex_t1.setText(schedule.getTeam1Score() + "/" + schedule.getTeam1Wickets());
                ((OtherViewHolder) holder).Tex_t2.setText(schedule.getTeam2Score() + "/" + schedule.getTeam2Wickets());
                ((OtherViewHolder) holder).Texover.setText("Overs:" + schedule.getTeam1Overs());
                ((OtherViewHolder) holder).Tex_over2.setText("Overs:" + schedule.getTeam2Overs());
                ((OtherViewHolder) holder).Tex_status.setText(schedule.getResult());
                ((OtherViewHolder) holder).txt_team_name.setText(schedule.getTeam1SN());
                ((OtherViewHolder) holder).txt_team_name2.setText(schedule.getTeam2SN());


                if(schedule.getResult().equals("Reminder"))
                {
                    ((OtherViewHolder) holder).LL.setVisibility(View.VISIBLE);
                    ((OtherViewHolder) holder).Tex_status.setVisibility(View.GONE);
                    ((OtherViewHolder) holder).txt_time.setVisibility(View.VISIBLE);
                    ((OtherViewHolder) holder).LL_master.setVisibility(View.GONE);
                    ((OtherViewHolder) holder).txt_time.setText(schedule.getMatchTime());


                 ((OtherViewHolder) holder).LL.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v) {
                            Intent intent= new Intent(context, Activity_Notification.class);
                            intent.putExtra("Model", scheduleList.get(i));
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context. startActivity(intent);
                        }
                    });
                }

               // Glide.with(context).load(schedule.getTeam1Logo()).error(R.mipmap.ic_launcher_round).into(((OtherViewHolder) holder).Img_team1);
               // Glide.with(context).load(schedule.getTeam2Logo()).error(R.mipmap.ic_launcher_round).into(((OtherViewHolder) holder).Img_team2);


            Picasso.with(context).load(schedule.getTeam1Logo()).resize(6000, 2000)
                    .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(((OtherViewHolder) holder).Img_team1);
            Picasso.with(context).load(schedule.getTeam2Logo()).resize(6000, 2000)
                    .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(((OtherViewHolder) holder).Img_team2);



                (((OtherViewHolder) holder).LL_Schedule).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, Activity_Home_Result.class);
                        intent.putExtra("Model", scheduleList.get(i));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                    }
                });

          //  Glide.with(context).load(scheduleList.getPhoto()).error(R.mipmap.ic_launcher_round).into(((OtherViewHolder) holder).Img_player);
          //  Glide.with(context).load(scheduleList.getTeamLogo()).error(R.mipmap.ic_launcher_round).into(((OtherViewHolder) holder).Img_team_logo);
        }

    }



    @Override
    public int getItemCount()
    {
        return scheduleList.size();
    }

    public static class TopViewHolder extends RecyclerView.ViewHolder
    {
         TextView Txt_teamname,Txt_position,Txt_coachname,Txt_MatchDate;
         ImageView Img_team_logo;

        TopViewHolder(View view) {
            super(view);
            Txt_teamname = view.findViewById(R.id.Txt_teamname);
            Txt_position = view.findViewById(R.id.Txt_position);
            Txt_coachname = view.findViewById(R.id.Txt_coachname);
            Txt_MatchDate = view.findViewById(R.id.Txt_MatchDate);
            Img_team_logo = view.findViewById(R.id.Img_team_logo);

        }
    }

    public static class OtherViewHolder extends RecyclerView.ViewHolder
    {
        public TextView Tex_place, Tex_t1, Tex_t2, Tex_status, Texover, Tex_over2,txt_time,txt_team_name,txt_team_name2;
        public LinearLayout LL_Schedule;
        private Context context;
        private ImageView Img_team1, Img_team2;
        private LinearLayout LL,LL_master;


        OtherViewHolder(View view) {
            super(view);
            Tex_place = view.findViewById(R.id.Tex_place);
            Tex_t1 = view.findViewById(R.id.Tex_t1);
            Tex_t2 = view.findViewById(R.id.Tex_t2);
            Texover = view.findViewById(R.id.Texover);
            Tex_over2 = view.findViewById(R.id.Tex_over2);
            txt_time = view.findViewById(R.id.txt_time);
            txt_team_name = view.findViewById(R.id.txt_team_name);
            txt_team_name2 = view.findViewById(R.id.txt_team_name2);
            Tex_status = view.findViewById(R.id.Tex_status);
            LL_Schedule = view.findViewById(R.id.LL_Schedule);
            LL = view.findViewById(R.id.LL);
            LL_master = view.findViewById(R.id.LL_master);
            Img_team1 = view.findViewById(R.id.Img_team1);
            Img_team2 = view.findViewById(R.id.Img_team2);
        }
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0) {
            return 0;
        }
        return 1;
    }
}
