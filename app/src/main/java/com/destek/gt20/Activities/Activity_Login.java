package com.destek.gt20.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Login;
import com.destek.gt20.Model.SocialLogin;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonObject;
import com.hbb20.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Activity_Login extends AppCompatActivity
{
    EditText txt_number, txt_login_Pass;
    TextView Txt_Signup, Txt_forgot_pass;
    Button Btn_login_newCard;
    String number, password, regId;
    public BroadcastReceiver mRegistrationBroadcastReceiver;
    public PreferenceSettings preferenceSettings;
    public ImageView img_google_login, img_facebook_login;
    public CountryCodePicker countryCodePicker;
    public static long back_pressed;
    GoogleSignInClient mGoogleSignInClient;
    private final static int RC_SIGN_IN = 123;
    private CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthListner;

    private Context mContext;
    private LoginButton loginButton;
    private Boolean exit = false;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();

        //FirebaseApp.initializeApp(this);
        //regId = FirebaseInstanceId.getInstance().getToken();

        mContext = Activity_Login.this;

        mAuth = FirebaseAuth.getInstance();


        mAuthListner = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    //startActivity(new Intent(singin_activity.this, Home_screen.class));
                }
            }
        };

        loginButton = findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
        // Creating CallbackManager
        callbackManager = CallbackManager.Factory.create();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(ProjectConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(ProjectConfig.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(ProjectConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");
                }
            }
        };

        displayFirebaseRegId();


        txt_number = findViewById(R.id.txt_number);
        txt_login_Pass = findViewById(R.id.txt_login_Pass);
        Btn_login_newCard = findViewById(R.id.Btn_login_newCard);
        Txt_Signup = findViewById(R.id.Txt_Signup);
        Txt_forgot_pass = findViewById(R.id.Txt_forgot_pass);
        countryCodePicker = (CountryCodePicker) findViewById(R.id.ccp_login);



//        txt_login_Pass.setFilters(new InputFilter[]{EMOJI_FILTER});
//        txt_login_Pass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS|InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

          txt_login_Pass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                String sample = s.toString();
                if(sample.contains(" "))
                {
                    sample = sample.replace(" ","");
                    txt_login_Pass.setText(new SpannableStringBuilder(sample));
                    txt_login_Pass.setSelection(txt_login_Pass.length());
                }
            }
        });

        img_google_login = findViewById(R.id.img_google_login);
        img_facebook_login = findViewById(R.id.img_facebook_login);

        callbackManager = CallbackManager.Factory.create();

        img_facebook_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginButton.performClick();
            }
        });

        img_google_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });


        Txt_Signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(getApplicationContext(), Activity_Signup.class);
                startActivity(mainIntent);
                finish();
            }
        });
        Txt_forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(getApplicationContext(), Activity_Forgot.class);
                startActivity(mainIntent);
                finish();
            }
        });

        Btn_login_newCard.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                number = txt_number.getText().toString();
                password = txt_login_Pass.getText().toString();

                if (!number.trim().equals("")) {
                    if (!password.trim().equals("")) {
                        if (number.length() >= 10) {
                            if (ProjectConfig.isNetworkConnected(Activity_Login.this))
                            {
                                Login();
                            } else {
                                Toast.makeText(Activity_Login.this, getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            txt_number.setError("Invalid Mobile Number");
                        }
                    } else {
                        txt_login_Pass.setError("Please enter Password");
                    }
                } else {
                    txt_number.setError("Please enter Number");

                }
            }
        });


        // Registering CallbackManager with the LoginButton
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // Retrieving access token using the LoginResult
                AccessToken accessToken = loginResult.getAccessToken();
                useLoginInformation(accessToken);
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void useLoginInformation(AccessToken accessToken) {
        /**
         Creating the GraphRequest to fetch user details
         1st Param - AccessToken
         2nd Param - Callback (which will be invoked once the request is successful)
         **/
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            //OnCompleted is invoked once the GraphRequest is successful
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    String name = object.getString("name");
                    String email = object.getString("email");
                    String image = object.getJSONObject("picture").getJSONObject("data").getString("url");
                    // txt_fb.setText(object.getString("id"));
                    checkUserByToken(true, object.getString("id"), name, email, regId);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        // We set parameters to the GraphRequest using a Bundle.
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,picture.width(200)");
        request.setParameters(parameters);
        // Initiate the GraphRequest
        request.executeAsync();
    }


    public  static final InputFilter EMOJI_FILTER = new InputFilter()
    {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            boolean keepOriginal = true;
            StringBuilder sb = new StringBuilder(end - start);
            for (int index = start; index < end; index++) {
                int type = Character.getType(source.charAt(index));
                if (type == Character.SURROGATE || type == Character.OTHER_SYMBOL) {
                    return "";
                }
                char c = source.charAt(index);
                if (isCharAllowed(c))
                    sb.append(c);
                else
                    keepOriginal = false;
            }
            if (keepOriginal)
                return null;
            else {
                if (source instanceof Spanned) {
                    SpannableString sp = new SpannableString(sb);
                    TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                    return sp;
                } else {
                    return sb;
                }
            }
        }
    };

    private static boolean isCharAllowed(char c) {
        return Character.isLetterOrDigit(c) || Character.isSpaceChar(c);
    }


    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(ProjectConfig.SHARED_PREF, 0);
        regId = pref.getString("regId","");
        System.out.println("##regId:" + regId);

    }


    private void Login()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(mContext);
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("CountryCode","+"+countryCodePicker.getSelectedCountryCode());
        jsonObject.addProperty("Mobile", number);
        jsonObject.addProperty("Password", password);
        jsonObject.addProperty("DeviceId", regId);


        System.out.println("##number" + number);
        System.out.println("##password" + password);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Login> call = apiService.doLogin(jsonObject);
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {

                progressDialog.dismiss();

                String Message = response.body().getMessage();
                String UserId = response.body().getUserId();
                String UserName = response.body().getUserName();
                String Email = response.body().getEmail();
                String Token = response.body().getToken();


                if (Message.equals("Success"))
                {
                    preferenceSettings.setLoggedIn(true);
                    preferenceSettings.setUserId(UserId);
                    preferenceSettings.setUserName(UserName);
                    preferenceSettings.setEmail(Email);
                    preferenceSettings.setTokenId(Token);

                    System.out.println("##UserId" + UserId);
                    System.out.println("##Email login" + Email);

                    Toast.makeText(getApplicationContext(), "Login Successful!!", Toast.LENGTH_SHORT).show();

                    Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                    mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mainIntent);
                    finish();
                } else{
                    Toast.makeText(getApplicationContext(), "Login Unsuccessful", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                // Log error here since request failed

                System.out.println("##onFailure" + t.toString());
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        if (exit) {
            finish(); // finish activity

            Activity_Login.this.finish();
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(startMain);

        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);

            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w("GT20Tag", "Google sign in failed", e);
                // ...
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        Log.w("GT20Tag", "1");

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("Gt20Tag", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            //updateUI(user);
                            assert user != null;
                            checkUserByToken(false, user.getUid(), user.getDisplayName(), user.getEmail(), regId);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Gt20Tag", "signInWithCredential:failure", task.getException());
                            // Toast.makeText(Login.this, "Unidentified user !!", Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }
                    }
                });
    }


    private void checkUserByToken(final Boolean isFb, String socialUserId, final String name, final String emailId, String deviceId) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(mContext);
        progressDialog.show();
        progressDialog.setCancelable(false);

        JsonObject jsonObject = new JsonObject();
        if (isFb)
            jsonObject.addProperty("fbtokenId", socialUserId);
        else
            jsonObject.addProperty("GtokenId", socialUserId);
        jsonObject.addProperty("DeviceId", deviceId);
        jsonObject.addProperty("Name", name);
        jsonObject.addProperty("EmailId", emailId);


        Call<SocialLogin> call = apiService.SocialLogin(jsonObject);
        call.enqueue(new Callback<SocialLogin>() {
            @Override
            public void onResponse(Call<SocialLogin> call, Response<SocialLogin> response) {

                assert response.body() != null;
                progressDialog.dismiss();

                SocialLogin socialLogin = response.body();
                Log.d("LogGT20", "Change pass response: " + socialLogin);
                if (!socialLogin.getMessage().equals("Success")) {

                    Toast.makeText(Activity_Login.this, socialLogin.getDescription(), Toast.LENGTH_SHORT).show();
                } else {

                    preferenceSettings.setUserId(socialLogin.getUserId());
                    //  preferenceSettings.(false);
                    preferenceSettings.setTokenId(socialLogin.getToken());
                    preferenceSettings.setUserName(name);
                    preferenceSettings.setEmail(emailId);

                    if(isFb)
                    {
                        LoginManager.getInstance().logOut();
                    }
                    else
                    {
                        mGoogleSignInClient.signOut();


                    }



                    Intent loginIntent = new Intent(Activity_Login.this, MainActivity.class);
                    loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loginIntent);
                    finish();
                    //Toast.makeText(mContext, socialLogin.getDescription(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SocialLogin> call, Throwable t) {
                // Log error here since request failed
                Log.e("LogGt20", t.toString());
                progressDialog.dismiss();
                Toast.makeText(Activity_Login.this, "Something went wrong !!", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
