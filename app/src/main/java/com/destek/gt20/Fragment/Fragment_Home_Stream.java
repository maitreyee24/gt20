package com.destek.gt20.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.destek.gt20.Activities.Activity_auction;
import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.Adapter.HomeStream_Adapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.Home_Stream.GetStreamCommentDetail;
import com.destek.gt20.Model.Home_Stream.HomeStream;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_Home_Stream extends Fragment
{

    public ImageView match_video;
    public TextView txt_MatchSummary;
    private RecyclerView recyclerView;
    private HomeStream_Adapter mAdapter;
    public  String video_url,match_summary,Fixtureid;
    public List<GetStreamCommentDetail> commentDetails=new ArrayList<>();
    private PreferenceSettings preferenceSettings;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_home_stream, container, false);

        recyclerView = view.findViewById(R.id.vertical_recycler_view_home);
        match_video = view.findViewById(R.id.match_video);
        txt_MatchSummary =view.findViewById(R.id.txt_MatchSummary);


        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
        Fixtureid=  preferenceSettings.getFixtureid();

        if(ProjectConfig.isNetworkConnected(getActivity()))
        {
            prepareStream();
        }
        else
        {
            Toast.makeText(getActivity(),getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }

        return  view;

    }

    private void prepareStream()
    {

        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity());
        progressDialog.show();
        progressDialog.setCancelable(false);


        JsonObject jsonObject = new JsonObject();
        System.out.println("##Fixtureid"+Fixtureid);
        jsonObject.addProperty("FixtureId", Fixtureid);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<HomeStream> call = apiService.GetStreamDetail("Bearer "+preferenceSettings.getTokenId(),jsonObject);
        call.enqueue(new Callback<HomeStream>() {
            @Override
            public void onResponse(Call<HomeStream> call, Response<HomeStream> response)
            {
                progressDialog.dismiss();
                String Message=response.body().getMessage();
                if(Message.equals("Success"))
                {
                    video_url=response.body().getData().getImageName();
                    match_summary=response.body().getData().getMatchSummary();
                    commentDetails=response.body().getData().getGetStreamCommentDetail();

                    match_video.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            playVideo(video_url);
                        }
                    });

                    txt_MatchSummary.setText(match_summary);

                    mAdapter = new HomeStream_Adapter(commentDetails,getActivity());
                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(mLayoutManager1);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(mAdapter);
                }
            }
            @Override
            public void onFailure(Call<HomeStream> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    public void playVideo(String key)
    {

        Uri uri = Uri.parse(key);
        // With this line the Youtube application, if installed, will launch immediately.
        // Without it you will be prompted with a list of the application to choose.
        uri = Uri.parse("vnd.youtube:" + uri.getQueryParameter("v"));
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}
