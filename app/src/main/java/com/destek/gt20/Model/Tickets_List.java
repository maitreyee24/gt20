package com.destek.gt20.Model;

public class Tickets_List
{
   String match,day,date,time;

    public Tickets_List(String match, String day, String date, String time) {
        this.match = match;
        this.day = day;
        this.date = date;
        this.time = time;
    }

    public String getMatch() {
        return match;
    }

    public void setMatch(String match) {
        this.match = match;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
