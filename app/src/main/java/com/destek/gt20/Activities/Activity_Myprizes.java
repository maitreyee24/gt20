package com.destek.gt20.Activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.destek.gt20.Fragment.Fragment_MyPrize;
import com.destek.gt20.Fragment.Fragment_Offers;
import com.destek.gt20.Fragment.Fragment_Redeem_history;
import com.destek.gt20.R;
import java.util.ArrayList;
import java.util.List;

public class Activity_Myprizes  extends AppCompatActivity
{
    private RecyclerView recyclerView;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ImageView Img_back;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myprizes);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        recyclerView = findViewById(R.id.vertical_recycler_view_home);
        viewPager = findViewById(R.id.viewpager);
        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);


        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setUpTabs(getApplicationContext(), "First", "Second","Three");

        Img_back = findViewById(R.id.Img_back);
        Img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setUpTabs(Context view, String team1, String team2, String team3)
    {

        View headerView = ((LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.custom_tab1, null, false);

        LinearLayout linearLayoutOne = headerView.findViewById(R.id.ll);
        LinearLayout linearLayout2 = headerView.findViewById(R.id.ll2);
        LinearLayout linearLayout3 = headerView.findViewById(R.id.ll3);

        tabLayout.getTabAt(0).setCustomView(linearLayoutOne);
        tabLayout.getTabAt(1).setCustomView(linearLayout2);
        tabLayout.getTabAt(2).setCustomView(linearLayout3);

        TextView txt = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.tvtab1);
        txt.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.tab_layout_middle));
        txt.setTextColor(getApplicationContext().getResources().getColor(R.color.white));

        TextView txt2 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.tvtab2);
        txt2.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.button_layout));
        txt2.setTextColor(getApplicationContext().getResources().getColor(R.color.colorAccent));

        TextView txt3= tabLayout.getTabAt(2).getCustomView().findViewById(R.id.tvtab3);
        txt3.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.button_layout));
        txt3.setTextColor(getApplicationContext().getResources().getColor(R.color.colorAccent));

    }


    private void setupViewPager(ViewPager viewPager)
    {
        Activity_Myprizes.ViewPagerAdapter adapter = new Activity_Myprizes.ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Fragment_MyPrize(), "My Prize");
        adapter.addFragment(new Fragment_Offers(), "Offers");
        adapter.addFragment(new Fragment_Redeem_history(), "Redeem History");
        viewPager.setAdapter(adapter);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    TextView txt = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.tvtab1);
                    txt.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.tab_layout_middle));
                    txt.setTextColor(getApplicationContext().getResources().getColor(R.color.white));

                    TextView txt2 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.tvtab2);
                    txt2.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.button_layout));
                    txt2.setTextColor(getApplicationContext().getResources().getColor(R.color.colorAccent));

                    TextView txt3= tabLayout.getTabAt(2).getCustomView().findViewById(R.id.tvtab3);
                    txt3.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.button_layout));
                    txt3.setTextColor(getApplicationContext().getResources().getColor(R.color.colorAccent));
                } else if (position == 1) {
                    TextView txt = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.tvtab1);
                    txt.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.button_layout));
                    txt.setTextColor(getApplicationContext().getResources().getColor(R.color.colorAccent));

                    TextView txt2 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.tvtab2);
                    txt2.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.tab_layout_middle));
                    txt2.setTextColor(getApplicationContext().getResources().getColor(R.color.white));

                    TextView txt3 = tabLayout.getTabAt(2).getCustomView().findViewById(R.id.tvtab3);
                    txt3.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.button_layout));
                    txt3.setTextColor(getApplicationContext().getResources().getColor(R.color.colorAccent));
                } else{
                    TextView txt = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.tvtab1);
                    txt.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.button_layout));
                    txt.setTextColor(getApplicationContext().getResources().getColor(R.color.colorAccent));

                    TextView txt2 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.tvtab2);
                    txt2.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.button_layout));
                    txt2.setTextColor(getApplicationContext().getResources().getColor(R.color.colorAccent));

                    TextView txt3 = tabLayout.getTabAt(2).getCustomView().findViewById(R.id.tvtab3);
                    txt3.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.tab_layout_middle));
                    txt3.setTextColor(getApplicationContext().getResources().getColor(R.color.white));


                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    class ViewPagerAdapter extends FragmentPagerAdapter
    {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
