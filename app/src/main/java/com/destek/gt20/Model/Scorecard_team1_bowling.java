package com.destek.gt20.Model;

public class Scorecard_team1_bowling
{
    String name,o,r,w,e,d;

    public Scorecard_team1_bowling(String name, String o, String r, String w, String e, String d) {
        this.name = name;
        this.o = o;
        this.r = r;
        this.w = w;
        this.e = e;
        this.d = d;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getO() {
        return o;
    }

    public void setO(String o) {
        this.o = o;
    }

    public String getR() {
        return r;
    }

    public void setR(String r) {
        this.r = r;
    }

    public String getW() {
        return w;
    }

    public void setW(String w) {
        this.w = w;
    }

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }
}
