package com.destek.gt20.Fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.destek.gt20.Activities.TransparentProgressDialog;
import com.destek.gt20.Adapter.Team_Player_Adapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.PlayerList.Datum;
import com.destek.gt20.Model.PlayerList.TeamPictures;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Fragment_Team_Players extends Fragment
{

    private List<Datum> Team_List =null;
    RecyclerView vertical_recycler_view_home;
    private Team_Player_Adapter mAdapter;
    private ImageView Img_player,Img_team_logo;
    private TextView txt_player_name,txt_player_role;
    private static SharedPreferences pref;
    public String team_logo,team_name,position,coach_name;
    public String team_id;
    private PreferenceSettings preferenceSettings;
    public LinearLayout LL_record,LL_norecord;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        View view = inflater.inflate(R.layout.fragment_team_player, container, false);
        vertical_recycler_view_home = view.findViewById(R.id.vertical_recycler_view_home);

        Img_player = view.findViewById(R.id.Img_player);
        Img_team_logo = view.findViewById(R.id.Img_team_logo);
        txt_player_name = view.findViewById(R.id.txt_player_name);
        txt_player_role = view.findViewById(R.id.txt_player_role);



        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();
        team_id=  preferenceSettings.getTeamId();
        System.out.println("##team_id"+team_id);

        LL_record =view. findViewById(R.id.LL_record);
        LL_norecord = view.findViewById(R.id.LL_norecord);


        if(ProjectConfig.isNetworkConnected(getActivity()))
        {
            prepareTeamPlayer();
        }
        else
        {
            Toast.makeText(getActivity(),getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    private void prepareTeamPlayer()
    {

        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity());
        progressDialog.show();
        progressDialog.setCancelable(false);

        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("TeamId",team_id);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<TeamPictures> call = apiService.GetPlayerList("Bearer "+preferenceSettings.getTokenId(),jsonObject);
        call.enqueue(new Callback<TeamPictures>() {
            @Override
            public void onResponse(Call<TeamPictures> call, Response<TeamPictures> response)
            {

                if(response.body().getMessage().equals("Success"))
                {
                    progressDialog.dismiss();

                    Team_List = response.body().getData();

                    if(Team_List.size()==0)
                    {
                        System.out.println("##null");
                        LL_record.setVisibility(View.GONE);
                        LL_norecord.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        LL_record.setVisibility(View.VISIBLE);
                        LL_norecord.setVisibility(View.GONE);

                        txt_player_name.setText(Team_List.get(0).getName());
                        txt_player_role.setText(Team_List.get(0).getRole());

                        //Glide.with(getActivity()).load(Team_List.get(0).getPlayerPic()).error(R.mipmap.ic_launcher_round).into(Img_player);
                        //Glide.with(getActivity()).load(Team_List.get(0).getTeamLogo()).error(R.mipmap.ic_launcher_round).into(Img_team_logo);



                        Picasso.with(getActivity()).load(Team_List.get(0).getPlayerPic()).resize(6000, 2000)
                                .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(Img_player);
                        Picasso.with(getActivity()).load(Team_List.get(0).getTeamLogo()).resize(6000, 2000)
                                .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(Img_team_logo);


                        Team_List.remove(0);

                        mAdapter = new Team_Player_Adapter(Team_List,getActivity());
                        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
                        vertical_recycler_view_home.setLayoutManager(mLayoutManager1);
                        vertical_recycler_view_home.setItemAnimator(new DefaultItemAnimator());
                        vertical_recycler_view_home.setAdapter(mAdapter);
                    }


                }

            }
            @Override
            public void onFailure(Call<TeamPictures> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}
