package com.destek.gt20.Model.Auction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum
{
    @SerializedName("PlayerId")
    @Expose
    private Integer playerId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Photo")
    @Expose
    private String photo;
    @SerializedName("Cost")
    @Expose
    private String cost;
    @SerializedName("TeamShortName")
    @Expose
    private String teamShortName;
    @SerializedName("TeamName")
    @Expose
    private String teamName;
    @SerializedName("TeamLogo")
    @Expose
    private String teamLogo;

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getTeamShortName() {
        return teamShortName;
    }

    public void setTeamShortName(String teamShortName) {
        this.teamShortName = teamShortName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamLogo() {
        return teamLogo;
    }

    public void setTeamLogo(String teamLogo) {
        this.teamLogo = teamLogo;
    }

}