package com.destek.gt20.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.destek.gt20.Model.Home_Stream.GetStreamCommentDetail;
import com.destek.gt20.R;
import java.util.List;

public class HomeStream_Adapter extends RecyclerView.Adapter<HomeStream_Adapter.MyViewHolder>
{
    private List<GetStreamCommentDetail> commentDetails;
    Context context;

    public HomeStream_Adapter(List<GetStreamCommentDetail> commentDetails, Context context)
    {
        this.commentDetails=commentDetails;
        this.context=context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_row, parent, false);
        return new HomeStream_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position)
    {
        GetStreamCommentDetail comment = commentDetails.get(position);
        holder.Txt_name.setText(comment.getUserName());
        holder.Txt_comments.setText(comment.getComments());
        holder.Txt_time.setText(comment.getTime());

    }

    @Override
    public int getItemCount() {
        return commentDetails.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView Txt_name, Txt_comments, Txt_time;

        public MyViewHolder(View view) {
            super(view);
            Txt_name = view.findViewById(R.id.Txt_name);
            Txt_comments = view.findViewById(R.id.Txt_comments);
            Txt_time = view.findViewById(R.id.Txt_time);
        }
    }
}
