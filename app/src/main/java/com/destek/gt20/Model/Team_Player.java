package com.destek.gt20.Model;

public class Team_Player
{
    String url,name,role;

    public Team_Player(String url, String name, String role) {
        this.url = url;
        this.name = name;
        this.role = role;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
