package com.destek.gt20.Adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.destek.gt20.Model.HighestRuns.Datum;
import com.destek.gt20.R;
import com.squareup.picasso.Picasso;
import java.util.List;

public class HighestRuns_Adapter extends RecyclerView.Adapter
{

    private List<Datum> highestruns_list;
    public Context context;

    public HighestRuns_Adapter(List<Datum> highestruns_list, Context context)
    {
        this.highestruns_list = highestruns_list;
        this.context=context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType)
    {

        View view;
        switch (viewType) {
            case 0:
                view = LayoutInflater.from(context).inflate(R.layout.highestruns_top_view, viewGroup, false);
                return new Highestruns_TopViewHolder(view);
            case 1:
                view = LayoutInflater.from(context).inflate(R.layout.highestruns_list_row, viewGroup, false);
                return new Highestruns_OtherViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i)
    {

        if (i == 0)
        {
            ((Highestruns_TopViewHolder) holder).Txt_name.setText(highestruns_list.get(0).getName());
            ((Highestruns_TopViewHolder) holder).txt_team_name.setText(highestruns_list.get(0).getShortName());
            ((Highestruns_TopViewHolder) holder).Txt_runs.setText("" + highestruns_list.get(0).getHighestRuns());

            //Glide.with(context).load(highestruns_list.get(0).getTeamLogo()).error(R.mipmap.ic_launcher_round).into(((Highestruns_TopViewHolder) holder).img_team_logo);
           // Glide.with(context).load(highestruns_list.get(0).getPhoto()).error(R.mipmap.ic_launcher_round).into(((Highestruns_TopViewHolder) holder).Img_player);

            Picasso.with(context).load(highestruns_list.get(0).getTeamLogo()).resize(6000, 2000)
                    .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(((Highestruns_TopViewHolder) holder).img_team_logo);
            Picasso.with(context).load(highestruns_list.get(0).getPhoto()).resize(6000, 2000)
                    .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(((Highestruns_TopViewHolder) holder).Img_player);
        } else
            {
            Datum highestRuns = highestruns_list.get(i);

            ((Highestruns_OtherViewHolder) holder).Txt_pos.setText("" + Integer.parseInt(String.valueOf(i + 1)) + ".");
            ((Highestruns_OtherViewHolder) holder).Txt_name.setText(highestRuns.getName());
            ((Highestruns_OtherViewHolder) holder).Txt_Runs.setText("" + highestRuns.getHighestRuns());

           // Glide.with(context).load(highestRuns.getPhoto()).error(R.mipmap.ic_launcher_round).into(((Highestruns_OtherViewHolder) holder).Img_Player);
           // Glide.with(context).load(highestRuns.getTeamLogo()).error(R.mipmap.ic_launcher_round).into(((Highestruns_OtherViewHolder) holder).Img_team_logo);


                System.out.println("##highestRuns.getPhoto()"+highestRuns.getPhoto());
                Picasso.with(context).load(highestRuns.getPhoto()).resize(6000, 2000)
                        .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(((Highestruns_OtherViewHolder) holder).Img_Player);
                Picasso.with(context).load(highestRuns.getTeamLogo()).resize(6000, 2000)
                        .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(((Highestruns_OtherViewHolder) holder).Img_team_logo);
            }
   }

    @Override
    public int getItemCount()
    {
        return highestruns_list.size();
    }

    public static class Highestruns_TopViewHolder extends RecyclerView.ViewHolder
    {
        private TextView Txt_name, txt_team_name, Txt_runs;
        private ImageView img_team_logo, Img_player;

        Highestruns_TopViewHolder(View view) {
            super(view);
            Txt_name = view.findViewById(R.id.Txt_name);
            Txt_runs = view.findViewById(R.id.Txt_runs);
            txt_team_name = view.findViewById(R.id.txt_team_name);
            img_team_logo = view.findViewById(R.id.img_team_logo);
            Img_player = view.findViewById(R.id.Img_player);

        }
    }

    public static class Highestruns_OtherViewHolder extends RecyclerView.ViewHolder
    {
        TextView Txt_pos, Txt_name,Txt_Runs;
        ImageView Img_Player, Img_team_logo;

        Highestruns_OtherViewHolder(View view) {
            super(view);
            Txt_pos = view.findViewById(R.id.Txt_pos);
            Txt_name = view.findViewById(R.id.Txt_name);
            Txt_Runs = view.findViewById(R.id.Txt_runs);

            Img_Player = view.findViewById(R.id.Img_Player);
            Img_team_logo = view.findViewById(R.id.Img_team_logo);

        }
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0) {
            return 0;
        }
        return 1;
    }
}
