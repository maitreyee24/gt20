package com.destek.gt20.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.destek.gt20.Activities.Activity_My_tickets;
import com.destek.gt20.Model.Tickets_List;
import com.destek.gt20.R;
import java.util.List;

public class Ticket_List_Adapter  extends RecyclerView.Adapter<Ticket_List_Adapter.MyViewHolder>
{
    private List<Tickets_List> tickets_lists;
    Context context;

    public Ticket_List_Adapter(List<Tickets_List> tickets_lists, Context applicationContext)
    {
        this.tickets_lists=tickets_lists;
        this.context=applicationContext;
    }

    @NonNull
    @Override
    public Ticket_List_Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ticket_list_row, parent, false);
        return new Ticket_List_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Ticket_List_Adapter.MyViewHolder holder, int position)
    {
        Tickets_List tickets_list = tickets_lists.get(position);
        holder.TxtVie_match.setText(tickets_list.getMatch());
        holder.TxtVieday.setText(tickets_list.getDay());
        holder.TxtViedate.setText(tickets_list.getDate());
        holder.TxtVietime.setText(tickets_list.getTime());
        holder.LL_tickets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Activity_My_tickets.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tickets_lists.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView TxtVie_match, TxtVieday, TxtViedate,TxtVietime;
        public LinearLayout LL_tickets;

        public MyViewHolder(View view) {
            super(view);
            TxtVie_match = view.findViewById(R.id.TxtVie_match);
            TxtVieday = view.findViewById(R.id.TxtVieday);
            TxtViedate = view.findViewById(R.id.TxtViedate);
            TxtVietime = view.findViewById(R.id.TxtVietime);
            LL_tickets = view.findViewById(R.id.LL_tickets);
        }
    }
}
