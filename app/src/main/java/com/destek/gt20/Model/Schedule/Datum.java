package com.destek.gt20.Model.Schedule;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class Datum implements Serializable
{
    @SerializedName("FixtureId")
    @Expose
    private Integer fixtureId;
    @SerializedName("MatchDate")
    @Expose
    private String matchDate;
    @SerializedName("MatchTime")
    @Expose
    private String matchTime;
    @SerializedName("Stadium")
    @Expose
    private String stadium;
    @SerializedName("Team1Id")
    @Expose
    private Integer team1Id;
    @SerializedName("Team1Name")
    @Expose
    private String team1Name;
    @SerializedName("Team1SN")
    @Expose
    private String team1SN;
    @SerializedName("Team1Logo")
    @Expose
    private String team1Logo;
    @SerializedName("Team1Overs")
    @Expose
    private String team1Overs;
    @SerializedName("Team1Score")
    @Expose
    private Integer team1Score;
    @SerializedName("Team1Wickets")
    @Expose
    private Integer team1Wickets;
    @SerializedName("Team2Id")
    @Expose
    private Integer team2Id;
    @SerializedName("Team2Name")
    @Expose
    private String team2Name;
    @SerializedName("Team2SN")
    @Expose
    private String team2SN;
    @SerializedName("Team2Logo")
    @Expose
    private String team2Logo;
    @SerializedName("Team2Overs")
    @Expose
    private String team2Overs;
    @SerializedName("Team2Score")
    @Expose
    private Integer team2Score;
    @SerializedName("Team2Wickets")
    @Expose
    private Integer team2Wickets;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Result")
    @Expose
    private String result;

    public Integer getFixtureId() {
        return fixtureId;
    }

    public void setFixtureId(Integer fixtureId) {
        this.fixtureId = fixtureId;
    }


    public String getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(String matchDate) {
        this.matchDate = matchDate;
    }

    public String getMatchTime() {
        return matchTime;
    }

    public void setMatchTime(String matchTime) {
        this.matchTime = matchTime;
    }

    public String getStadium() {
        return stadium;
    }

    public void setStadium(String stadium) {
        this.stadium = stadium;
    }

    public Integer getTeam1Id() {
        return team1Id;
    }

    public void setTeam1Id(Integer team1Id) {
        this.team1Id = team1Id;
    }

    public String getTeam1Name() {
        return team1Name;
    }

    public void setTeam1Name(String team1Name) {
        this.team1Name = team1Name;
    }
    public String getTeam2SN() {
        return team2SN;
    }

    public void setTeam2SN(String team2SN) {
        this.team2SN = team2SN;
    }
    public String getTeam1SN() {
        return team1SN;
    }

    public void setTeam1SN(String team1SN) {
        this.team1SN = team1SN;
    }

    public String getTeam1Logo() {
        return team1Logo;
    }

    public void setTeam1Logo(String team1Logo) {
        this.team1Logo = team1Logo;
    }

    public String getTeam1Overs() {
        return team1Overs;
    }

    public void setTeam1Overs(String team1Overs) {
        this.team1Overs = team1Overs;
    }

    public Integer getTeam1Score() {
        return team1Score;
    }

    public void setTeam1Score(Integer team1Score) {
        this.team1Score = team1Score;
    }

    public Integer getTeam1Wickets() {
        return team1Wickets;
    }

    public void setTeam1Wickets(Integer team1Wickets) {
        this.team1Wickets = team1Wickets;
    }

    public Integer getTeam2Id() {
        return team2Id;
    }

    public void setTeam2Id(Integer team2Id) {
        this.team2Id = team2Id;
    }

    public String getTeam2Name() {
        return team2Name;
    }

    public void setTeam2Name(String team2Name) {
        this.team2Name = team2Name;
    }

    public String getTeam2Logo() {
        return team2Logo;
    }

    public void setTeam2Logo(String team2Logo) {
        this.team2Logo = team2Logo;
    }

    public String getTeam2Overs() {
        return team2Overs;
    }

    public void setTeam2Overs(String team2Overs) {
        this.team2Overs = team2Overs;
    }

    public Integer getTeam2Score() {
        return team2Score;
    }

    public void setTeam2Score(Integer team2Score) {
        this.team2Score = team2Score;
    }

    public Integer getTeam2Wickets() {
        return team2Wickets;
    }

    public void setTeam2Wickets(Integer team2Wickets) {
        this.team2Wickets = team2Wickets;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}