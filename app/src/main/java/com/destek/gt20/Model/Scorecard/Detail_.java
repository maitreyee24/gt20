package com.destek.gt20.Model.Scorecard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detail_ {

    @SerializedName("PlayerName")
    @Expose
    private String playerName;
    @SerializedName("O")
    @Expose
    private String o;
    @SerializedName("R")
    @Expose
    private Integer r;
    @SerializedName("W")
    @Expose
    private Integer w;
    @SerializedName("E")
    @Expose
    private Float e;
    @SerializedName("D")
    @Expose
    private Integer d;

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getO() {
        return o;
    }

    public void setO(String o) {
        this.o = o;
    }

    public Integer getR() {
        return r;
    }

    public void setR(Integer r) {
        this.r = r;
    }

    public Integer getW() {
        return w;
    }

    public void setW(Integer w) {
        this.w = w;
    }

    public Float getE() {
        return e;
    }

    public void setE(Float e) {
        this.e = e;
    }

    public Integer getD() {
        return d;
    }

    public void setD(Integer d) {
        this.d = d;
    }

}