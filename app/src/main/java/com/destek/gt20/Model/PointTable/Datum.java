package com.destek.gt20.Model.PointTable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum
{
    @SerializedName("TeamName")
    @Expose
    private String teamName;
    @SerializedName("TeamLogo")
    @Expose
    private String teamLogo;
    @SerializedName("M")
    @Expose
    private Integer m;
    @SerializedName("L")
    @Expose
    private Integer l;
    @SerializedName("W")
    @Expose
    private Integer w;
    @SerializedName("D")
    @Expose
    private Integer d;
    @SerializedName("Points")
    @Expose
    private Integer points;
    @SerializedName("NRR")
    @Expose
    private String nRR;

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamLogo() {
        return teamLogo;
    }

    public void setTeamLogo(String teamLogo) {
        this.teamLogo = teamLogo;
    }

    public Integer getM() {
        return m;
    }

    public void setM(Integer m) {
        this.m = m;
    }

    public Integer getL() {
        return l;
    }

    public void setL(Integer l) {
        this.l = l;
    }

    public Integer getW() {
        return w;
    }

    public void setW(Integer w) {
        this.w = w;
    }

    public Integer getD() {
        return d;
    }

    public void setD(Integer d) {
        this.d = d;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getNRR() {
        return nRR;
    }

    public void setNRR(String nRR) {
        this.nRR = nRR;
    }

}