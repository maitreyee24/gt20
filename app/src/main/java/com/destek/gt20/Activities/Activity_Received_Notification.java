package com.destek.gt20.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.destek.gt20.Adapter.MyNotification_Adapter;
import com.destek.gt20.ApiClient;
import com.destek.gt20.ApiInterface;
import com.destek.gt20.App.AppSingleton;
import com.destek.gt20.Model.ReceivedNotification.Datum;
import com.destek.gt20.Model.ReceivedNotification.GetReceivedNotification;
import com.destek.gt20.ProjectConfig;
import com.destek.gt20.R;
import com.destek.gt20.Utils.PreferenceSettings;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_Received_Notification  extends AppCompatActivity
{

    private ImageView Img_back;
    private RecyclerView recyclerView;
    private List<Datum> NotificationList = new ArrayList<>();
    private MyNotification_Adapter mAdapter;
    private PreferenceSettings preferenceSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allnotifications);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        recyclerView = findViewById(R.id.vertical_recycler_view_home);

        preferenceSettings = AppSingleton.getInstance().getPreferenceSettings();

        if(ProjectConfig.isNetworkConnected(Activity_Received_Notification.this))
        {
            prepareNotificaion();
        }
        else
        {
            Toast.makeText(Activity_Received_Notification.this,getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }



        Img_back = findViewById(R.id.Img_back);
        Img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }

    private void prepareNotificaion()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(Activity_Received_Notification.this);
        progressDialog.show();
        progressDialog.setCancelable(false);

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("Userid", "2");


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<GetReceivedNotification> call = apiService.GetReceivedNotification("Bearer "+preferenceSettings.getTokenId(),jsonObject);
        call.enqueue(new Callback<GetReceivedNotification>() {
            @Override
            public void onResponse(Call<GetReceivedNotification> call, Response<GetReceivedNotification> response)
            {
                progressDialog.dismiss();
                String Message = response.body().getMessage();
                String Description = response.body().getDescription();
                String Error = response.body().getError();


                if (Message.equals("Success"))
                {
                    NotificationList = response.body().getData();

                    mAdapter = new MyNotification_Adapter(NotificationList, getApplicationContext());
                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext());
                    recyclerView.setLayoutManager(mLayoutManager1);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<GetReceivedNotification> call, Throwable t) {
                // Log error here since request failed
                progressDialog.dismiss();
                System.out.println("##onFailure" + t.toString());
            }
        });
    }
}
