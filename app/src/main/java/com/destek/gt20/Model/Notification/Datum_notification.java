package com.destek.gt20.Model.Notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum_notification
{
    @SerializedName("NotificationId")
    @Expose
    private Integer notificationId;
    @SerializedName("RemindarNo")
    @Expose
    private Integer remindarNo;
    @SerializedName("RemindarType")
    @Expose
    private String remindarType;

    public Integer getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Integer notificationId) {
        this.notificationId = notificationId;
    }

    public Integer getRemindarNo() {
        return remindarNo;
    }

    public void setRemindarNo(Integer remindarNo) {
        this.remindarNo = remindarNo;
    }

    public String getRemindarType() {
        return remindarType;
    }

    public void setRemindarType(String remindarType) {
        this.remindarType = remindarType;
    }

}