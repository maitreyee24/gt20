package com.destek.gt20.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.destek.gt20.Model.Scorecard.FallOfWicketsDetail;
import com.destek.gt20.R;
import java.util.List;

public class Home_Scorecardfallofwickets extends RecyclerView.Adapter<Home_Scorecardfallofwickets.MyViewHolder>
{

    List<FallOfWicketsDetail> score_team_fallofwickets__list;
    Context context;

    public Home_Scorecardfallofwickets(List<FallOfWicketsDetail> score_team_fallofwickets__list,  Context context)
    {
       this.score_team_fallofwickets__list=score_team_fallofwickets__list;
       this.context=context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fallofwicketslist_row, parent, false);
        return new Home_Scorecardfallofwickets.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position)
    {
        FallOfWicketsDetail fallofwickets = score_team_fallofwickets__list.get(position);
        holder.Txt_name.setText(fallofwickets.getPlayerName());
        holder.Txt_score.setText(fallofwickets.getScore());
        holder.Txt_runs.setText(""+fallofwickets.getOver());

        if (position % 2 == 0)
        {
            holder.LL_Master.setBackgroundColor(Color.parseColor("#e6e6ea"));
        }else
        {
            holder.LL_Master.setBackgroundColor(Color.parseColor("#ffffff"));
        }
    }

    @Override
    public int getItemCount() {
        return score_team_fallofwickets__list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView Txt_name, Txt_score, Txt_runs;
        public LinearLayout LL_Master;

        public MyViewHolder(View view) {
            super(view);
            Txt_name = view.findViewById(R.id.Txt_name);
            Txt_score = view.findViewById(R.id.Txt_score);
            Txt_runs = view.findViewById(R.id.Txt_runs);
            LL_Master = view.findViewById(R.id.LL_Master);
        }
    }
}
