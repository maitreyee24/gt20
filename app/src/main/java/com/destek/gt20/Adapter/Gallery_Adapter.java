package com.destek.gt20.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.destek.gt20.Model.Media.Datum_Pictures;
import com.destek.gt20.R;
import com.squareup.picasso.Picasso;

import java.util.List;


public class Gallery_Adapter extends RecyclerView.Adapter<Gallery_Adapter.MyViewHolder> {
    private List<Datum_Pictures> gallery_List;
    public Context context;

    public Gallery_Adapter(List<Datum_Pictures> gallery_list, Context context) {
        this.gallery_List = gallery_list;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_list_row, parent, false);
        return new Gallery_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final Datum_Pictures gallery = gallery_List.get(position);
//        Glide.with(context).load(gallery.getImageName()).error(R.mipmap.ic_launcher_round).into(holder.Img);
        Picasso.with(context).load(gallery.getImageName()).resize(6000, 2000)
                .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(holder.Img);

        holder.ll_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog settingsDialog = new Dialog(context);
                settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                View view = LayoutInflater.from(context).inflate(R.layout.image_layout, null);
                ImageView Img_pic = view.findViewById(R.id.Img_pic);

                Glide.with(context).load(gallery.getImageName()).error(R.mipmap.ic_launcher_round).into(Img_pic);
                /*Picasso.with(context).load(gallery.getImageName()).resize(6000, 2000)
                        .onlyScaleDown().placeholder(R.mipmap.ic_launcher_round).into(Img_pic);
                settingsDialog.setContentView(view);
                settingsDialog.show();*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return gallery_List.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView Img;
        public LinearLayout ll_image;

        public MyViewHolder(View view) {
            super(view);
            Img = view.findViewById(R.id.Img);
            ll_image = view.findViewById(R.id.ll_image);

        }
    }

}
