package com.destek.gt20.Model.Team;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Datum implements Serializable
{

    @SerializedName("TeamId")
    @Expose
    private Integer teamId;
    @SerializedName("SeasonId")
    @Expose
    private Integer seasonId;
    @SerializedName("TeamName")
    @Expose
    private String teamName;
    @SerializedName("TeamLogo")
    @Expose
    private String teamLogo;
    @SerializedName("CaptainName")
    @Expose
    private String captainName;
    @SerializedName("CoachName")
    @Expose
    private String coachName;
    @SerializedName("ShortName")
    @Expose
    private String shortName;
    @SerializedName("TeamColor")
    @Expose
    private String teamColor;

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(Integer seasonId) {
        this.seasonId = seasonId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamLogo() {
        return teamLogo;
    }

    public void setTeamLogo(String teamLogo) {
        this.teamLogo = teamLogo;
    }

    public String getCaptainName() {
        return captainName;
    }

    public void setCaptainName(String captainName) {
        this.captainName = captainName;
    }

    public String getCoachName() {
        return coachName;
    }

    public void setCoachName(String coachName) {
        this.coachName = coachName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getTeamColor() {
        return teamColor;
    }

    public void setTeamColor(String teamColor) {
        this.teamColor = teamColor;
    }

}